"""Aplica un clasificador para parches para obtener la curva ROC y PR.

Aplica la clasificacion a una lista de imagenes etiquetadas, la curva
se calcula moviendo el valor del threshold de a pasos definidos por
tr_step. Ademas hace lo mismo pero discriminando parches que estan
en el centro de la imagen y parches que estan en los bordes laterales.
Esto ultimo es para comprar si es verdad que el clasificador es mejor
en el centro. Para eso se usa el archivo idx, con la info de cada parche.
"""

import os
import sys
import tflib.soja_dataset_labeled
import numpy as np
import tensorflow as tf2
tf = tf2.compat.v1
tf.disable_v2_behavior()
import tflib as lib
from gan import GeneratorAndDiscriminator
import argparse
import time
import datetime
from numpy import trapz
from utils import coord_pos, get_coords

sys.path.append(os.getcwd())
parser = argparse.ArgumentParser()

parser.add_argument("--labels", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ALL_IDX/labels_(14031,3)_3_clases_float64_BALANCEADO_TRAIN.npy')

parser.add_argument("--parches", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ALL_IDX/parches_labeled_(14031,96,96,3)_uint8_BALANCEADO_TRAIN.npy')

parser.add_argument("--indice", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ALL_IDX/idx_96.npy')

parser.add_argument("--batch_size", type=int, default=1559) # tiene que ser divisor del tamanio total de parches

parser.add_argument("--margen_i", type=int, default=224 + 64 * 5) # para definir que es centro y borde

parser.add_argument("--model_dim", type=int, default=32)  # model size

parser.add_argument("--n_classes", type=int, default=3)
parser.add_argument("--output_dim", type=int, default=64 * 64 * 3)
parser.add_argument("--checkpoint_path", type=str,
                    default='/home/jmesuro/checkpoints/best_iter_class-69900')  # path to save the check for continue training
parser.add_argument("--tr_step", type=float, default=0.01) # threshold step size

args = parser.parse_args()




l_idx = open(args.indice).readlines()
coords = get_coords(l_idx)  # obtiene las coordenadas del parche en la imagen original
cord_pos, b, c = coord_pos(coords,
                           args.margen_i)  # define si el parche esta en el borde o en el centro y las cantidades


print('cantidad: border center' + str(b) + ' ' + str(c))
# -----------------------
# Build folders and files
# -----------------------
actual_path = os.getcwd()
OUT_PATH = actual_path + '/out/'
os.path.abspath(OUT_PATH)
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)

DATE = (time.strftime("%Y_%m_%d")) + '_' + str(datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S'))
THIS_RUN_PATH = OUT_PATH + DATE + '_class_roc_np' + '_' + str(args.model_dim)

LOG_FOLDER_PATH = THIS_RUN_PATH + '/logs/'

os.mkdir(THIS_RUN_PATH)

OUT_PATH = os.getcwd() + '/out/'
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)

if not os.path.exists(LOG_FOLDER_PATH):
    os.mkdir(LOG_FOLDER_PATH)

BORDER_PATH = LOG_FOLDER_PATH + 'border/'
if not os.path.exists(BORDER_PATH):
    os.mkdir(BORDER_PATH)

CENTER_PATH = LOG_FOLDER_PATH + 'center/'
if not os.path.exists(CENTER_PATH):
    os.mkdir(CENTER_PATH)

# Log comun
with open(LOG_FOLDER_PATH + '/roc.log', 'w') as f:
    f.write('# thres   acc       tp        fp        tn        fn        tpr       fpr')
    f.write('\n')

with open(LOG_FOLDER_PATH + '/best_line_roc.log', 'w') as f:
    f.write('# thres   acc       tp        fp        tn        fn        tpr       fpr')
    f.write('\n')

with open(LOG_FOLDER_PATH + '/print.log', 'w') as f:
    f.write('# thres   acc       aur       aupr')
    f.write('\n')

with open(LOG_FOLDER_PATH + '/pr.log', 'w') as f:
    f.write('# thres   acc       tp        fp        tn        fn        p       r')
    f.write('\n')

with open(LOG_FOLDER_PATH + '/solo_pr.log', 'w') as f:
    f.write('# p       r')
    f.write('\n')

with open(LOG_FOLDER_PATH + '/solo_roc.log', 'w') as f:
    f.write('# tpr       fpr')
    f.write('\n')

# BORDER
with open(BORDER_PATH + '/roc_border.log', 'w') as f:
    f.write('# thres   acc       tp        fp        tn        fn        tpr       fpr')
    f.write('\n')

with open(BORDER_PATH + '/best_line_roc_border.log', 'w') as f:
    f.write('# thres   acc       tp        fp        tn        fn        tpr       fpr')
    f.write('\n')

with open(BORDER_PATH + '/print_border.log', 'w') as f:
    f.write('# thres   acc       aur       aupr')
    f.write('\n')

with open(BORDER_PATH + '/pr_border.log', 'w') as f:
    f.write('# thres   acc       tp        fp        tn        fn        p       r')
    f.write('\n')

with open(BORDER_PATH + '/solo_pr_border.log', 'w') as f:
    f.write('# p       r')
    f.write('\n')

with open(BORDER_PATH + '/solo_roc_border.log', 'w') as f:
    f.write('# tpr       fpr')
    f.write('\n')

# CENTER
with open(CENTER_PATH + '/roc_center.log', 'w') as f:
    f.write('# thres   acc       tp        fp        tn        fn        tpr       fpr')
    f.write('\n')

with open(CENTER_PATH + '/best_line_roc_center.log', 'w') as f:
    f.write('# thres   acc       tp        fp        tn        fn        tpr       fpr')
    f.write('\n')

with open(CENTER_PATH + '/center_print.log', 'w') as f:
    f.write('# thres   acc       aur       aupr')
    f.write('\n')

with open(CENTER_PATH + '/pr_center.log', 'w') as f:
    f.write('# thres   acc       tp        fp        tn        fn        p       r')
    f.write('\n')

with open(CENTER_PATH + '/solo_pr_center.log', 'w') as f:
    f.write('# p       r')
    f.write('\n')

with open(CENTER_PATH + '/solo_roc_center.log', 'w') as f:
    f.write('# tpr       fpr')
    f.write('\n')

# -------------------------------------
# Create the model
# -------------------------------------
_, Discriminator = GeneratorAndDiscriminator(model_arch='dcgan')
all_real_data_conv = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])
real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [args.batch_size, args.output_dim])
real_labels = tf.placeholder(tf.float32, [args.batch_size, args.n_classes])
disc_real, generated_labels = Discriminator(real_data, n_classes=args.n_classes, model_dim=args.model_dim)

generated_labels_sig = tf.nn.softmax(generated_labels)
x_thresholds = tf.placeholder(tf.float32, shape=args.batch_size)

# ------------------
# Get data iterators
# ------------------
all_parches, all_labels = lib.soja_dataset_labeled.load_all_dataset(args.parches, args.labels)
n_patches = all_labels.shape[0]

# Se crea un iterador para el dataset y los labels
def get_next(it, all_parches=all_parches, all_labels=all_labels, bs=args.batch_size):
    return all_parches[it * bs:(it + 1) * bs, :, :, :], all_labels[it * bs:(it + 1) * bs, :]


t = time.time()

# ----------------------
# Restore trained model
# ----------------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())
disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
disc_saver.restore(session, save_path=args.checkpoint_path)
n_thresholds = int(1 + 1 / args.tr_step)

tpr_list = []
fpr_list = []
tpr_list_b = []
fpr_list_b = []
tpr_list_c = []
fpr_list_c = []
all_generated_labels = []
all_real_labels = []

precision_list = []
recall_list = []
precision_list_b = []
recall_list_b = []
precision_list_c = []
recall_list_c = []

for iteration in range(0, int(n_patches / args.batch_size)):
    batch_xs, batch_ys = get_next(iteration)

    t = time.time()

    _generated_labels_sig = session.run([generated_labels_sig],
                                        feed_dict={all_real_data_conv: batch_xs, real_labels: batch_ys})

    all_generated_labels.extend(np.ndarray.tolist(_generated_labels_sig[0][:, 0]))
    all_real_labels.extend(np.ndarray.tolist(batch_ys[:, 0]))

    print(time.time() - t)

# clasifico los parches segun la posicion, centrales o laterales

n_patches_center = len(list((filter(lambda x: x == 'center', cord_pos))))
n_patches_border = n_patches - n_patches_center

best_acc = 0
best_line = []
best_acc_b = 0
best_line_b = []
best_acc_c = 0
best_line_c = []

for tr in range(0, n_thresholds):
    np_a = np.array([tr * args.tr_step] * args.batch_size, dtype=np.float32)

    malezas_generadas = [a >= (tr * args.tr_step) for a in all_generated_labels]
    malezas_reales = [int(a) == 1 for a in all_real_labels]

    malezas_generadas_center = [x for x, y in zip(malezas_generadas, cord_pos) if y == 'center']
    malezas_generadas_border = [x for x, y in zip(malezas_generadas, cord_pos) if y == 'border']

    malezas_reales_center = [x for x, y in zip(malezas_reales, cord_pos) if y == 'center']
    malezas_reales_border = [x for x, y in zip(malezas_reales, cord_pos) if y == 'border']

    tp = sum([mg and mr for (mg, mr) in zip(malezas_generadas, malezas_reales)])
    fp = sum([mg and (not mr) for (mg, mr) in zip(malezas_generadas, malezas_reales)])
    tn = sum([(not mg) and (not mr) for (mg, mr) in zip(malezas_generadas, malezas_reales)])
    fn = sum([(not mg) and mr for (mg, mr) in zip(malezas_generadas, malezas_reales)])
    acc = np.float32(sum([mg == mr for (mg, mr) in zip(malezas_generadas, malezas_reales)])) / n_patches

    tp_b = sum([mg and mr for (mg, mr) in zip(malezas_generadas_border, malezas_reales_border)])
    fp_b = sum([mg and (not mr) for (mg, mr) in zip(malezas_generadas_border, malezas_reales_border)])
    tn_b = sum([(not mg) and (not mr) for (mg, mr) in zip(malezas_generadas_border, malezas_reales_border)])
    fn_b = sum([(not mg) and mr for (mg, mr) in zip(malezas_generadas_border, malezas_reales_border)])
    acc_b = np.float32(sum([mg == mr for (mg, mr) in zip(malezas_generadas_border,
                                                         malezas_reales_border)])) / n_patches_border  # da mas de uno porque hay qye filtrar

    tp_c = sum([mg and mr for (mg, mr) in zip(malezas_generadas_center, malezas_reales_center)])
    fp_c = sum([mg and (not mr) for (mg, mr) in zip(malezas_generadas_center, malezas_reales_center)])
    tn_c = sum([(not mg) and (not mr) for (mg, mr) in zip(malezas_generadas_center, malezas_reales_center)])
    fn_c = sum([(not mg) and mr for (mg, mr) in zip(malezas_generadas_center, malezas_reales_center)])
    acc_c = np.float32(
        sum([mg == mr for (mg, mr) in zip(malezas_generadas_center, malezas_reales_center)])) / n_patches_center

    tpr = np.float32(tp) / (np.float32(tp) + np.float32(fn))
    fpr = np.float32(fp) / (np.float32(fp) + np.float32(tn))
    if (((np.float32(tp) + np.float32(fp))) > 0):
        p = np.float32(tp) / ((np.float32(tp) + np.float32(fp)))  # precision tp / (tp + fp)
    else:
        p = 1

    if ((np.float32(tp) + np.float32(fn)) > 0):
        r = np.float32(tp) / ((np.float32(tp) + np.float32(fn)))  # recall
    else:
        r = 1

    tpr_b = np.float32(tp_b) / (np.float32(tp_b) + np.float32(fn_b))
    fpr_b = np.float32(fp_b) / (np.float32(fp_b) + np.float32(tn_b))

    if (((np.float32(tp_b) + np.float32(fp_b))) > 0):
        p_b = np.float32(tp_b) / ((np.float32(tp_b) + np.float32(fp_b)))  # precision tp / (tp + fp)
    else:
        p_b = 1

    if ((np.float32(tp_b) + np.float32(fn_b)) > 0):
        r_b = np.float32(tp_b) / ((np.float32(tp_b) + np.float32(fn_b)))  # recall
    else:
        r_b = 1

    tpr_c = np.float32(tp_c) / (np.float32(tp_c) + np.float32(fn_c))
    fpr_c = np.float32(fp_c) / (np.float32(fp_c) + np.float32(tn_c))

    if (((np.float32(tp_c) + np.float32(fp_c))) > 0):
        p_c = np.float32(tp_c) / ((np.float32(tp_c) + np.float32(fp_c)))  # precision tp / (tp + fp)
    else:
        p_c = 1

    if ((np.float32(tp_c) + np.float32(fn_c)) > 0):
        r_c = np.float32(tp_c) / ((np.float32(tp_c) + np.float32(fn_c)))  # recall
    else:
        r_c = 1

    tpr_list.append(tpr)
    fpr_list.append(fpr)
    precision_list.append(p)
    recall_list.append(r)

    tpr_list_b.append(tpr_b)
    fpr_list_b.append(fpr_b)
    precision_list_b.append(p_b)
    recall_list_b.append(r_b)

    tpr_list_c.append(tpr_c)
    fpr_list_c.append(fpr_c)
    precision_list_c.append(p_c)
    recall_list_c.append(r_c)

    if acc > best_acc:
        best_acc = acc
        best_line = [[tr * args.tr_step, acc, tp, fp, tn, fn, tpr, fpr]]
    if acc_b > best_acc_b:
        best_acc_b = acc_b
        best_line_b = [[tr * args.tr_step, acc_b, tp_b, fp_b, tn_b, fn_b, tpr_b, fpr_b]]
    if acc_c > best_acc_c:
        best_acc_c = acc_c
        best_line_c = [[tr * args.tr_step, acc_c, tp_c, fp_c, tn_c, fn_c, tpr_c, fpr_c]]

    with open(LOG_FOLDER_PATH + '/roc.log', 'a') as f:
        np.savetxt(f, [[tr * args.tr_step, acc, tp, fp, tn, fn, tpr, fpr]], fmt='%1.3e')
    with open(LOG_FOLDER_PATH + '/pr.log', 'a') as f:
        np.savetxt(f, [[tr * args.tr_step, acc, tp, fp, tn, fn, p, r]], fmt='%1.3e')

    with open(LOG_FOLDER_PATH + '/solo_roc.log', 'a') as f:
        np.savetxt(f, [[tpr, fpr]], fmt='%1.3e')
    with open(LOG_FOLDER_PATH + '/solo_pr.log', 'a') as f:
        np.savetxt(f, [[p, r]], fmt='%1.3e')

    with open(BORDER_PATH + '/roc_border.log', 'a') as f:
        np.savetxt(f, [[tr * args.tr_step, acc_b, tp_b, fp_b, tn_b, fn_b, tpr_b, fpr_b]], fmt='%1.3e')
    with open(BORDER_PATH + '/pr_border.log', 'a') as f:
        np.savetxt(f, [[tr * args.tr_step, acc_b, tp_b, fp_b, tn_b, fn_b, p_b, r_b]], fmt='%1.3e')
    with open(BORDER_PATH + '/solo_roc_border.log', 'a') as f:
        np.savetxt(f, [[tpr_b, fpr_b]], fmt='%1.3e')
    with open(BORDER_PATH + '/solo_pr_border.log', 'a') as f:
        np.savetxt(f, [[p_b, r_b]], fmt='%1.3e')

    with open(CENTER_PATH + '/roc_center.log', 'a') as f:
        np.savetxt(f, [[tr * args.tr_step, acc_c, tp_c, fp_c, tn_c, fn_c, tpr_c, fpr_c]], fmt='%1.3e')
    with open(CENTER_PATH + '/pr_center.log', 'a') as f:
        np.savetxt(f, [[tr * args.tr_step, acc_c, tp_c, fp_c, tn_c, fn_c, p_c, r_c]], fmt='%1.3e')
    with open(CENTER_PATH + '/solo_roc_center.log', 'a') as f:
        np.savetxt(f, [[tpr_c, fpr_c]], fmt='%1.3e')
    with open(CENTER_PATH + '/solo_pr_center.log', 'a') as f:
        np.savetxt(f, [[p_c, r_c]], fmt='%1.3e')

y = np.asarray(tpr_list, dtype=np.float64)
x1 = np.asarray(fpr_list, dtype=np.float64)
y_b = np.asarray(tpr_list_b, dtype=np.float64)
x1_b = np.asarray(fpr_list_b, dtype=np.float64)
y_c = np.asarray(tpr_list_c, dtype=np.float64)
x1_c = np.asarray(fpr_list_c, dtype=np.float64)

ypr = np.asarray(precision_list, dtype=np.float64)
xpr = np.asarray(recall_list, dtype=np.float64)
ypr_b = np.asarray(precision_list_b, dtype=np.float64)
xpr_b = np.asarray(recall_list_b, dtype=np.float64)
ypr_c = np.asarray(precision_list_c, dtype=np.float64)
xpr_c = np.asarray(recall_list_c, dtype=np.float64)

aur = trapz(y=y, x=x1)
aur_pr = trapz(y=ypr, x=xpr)

aur_b = trapz(y=y_b, x=x1_b)
aur_pr_b = trapz(y=ypr_b, x=xpr_b)

aur_c = trapz(y=y_c, x=x1_c)
aur_pr_c = trapz(y=ypr_c, x=xpr_c)

# print 'AUR-pr' + str(trapz(y=ypr, x=xpr))
# print 'AUR-pr_b' + str(trapz(y=ypr_b, x=xpr_b))
# print 'AUR-pr_c' + str(trapz(y=ypr_c, x=xpr_c))
# print 'AUR-roc' + str(trapz(y=y, x=x1))
# print 'AUR-roc_border' + str(trapz(y=y_b, x=x1_b))
# print 'AUR-roc_center' + str(trapz(y=y_c, x=x1_c))

with open(LOG_FOLDER_PATH + '/best_line_roc.log', 'a') as f:
    np.savetxt(f, best_line, fmt='%1.3e')
with open(BORDER_PATH + '/best_line_roc_border.log', 'a') as f:
    np.savetxt(f, best_line_b, fmt='%1.3e')
with open(CENTER_PATH + '/best_line_roc_center.log', 'a') as f:
    np.savetxt(f, best_line_c, fmt='%1.3e')

with open(LOG_FOLDER_PATH + '/print.log', 'w') as f:
    np.savetxt(f, [[best_acc, -aur, -aur_pr]], fmt='%0.3f')
with open(BORDER_PATH + '/print_border.log', 'w') as f:
    np.savetxt(f, [[best_acc_b, -aur_b, -aur_pr_b]], fmt='%0.3f')
with open(CENTER_PATH + '/print_center.log', 'w') as f:
    np.savetxt(f, [[best_acc_c, -aur_c, -aur_pr_c]], fmt='%0.3f')

print("class ended!\n")
