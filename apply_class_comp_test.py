"""Aplica un clasificador de parches.

Aplica la clasificacion a una lista de imagenes, que se suponen que son
frames consecutivos de un video sobre un campo de soja. Sobre cada frame
se define un ROI (region of interest) el cual se divide en celdas (parches)
iguales y se realiza la clasificacion de cada una en (soja, maleza, suelo).

Esta probado sobre frames son 1080 x 1920 y celdas de 64 x 64
"""

import os
import numpy as np
import tensorflow as tf2
tf = tf2.compat.v1
tf.disable_v2_behavior()
import tflib as lib
from gan import GeneratorAndDiscriminator_comp_gan
import time
from PIL import Image, ImageDraw
from utils import listFiles, mark_frame


def apply_class_file_in(input='/home/jmesuro/detecting-weed/in_16_test.in'):
    """Aplica clasificaciones definidas por los parametros en el archivo input.
    Esta pensado para aplicar varias clasificaciones sobre varias carpetas de
    frames de video.

    Args:
        input: es un archivo de txt con el siguiente formato:

        ... empty line
        [checkpint_path]
        [input_frame_path]
        [output_class_frame_path]
        [threshold]
        ... empty line
        [checkpint_path]
        [input_frame_path]
        [output_class_frame_path]
        [threshold]
        [model_dim]

        ej:

        /home/jmesuro/checkpoints/best_iter_class-66600
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/New Folder
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_[enrRRR_88]/
        0.88
        16

        /home/jmesuro/checkpoints/best_iter_class-66600
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/New Folder
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_[enrRRR_50]/
        0.50
        16
    """

    f = open(input)
    lines = f.read().split('\n')
    for l in range(0, len(lines) - 1, 6):
        checkpoint_path = lines[l + 1]
        input_frames_path = lines[l + 2]
        output_class_frames_path = lines[l + 3]
        threshold = float(lines[l + 4])
        model_dim = int(lines[l + 5])

        # obtiene la cantidad de frames de la carpeta input_frames_path
        total_frames = len(listFiles(input_frames_path))

        log_folder = output_class_frames_path

        if not os.path.exists(output_class_frames_path):
            os.mkdir(output_class_frames_path)
        if not os.path.exists(log_folder):
            os.mkdir(log_folder)

        main(checkpoint_path, input_frames_path, output_class_frames_path, log_folder, threshold, total_frames,
             model_dim)


def apply_class():
    """Aplica la clasificacion definida por los parametros:

            checkpoint_path
            input_frames_path
            output_class_frames_path
            model_dim
            threshold

        Esta pensado para aplicar una sola clasificaciones sobre una sola carpeta de
        frames de video.
    """
    checkpoint_path = '/home/jmesuro/checkpoints/best_iter_c-147300'  # path del clasificador entrenado, ojo tamnio gan
    input_frames_path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/he_left_trunked/'  # path images a clasificar
    output_class_frames_path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_trunked_[0]/'  # path imagenes clasificadaas
    log_folder = output_class_frames_path
    total_frames = len(listFiles(input_frames_path))
    model_dim = 16
    threshold = 0.50

    if not os.path.exists(output_class_frames_path):
        os.mkdir(output_class_frames_path)
    if not os.path.exists(log_folder):
        os.mkdir(log_folder)

    main(checkpoint_path, input_frames_path, output_class_frames_path, log_folder, threshold, total_frames,
         model_dim)

def main(checkpoint_path, input_frames_path, output_class_frames_path, log_folder, threshold, total_frames, model_dim):


    model_arch = 'dcgan'
    threshold = threshold   # 0.25
    model_dim = model_dim  # 32
    n_classes = 3
    h = w = 64
    c = 3
    output_dim = h * w * c

    parches_w = 30  # cantidad de parches por ancho del ROI, 23 original, 30 nuevo
    parches_h = 9  # cantidad de parches por alto del ROI, 9 original, 5 nuevo
    batch_size = parches_w * parches_h  # cantidad de parches que entran en un ROI, es decir en una imagen

    #     W 1920
    # ----------------------------------
    # |                                |H 1080
    # |          W_ROI 1472            |
    # |       -----------------        |
    # |       | ROI           |H_ROI   |
    # |       |               |576     |
    # |       |               |        |
    # |marg_l |               |marg_l  |
    # ----------------------------------
    H = 1080
    W = 1920
    H_ROI = 576  # 576 original, 320 nuevo
    W_ROI = 1920  # 1472 original, 1920 nuevo
    if W_ROI >= 1920:
        marg_l = 0
    else:
        marg_l = (W - W_ROI) / 2 - 1
    marg_s = H - H_ROI

    # ---------------------------------------------------
    # Armo el clasificador, toma de una imagen a la vez
    # ---------------------------------------------------
    # images_crudas_t = tf.placeholder(tf.uint8, shape=[c, H_ROI, W_ROI])  # imagen entera cHN
    # a1 = tf.reshape(images_crudas_t, shape=[c, int(H_ROI / h), h, int(W_ROI / w), w])
    # a2 = tf.transpose(a1, perm=[1, 3, 0, 2, 4])
    batch_size = 170
    #all_real_data_conv = tf.reshape(a2, shape=[int(H_ROI / h * W_ROI / w), c, h, w])
    all_real_data_conv = tf.placeholder(tf.int32, shape=[batch_size, 3, 64, 64])
    real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [batch_size, output_dim])
    _, Discriminator = GeneratorAndDiscriminator_comp_gan(model_arch)

    _, _, flat = Discriminator(real_data, model_dim, n_classes=n_classes)


    # ----------------------
    # Restore trained model
    # ----------------------
    session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
    session.run(tf.global_variables_initializer())
    disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
    disc_saver.restore(session, save_path=checkpoint_path)

    im_1 = Image.open('/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf[0]/image-02568.png')
    im_0 = Image.open('/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf[0]/image-02569.png') # frente
    a_1 = np.asarray(im_1)
    a_0 = np.asarray(im_0)

    pos = (1080 - 128, 128) # izq

    w_min = pos[1] - 32
    w_max = pos[1] + 64 - 32
    h_max = pos[0]
    h_min = pos[0] - 64

    draw = ImageDraw.Draw(im_1)
    draw.rectangle(((pos[1], pos[0]), (pos[1] + 64, pos[0] + 64)), outline=(255, 0, 0), width=2)


    batch_izq = np.zeros(shape=(batch_size, 64, 64, 3),dtype='uint8')
    batch_izq[0] = a_0[pos[0]:pos[0]+64,pos[1]:pos[1]+64,:]
    k = 1
    for i in range(w_min, w_max, 5):
        for j in range(h_min, h_max, 5):
            batch_izq[k] = a_1[j:j + 64, i:i + 64, :]
            k +=1


    batch = batch_izq.transpose((0, 3, 1, 2))
    #batch = np.load('/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf2/batch.npy').transpose((0, 3, 1, 2))

    f =  open('/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf2/batch.idx')
    lines = f.readlines()



    batch_xs = batch
    _flat = session.run(flat, feed_dict={all_real_data_conv: batch_xs})

    dis = []
    dis.append(1000)
    for i in range(1,len(_flat)-1):
        dis.append( np.linalg.norm(_flat[0] - _flat[i]) )

    best = dis.index(min(dis))
    line = lines[best]
    a = int(line.split('_')[1])
    b = int((line.split('_')[2]).split('.')[0])

    draw = ImageDraw.Draw(a_1)
    draw.rectangle(((b, a), (b+64, a+64)), outline=(255, 0, 0), width=2)
    a_1.save('/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf[0]/m_image-02568.png')

   # draw.rectangle(((pos[1], pos[0] + 64), (pos[1] + 64, pos[0] + 128)), outline=(255, 0, 0), width=2)


    x = 3


#apply_class_file_in()
apply_class()