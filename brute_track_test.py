"""

0 - importar un coder, LISTO
1 - armar el roi con la lista de parches, la penúltima de abajo arriba, LISTO (pero tomo las dos, optmizar)
2 - deslizarlo en el frame siguiente
    a - hago una lista con todos los parches del frame a comparar,
        ve como se leen los frames del disco, si van en orden, LISTO
    b - los codeo
    c - calculo la distancia mínima y predigo donde está


PROBAR CON UN SOLO PARCHE

"""

import os
import numpy as np
import tensorflow as tf2
tf = tf2.compat.v1
tf.disable_v2_behavior()
import tflib as lib
from gan import GeneratorAndDiscriminator_comp_gan
import time
from PIL import Image
from utils import listFiles, mark_frame, mark_roi


def apply_class():
    """Aplica la clasificacion definida por los parametros:

            checkpoint_path
            input_frames_path
            output_class_frames_path
            model_dim
            threshold

        Esta pensado para aplicar una sola clasificaciones sobre una sola carpeta de
        frames de video.
    """
    checkpoint_path = '/home/jmesuro/checkpoints/best_iter_c-147300'  # path del clasificador entrenado, ojo tamnio gan
    input_frames_path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf/'  # path images a clasificar
    output_class_frames_path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf[0]/'  # path imagenes clasificadaas
    log_folder = output_class_frames_path
    total_frames = len(listFiles(input_frames_path))
    model_dim = 16
    threshold = 0.0

    if not os.path.exists(output_class_frames_path):
        os.mkdir(output_class_frames_path)
    if not os.path.exists(log_folder):
        os.mkdir(log_folder)

    main(checkpoint_path, input_frames_path, output_class_frames_path, log_folder, threshold, total_frames,
         model_dim)

def diff_batch(b1,b2):
    #diff = map((lambda x: np.linalg.norm(x[0] - x[1])), list(zip(b1, b2)))
    return sum(map( lambda x: np.average( np.linalg.norm(x[0] - x[1]) ) , list(zip(b1, b2)) ))


def main(checkpoint_path, input_frames_path, output_class_frames_path, log_folder, threshold, total_frames, model_dim):


    model_arch = 'dcgan'
    threshold = threshold   # 0.25
    model_dim = model_dim  # 32
    n_classes = 3
    h = w = 64
    c = 3
    output_dim = h * w * c
    pix_slide = 4  # cada cuando se desliza la ventana para el tracking

    parches_w = 23  # cantidad de parches por ancho del ROI, 23 original, 30 nuevo
    parches_h = 2  # cantidad de parches por alto del ROI, 9 original, 5 nuevo
    batch_size = parches_w * parches_h  # cantidad de parches que entran en un ROI, es decir en una imagen

    #     W 1920
    # ----------------------------------
    # |                                |H 1080
    # |          W_ROI 1472            |
    # |       -----------------        |
    # |       | ROI           |H_ROI   |
    # |       |               |576     |
    # |       |               |        |
    # |marg_l |               |marg_l  |
    # ----------------------------------
    H = 1080
    W = 1920
    H_ROI = parches_h * h  # 128, 576 original, 320 nuevo
    W_ROI = parches_w * w  # 1472 original, 1920 nuevo
    if W_ROI >= 1920:
        marg_l = 0
    else:
        marg_l = (W - W_ROI) / 2 - 1
    marg_s = H - H_ROI

    marg_l = int(marg_l)
    marg_s = int(marg_s)
    W_ROI = int(W_ROI)
    H_ROI = int(H_ROI)


    # ---------------------------------------------------
    # Armo el clasificador, toma de una imagen a la vez
    # ---------------------------------------------------
    images_crudas_t = tf.placeholder(tf.uint8, shape=[c, H_ROI, W_ROI])  # imagen entera cHN
    a1 = tf.reshape(images_crudas_t, shape=[c, int(H_ROI / h), h, int(W_ROI / w), w])
    a2 = tf.transpose(a1, perm=[1, 3, 0, 2, 4])

    all_real_data_conv = tf.reshape(a2, shape=[int(H_ROI / h * W_ROI / w), c, h, w])
    real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [batch_size, output_dim])

    # ---------------------------------------------------
    # Armo el clasificador, para el frame siguiente
    # ---------------------------------------------------
    H_ROI_NEXT = int(H_ROI /2)
    images_crudas_t_next = tf.placeholder(tf.uint8, shape=[c, H_ROI_NEXT, W_ROI])  # imagen entera cHN
    a1_next = tf.reshape(images_crudas_t_next, shape=[c, int(H_ROI_NEXT / h), h, int(W_ROI / w), w])
    a2_next = tf.transpose(a1_next, perm=[1, 3, 0, 2, 4])
    all_real_data_conv_next = tf.reshape(a2_next, shape=[int(H_ROI_NEXT / h * W_ROI / w), c, h, w])
    real_data_next = tf.reshape(2 * ((tf.cast(all_real_data_conv_next, tf.float32) / 255.) - .5), [int(batch_size/2), output_dim])


    _, Discriminator = GeneratorAndDiscriminator_comp_gan(model_arch)
    disc_real, generated_labels, flat = Discriminator(real_data, model_dim, n_classes=n_classes)
    _, _, flat_next = Discriminator(real_data_next, model_dim, n_classes=n_classes)

    generated_labels_sof = tf.nn.softmax(generated_labels)
    # ----------------------
    # Restore trained model
    # ----------------------
    session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
    session.run(tf.global_variables_initializer())
    disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
    disc_saver.restore(session, save_path=checkpoint_path)

    i_image = 0
    file = open(log_folder + "log.txt","w")
    tiempos2 = []
    tiempos1 = []


    l_iters = range(0,total_frames)
    for dirpath, _, filenames in os.walk(input_frames_path):
        filenames.sort()
        for f,n in zip(filenames,l_iters):
            if ( n +1< len(filenames)):
                path = os.path.abspath(os.path.join(dirpath, f))
                path_next = os.path.abspath(os.path.join(dirpath, filenames[n+1]))

                # if not os.path.exists(output_class_frames_path + 'm_' + f[:-3] + 'jpg'):

                im_next = Image.open(path_next)
                im_original = Image.open(path)

                roi_original = np.asarray(im_original)[marg_s:, marg_l: marg_l + W_ROI]
                batch_xs = roi_original.transpose(2, 0, 1)  # channel first

                t = time.time()
                _generated_labels, parches, _flat = session.run([generated_labels_sof, all_real_data_conv,flat],
                                                         feed_dict={images_crudas_t: batch_xs})

                im_roi = mark_roi(parches, c, im_original,   H_ROI, W_ROI, parches_h, parches_w)

                im_l = []
                flat_l = []
                batch_min_l = []
                batch_min = np.full((23,2048),255,dtype='float32')
                pos_min = (0,0)
                for i in range(1, 16):
                    for j in range(-16, 16):
                        roi_i = np.asarray(im_next)[marg_s + (i * pix_slide):marg_s + (i * pix_slide) + h,
                                (marg_l) + (j * pix_slide): (marg_l + W_ROI) + (j * pix_slide)]

                        batch_xs_i = roi_i.transpose(2, 0, 1)
                        parches_next, _flat_next = session.run([all_real_data_conv_next,flat_next], feed_dict={images_crudas_t_next: batch_xs_i})

                        flat_l.append(_flat_next)

                        # if (diff_batch(_flat[23:],_flat_next) <= (_flat[23:],batch_min) ):
                        #     batch_min = _flat_next
                        #     pos_min = (i,j)

                        im_roi_next = mark_roi(parches_next, c, im_original,H_ROI, W_ROI, int(parches_h/2), parches_w)
                        im_l.append(im_roi_next)  # channel first

                # a = np.zeros(shape=(, parches_w, c), dtype=np.uint8)
                # a[:, :, :] = np.asarray(im_original)
                # a[marg_s:, marg_l:marg_l + w_roi] = b



                t1 = time.time() - t


                im = mark_frame(_generated_labels, parches, threshold, c, marg_s, marg_l, im_original,
                                 H_ROI, W_ROI, parches_h, parches_w)  # marco la imagen
                #im.save(output_class_frames_path + 'm_' + f[:-3] + 'jpg')

                i_image = i_image + 1
                t2 = time.time() - t
                tiempos2.append(t2)
                tiempos1.append(t1)
                l = 'imagen numero ' + str(i_image) + ' tiempo1 ' + str(t1) + ' tiempo2 ' + str(t2)

                print(l)
                file.write(l + '\n')


    print("class ended!\n")
    if (len(tiempos1) != 0):
        file.write('promedio tiempos1, con escritura en disco: ' + str(sum(tiempos1) / len(tiempos1)) + '\n')
    if (len(tiempos2) != 0):
        file.write('promedio tiempos2, con escritura en disco: ' + str(sum(tiempos2) / len(tiempos2)))
    file.close()

apply_class()