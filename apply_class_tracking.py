"""Aplica un clasificador de parches.

Aplica la clasificacion a una lista de imagenes, que se suponen que son
frames consecutivos de un video sobre un campo de soja. Sobre cada frame
se define un ROI (region of interest) el cual se divide en celdas (parches)
iguales y se realiza la clasificacion de cada una en (soja, maleza, suelo).

Esta probado sobre frames son 1080 x 1920 y celdas de 64 x 64
"""

import os
import numpy as np
import tensorflow as tf2

tf = tf2.compat.v1
tf.disable_v2_behavior()
import tflib as lib
from gan import GeneratorAndDiscriminator_comp_gan
import time
from PIL import Image, ImageDraw
from utils import listFiles, mark_frame


def apply_class_file_in(input='/home/jmesuro/detecting-weed/in_16_test.in'):
    """Aplica clasificaciones definidas por los parametros en el archivo input.
    Esta pensado para aplicar varias clasificaciones sobre varias carpetas de
    frames de video.

    Args:
        input: es un archivo de txt con el siguiente formato:

        ... empty line
        [checkpint_path]
        [input_frame_path]
        [output_class_frame_path]
        [threshold]
        ... empty line
        [checkpint_path]
        [input_frame_path]
        [output_class_frame_path]
        [threshold]
        [model_dim]

        ej:

        /home/jmesuro/checkpoints/best_iter_class-66600
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/New Folder
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_[enrRRR_88]/
        0.88
        16

        /home/jmesuro/checkpoints/best_iter_class-66600
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/New Folder
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_[enrRRR_50]/
        0.50
        16
    """

    f = open(input)
    lines = f.read().split('\n')
    for l in range(0, len(lines) - 1, 6):
        checkpoint_path = lines[l + 1]
        input_frames_path = lines[l + 2]
        output_class_frames_path = lines[l + 3]
        threshold = float(lines[l + 4])
        model_dim = int(lines[l + 5])

        # obtiene la cantidad de frames de la carpeta input_frames_path
        total_frames = len(listFiles(input_frames_path))

        log_folder = output_class_frames_path

        if not os.path.exists(output_class_frames_path):
            os.mkdir(output_class_frames_path)
        if not os.path.exists(log_folder):
            os.mkdir(log_folder)

        main(checkpoint_path, input_frames_path, output_class_frames_path, log_folder, threshold, total_frames,
             model_dim)


def apply_class():
    """Aplica la clasificacion definida por los parametros:

            checkpoint_path
            input_frames_path
            output_class_frames_path
            model_dim
            threshold

        Esta pensado para aplicar una sola clasificaciones sobre una sola carpeta de
        frames de video.
    """
    checkpoint_path = '/home/jmesuro/checkpoints/best_iter_c-147300'  # path del clasificador entrenado, ojo tamnio gan
    input_frames_path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/he_left_trunked_2'  # path images a clasificar
    output_class_frames_path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf[10]/'  # path imagenes clasificadaas
    log_folder = output_class_frames_path
    total_frames = len(listFiles(input_frames_path))
    model_dim = 16
    threshold = 0.0

    if not os.path.exists(output_class_frames_path):
        os.mkdir(output_class_frames_path)
    if not os.path.exists(log_folder):
        os.mkdir(log_folder)

    main(checkpoint_path, input_frames_path, output_class_frames_path, log_folder, threshold, total_frames,
         model_dim)

#
#
# def track_batch(batch, im):
#     im_a = np.asarray(im)
#
#     w_min = pos[1] - 32
#     w_max = pos[1] + 64 - 32
#     h_max = pos[0]
#     h_min = pos[0] - 64
#
#     batch_coords = []
#     batch_der_coords = []
#     batch_size = len(range(w_min_izq, w_max_izq, 5)) * len(range(h_min_izq, h_max_izq, 5)) + 1
#     batch_izq = np.zeros(shape=(batch_size, 64, 64, 3), dtype='uint8')
#     batch_izq[0] = elem_izq
#     batch_izq_coords.append(pos_izq)
#     batch_der = np.zeros(shape=(batch_size, 64, 64, 3), dtype='uint8')
#     batch_der[0] = elem_der
#
#     k = 1
#     for i in range(w_min_izq, w_max_izq, 5):
#         for j in range(h_min_izq, h_max_izq, 5):
#             batch_izq[k] = (im_a[j:j + 64, i:i + 64, :])
#             batch_izq_coords.append((j, i))
#             k += 1
#
#
#     return batch_izq, batch_der, batch_izq_coords, batch_der_coords

def track_batch(batch_actual, im_ant, parches_w, parches_h):
    im_a = np.asarray(im_ant)
    #     W 1920
    # ----------------------------------
    # |                                |H 1080
    # |          W_ROI 1472            |
    # |       -----------------        |
    # |       | ROI           |H_ROI   |
    # |       |               |576     |
    # |       |               |        |
    # |marg_l |               |marg_l  |
    # ----------------------------------
    H = 1080
    W = 1920
    H_ROI = int(parches_h * 64)  # 576 original, 320 nuevo
    W_ROI = int(parches_w * 64)  # 1472 original, 1920 nuevo
    if W_ROI >= 1920:
        marg_l = 0
    else:
        marg_l = int((W - W_ROI) / 2)
    marg_s = int(H - H_ROI)

    elem_izq = batch_actual[0:64, 0:64]
    elem_der = batch_actual[0:64, (parches_w - 1) * 64:parches_w * 64]

    pos_izq = (marg_s, marg_l)
    pos_der = (marg_s, marg_l + W_ROI - 64)

    w_min_izq = pos_izq[1] - 32
    w_max_izq = pos_izq[1] + 64 - 32
    h_max_izq = pos_izq[0]
    h_min_izq = pos_izq[0] - 64

    w_min_der = pos_der[1] - 32
    w_max_der = pos_der[1] + 64 - 32
    h_max_der = pos_der[0]
    h_min_der = pos_der[0] - 64

    batch_izq_coords = []
    batch_der_coords = []
    batch_size = len(range(w_min_izq, w_max_izq, 5)) * len(range(h_min_izq, h_max_izq, 5)) + 1
    batch_izq = np.zeros(shape=(batch_size, 64, 64, 3), dtype='uint8')
    batch_izq[0] = elem_izq
    batch_izq_coords.append(pos_izq)
    batch_der = np.zeros(shape=(batch_size, 64, 64, 3), dtype='uint8')
    batch_der[0] = elem_der
    batch_der_coords.append(pos_der)

    k = 1
    for i in range(w_min_izq, w_max_izq, 5):
        for j in range(h_min_izq, h_max_izq, 5):
            batch_izq[k] = (im_a[j:j + 64, i:i + 64, :])
            batch_izq_coords.append((j, i))
            k += 1

    k = 1
    for i in range(w_min_der, w_max_der, 5):
        for j in range(h_min_der, h_max_der, 5):
            batch_der[k] = (im_a[j:j + 64, i:i + 64, :])
            batch_der_coords.append((j, i))
            k += 1

    return batch_izq, batch_der, batch_izq_coords, batch_der_coords


def track_batch_test(batch_actual, im_ant, parches_w, parches_h):
    im_a = np.asarray(im_ant)
    pix = 4
    #     W 1920
    # ----------------------------------
    # |                                |H 1080
    # |          W_ROI 1472            |
    # |       -----------------        |
    # |       | ROI           |H_ROI   |
    # |       |               |576     |
    # |       |               |        |
    # |marg_l |               |marg_l  |
    # ----------------------------------
    H = 1080
    W = 1920
    H_ROI = int(parches_h * 64)  # 576 original, 320 nuevo
    W_ROI = int(parches_w * 64)  # 1472 original, 1920 nuevo
    if W_ROI >= 1920:
        marg_l = 0
    else:
        marg_l = int((W - W_ROI) / 2)
    marg_s = int(H - H_ROI)

    # pos 11 13 14 16
    elem_11 = batch_actual[0:64, 10*64:10*64+64]
    elem_13 = batch_actual[0:64, 12*64:12*64+64]
    elem_14 = batch_actual[0:64, 13*64:13*64+64]
    elem_16 = batch_actual[0:64, 15*64:15*64+64]

    pos_11 = (marg_s, marg_l + 10 * 64)
    pos_13 = (marg_s, marg_l + 12 * 64)
    pos_14 = (marg_s, marg_l + 13 * 64)
    pos_16 = (marg_s, marg_l + 15 * 64)

    w_min_11 = pos_11[1] - 32
    w_max_11 = pos_11[1] + 64 - 32
    h_min_11 = pos_11[0] - 64
    h_max_11 = pos_11[0]

    w_min_13 = pos_13[1] - 32
    w_max_13 = pos_13[1] + 64 - 32
    h_min_13 = pos_13[0] - 64
    h_max_13 = pos_13[0]

    w_min_14 = pos_14[1] - 32
    w_max_14 = pos_14[1] + 64 - 32
    h_min_14 = pos_14[0] - 64
    h_max_14 = pos_14[0]

    w_min_16 = pos_16[1] - 32
    w_max_16 = pos_16[1] + 64 - 32
    h_min_16 = pos_16[0] - 64
    h_max_16 = pos_16[0]

    batch_size = len(range(w_min_11, w_max_11,pix)) * len(range(h_min_11, h_max_11, pix)) + 1
    batch_11 = np.zeros(shape=(batch_size, 64, 64, 3), dtype='uint8')
    batch_13 = np.zeros(shape=(batch_size, 64, 64, 3), dtype='uint8')
    batch_14 = np.zeros(shape=(batch_size, 64, 64, 3), dtype='uint8')
    batch_16 = np.zeros(shape=(batch_size, 64, 64, 3), dtype='uint8')

    batch_11[0] = elem_11
    batch_13[0] = elem_13
    batch_14[0] = elem_14
    batch_16[0] = elem_16

    coords_11, coords_13, coords_14, coords_16 = [], [], [], []
    coords_11.append(pos_11)
    coords_13.append(pos_13)
    coords_14.append(pos_14)
    coords_16.append(pos_16)

    k = 1
    for i in range(w_min_11, w_max_11, pix):
        for j in range(h_min_11, h_max_11, pix):
            batch_11[k] = (im_a[j:j + 64, i:i + 64, :])
            coords_11.append((j, i))
            k += 1

    k = 1
    for i in range(w_min_13, w_max_13, pix):
        for j in range(h_min_13, h_max_13, pix):
            batch_13[k] = (im_a[j:j + 64, i:i + 64, :])
            coords_13.append((j, i))
            k += 1

    k = 1
    for i in range(w_min_14, w_max_14, pix):
        for j in range(h_min_14, h_max_14, pix):
            batch_14[k] = (im_a[j:j + 64, i:i + 64, :])
            coords_14.append((j, i))
            k += 1

    k = 1
    for i in range(w_min_16, w_max_16, pix):
        for j in range(h_min_16, h_max_16, pix):
            batch_16[k] = (im_a[j:j + 64, i:i + 64, :])
            coords_16.append((j, i))
            k += 1

    return batch_11,batch_13,batch_14,batch_16, coords_11,coords_13,coords_14,coords_16

def get_best_match(_flat):
    dis = []
    dis.append(1000)
    for i in range(1, len(_flat) - 1):
        dis.append(np.linalg.norm(_flat[0] - _flat[i]))
    return dis.index(min(dis))

def draw_box_im(im,coords,best):
    # la posición hay que invertirla
    draw = ImageDraw.Draw(im)
    draw.rectangle(((coords[best][1], coords[best][0]),
                    (coords[best][1] + 64, coords[best][0] + 64)),
                   outline=(255, 0, 0), width=2)


def main(checkpoint_path, input_frames_path, output_class_frames_path, log_folder, threshold, total_frames, model_dim):
    model_arch = 'dcgan'
    threshold = threshold  # 0.25
    model_dim = model_dim  # 32
    n_classes = 3
    h = w = 64
    c = 3
    output_dim = h * w * c

    parches_w = 26  # cantidad de parches por ancho del ROI, 23 original, 30 nuevo
    parches_h = 2  # cantidad de parches por alto del ROI, 9 original, 5 nuevo
    batch_size = parches_w * parches_h  # cantidad de parches que entran en un ROI, es decir en una imagen

    #     W 1920
    # ----------------------------------
    # |                                |H 1080
    # |          W_ROI 1472            |
    # |       -----------------        |
    # |       | ROI           |H_ROI   |
    # |       |               |576     |
    # |       |               |        |
    # |marg_l |               |marg_l  |
    # ----------------------------------
    H = 1080
    W = 1920
    H_ROI = int(parches_h * h)  # 576 original, 320 nuevo
    W_ROI = int(parches_w * w)  # 1472 original, 1920 nuevo
    if W_ROI >= 1920:
        marg_l = 0
    else:
        marg_l = int((W - W_ROI) / 2)
    marg_s = int(H - H_ROI)

    # ---------------------------------------------------
    # Armo el clasificador, toma de una imagen a la vez
    # ---------------------------------------------------
    images_crudas_t = tf.placeholder(tf.uint8, shape=[c, H_ROI, W_ROI])  # imagen entera cHN
    a1 = tf.reshape(images_crudas_t, shape=[c, int(H_ROI / h), h, int(W_ROI / w), w])
    a2 = tf.transpose(a1, perm=[1, 3, 0, 2, 4])

    all_real_data_conv = tf.reshape(a2, shape=[int(H_ROI / h * W_ROI / w), c, h, w])
    real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [batch_size, output_dim])
    _, Discriminator = GeneratorAndDiscriminator_comp_gan(model_arch)

    all_real_data_conv_track = tf.placeholder(tf.int32, shape=[257, 3, 64, 64])
    real_data_track = tf.reshape(2 * ((tf.cast(all_real_data_conv_track, tf.float32) / 255.) - .5), [257, output_dim])

    _, _, flat = Discriminator(real_data_track, model_dim, n_classes=n_classes)
    disc_real, generated_labels, _ = Discriminator(real_data, model_dim, n_classes=n_classes)

    generated_labels_sof = tf.nn.softmax(generated_labels)

    # ----------------------
    # Restore trained model
    # ----------------------
    session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
    session.run(tf.global_variables_initializer())
    disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
    disc_saver.restore(session, save_path=checkpoint_path)

    l_iters = range(0, total_frames)

    for dirpath, _, filenames in os.walk(input_frames_path):
        filenames.sort()
        for f, n in zip(filenames, l_iters):
            if n + 1 < len(filenames):
                path_1 = os.path.abspath(os.path.join(dirpath, f))
                path_0 = os.path.abspath(os.path.join(dirpath, filenames[n + 1]))

                im_1 = Image.open(path_1)
                im = Image.open(path_0)

                # obtengo el ROI de la imagen
                roi = np.asarray(im)[marg_s:, marg_l: marg_l + W_ROI,:]

                b_11,b_13,b_14,b_16,c_11,c_13,c_14,c_16 = track_batch_test(roi, im_1, parches_w, parches_h)

                b_11 = b_11.transpose(0, 3, 1, 2)
                b_13 = b_13.transpose(0, 3, 1, 2)
                b_14 = b_14.transpose(0, 3, 1, 2)
                b_16 = b_16.transpose(0, 3, 1, 2)

                _flat_11 = session.run(flat, feed_dict={all_real_data_conv_track: b_11})
                _flat_13 = session.run(flat, feed_dict={all_real_data_conv_track: b_13})
                _flat_14 = session.run(flat, feed_dict={all_real_data_conv_track: b_14})
                _flat_16 = session.run(flat, feed_dict={all_real_data_conv_track: b_16})

                best_11 = get_best_match(_flat_11)
                best_13 = get_best_match(_flat_13)
                best_14 = get_best_match(_flat_14)
                best_16 = get_best_match(_flat_16)

                draw_box_im(im_1, c_11, best_11)
                draw_box_im(im_1, c_13, best_13)
                draw_box_im(im_1, c_14, best_14)
                draw_box_im(im_1, c_16, best_16)

                draw_box_im(im, c_11, 0)
                draw_box_im(im, c_13, 0)
                draw_box_im(im, c_14, 0)
                draw_box_im(im, c_16, 0)

                im.save(path_0)
                im_1.save(output_class_frames_path + 'm_' + f[:-3] + 'jpg')
                print(f)


    print("class ended!\n")

# apply_class_file_in()
apply_class()
