""" Aplica fft (fast fourier transform) por linea (de arriba para abajo) horizontal a un conjunto de imagenes
devuelve por linea el promedio (del total de imagenes) de las frecuencias medias de todas las imagenes

Esto era para intentar a partir de la frecuencia del parche detectar la distancia al robot
en teoría un parche lejano debería tener más frecuencia que uno cercano. La idea
era hacer el tamaño de los parches según la distancia

"""

import numpy as np
import matplotlib.pyplot as plt
import os
from PIL import Image


def mean_freq(xm):
    #freqs = np.fft.fftshift( np.fft.fftfreq(xm.size))
    n = xm.size
    I = 0
    sum = 0
    for i in range(n / 2, n-1):
        # sum = sum + (xm[i] * freqs[i])
        sum = sum + (xm[i] * i)
        I = I + xm[i]
    return sum / I



def all_fft(im):
    im_gray = im.convert('L')
    a = np.asarray(im_gray)

    lines = im.size[1]
    N = im.size[0]
    M = []
    for i in range(0, lines - 1):
        x = a[i]
        xm = np.fft.fftshift(np.abs(np.fft.fft(x)))
        x = np.fft.fftshift(x)
        plt.plot(xm)
        plt.show()
        M.append(xm)
        # plt.show()
    return M



def means_freq_from_im(im):
    im_gray = im.convert('L')
    a = np.asarray(im_gray)

    lines = im.size[1]
    N = im.size[0]
    M = []
    for i in range(0, lines - 1):
        x = a[i]
        xm = np.fft.fftshift(np.abs(np.fft.fft(x)))
        x = np.fft.fftshift(x)
        # plt.plot(xm)
        M.append(mean_freq(xm))
        # plt.show()
    return M


def list_img_files(directory):
    files = []
    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            path = os.path.abspath(os.path.join(dirpath, f))
            if os.path.isfile(path) and (".jpg" in f or ".png" in f):
                files.append(path)
    return files

#     W 1920
# ----------------------------------
# |                                |H 1080
# |          W_ROI 1472            |
# |       -----------------        |
# |       | ROI           |H_ROI   |
# |       |               |576     |
# |       |               |        |
# |marg_l |               |marg_l  |
# ----------------------------------

H = 1080
W = 1920
H_ROI = 576
W_ROI = 1472
marg_l = (W - W_ROI) / 2 - 1
marg_s = H - H_ROI




l = list_img_files('/home/jmesuro/imgs_test/')
Ms = []
all_freq_all_imgs = []

for im_i in l:
    im = Image.open(im_i)
    #roi_original = np.asarray(im)[H - H_ROI:, marg_l: marg_l + W_ROI]
    #im = Image.fromarray(roi_original)
    #im.show()
    print(im_i)

    m_freq_i = means_freq_from_im(im)
    all_freq_i = all_fft(im)
    #plt.plot(m_freq_i)
    #plt.show()


    #plt.plot(all_freq_i)
    #plt.show()

    all_freq_all_imgs.append(all_freq_i)
    Ms.append( m_freq_i )

am = np.asarray(Ms)
M_means = sum(am) / len(l)
print('average')
plt.plot(M_means)
plt.show()

x= 3
