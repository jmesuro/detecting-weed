"""Entrena un clasificador softmax
basado en WGAN con arquitectura DCGAN

Utiliza 6 datasets en disco:
train labels_train,
test  labels_test
unlabeled_fot, unlabeled_robot

Asume que los datasets corresponden a imágenes
    - train, test: NHWC 96x96x3 uint8
    - unlabeled_fot, unlabeled_robot: NCHW 3x224x224 uint8
    - labels_train, labels_test: NC float64

Entrena un clasificador el cual esta pensado para la detección de maleza
en videos de 1920x1080, dividiendo cada frame en celdas (parches) de 64x64
y clasificando cada parche en (soja, maleza, suelo). Además tiene en cuenta
el entorno de cada parche, es decir utiliza la celda de 192x192 que contiene
al parche en el centro. De esta manera se usan dos redes GAN

Returns:
    checkpoints
    best_checkpoints
    logs: acc, bests_acc, errors
    samples
"""

import os
import sys
import time
import tflib.soja_dataset_labeled
import tflib.soja_dataset_unlabeled
from tflib.utils import crop_192_to_64
from tflib.utils import resize_192_to_64
from tflib.utils import crop_224_to_192
import numpy as np
import tensorflow as tf2
tf = tf2.compat.v1
tf.disable_v2_behavior()
import tflib as lib
from utils import save_images
from gan import GeneratorAndDiscriminator_pe
import argparse
import datetime
from tflib.augmentation import Augmentation
import tflib.ops.linear_c
from utils import generate_image_entornos

# -----------------------------------------
# WGAN + softmax clasificador con entornos
# -----------------------------------------
sys.path.append(os.getcwd())

parser = argparse.ArgumentParser()

# train dataset
parser.add_argument("--labels_train", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/FOT_224_BALANCEADO_IDX/'
                            'labels_(9585,3)_3_clases_float64_FOT_BALANCEADO.npy')
parser.add_argument("--entornos_train", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/FOT_224_BALANCEADO_IDX/'
                            'parches_labeled_(9585,224,224,3)_uint8_FOT_BALANCEADO.npy')

# test dataset
parser.add_argument("--labels_test", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ROB_224_BALANCEADO_IDX/'
                            'labels_(999,3)_3_clases_float64_BALANCEADO_TEST.npy')
parser.add_argument("--entornos_test", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ROB_224_BALANCEADO_IDX/'
                            'parches_labeled_(999,224,224,3)_uint8_BALANCEADO_TEST.npy')

parser.add_argument("--parches_u_fot", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/soja'
                            '/parches_fotografos_u_1200878_3_224_224_utin8.h5')


parser.add_argument("--parches_u_robot", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/soja'
                            '/parches_robot_u_376200_3_224_244_uint8.h5')

parser.add_argument("--model_arch", type=str, default='dcgan')       # model arch: 'good', 'dcgan'
parser.add_argument("--model_dim", type=int, default=16)             # model size
parser.add_argument("--batch_size", type=int, default=16)
parser.add_argument("--n_iters", type=int, default=1)
parser.add_argument("--n_classes", type=int, default=3)
parser.add_argument("--start_iter", type=int, default=0)             # for continue training from chechpoint
parser.add_argument("--lambda_gp", type=int, default=10)             # wgan param
parser.add_argument("--output_dim", type=int, default=64 * 64 * 3)
parser.add_argument("--checkpoint_start_iter", type=int, default=0)  # iter from where to start to check
parser.add_argument("--checkpoint_iter", type=int, default=1)      # freq to validate
parser.add_argument("--checkpoint_gen_im", type=int, default=1)     # freq to gen im
parser.add_argument("--checkpoint_path", type=str, default=None)     # path to save the check for continue training
parser.add_argument("--rand_seed", type=int, default=2017)
parser.add_argument("--wd", type=float, default=0.01)
parser.add_argument("--back_class", type=str, default='no')          # indicate the deep of backprop signal in class
parser.add_argument("--labeled_da", type=str, default='no')  # data aumentation  labeled dataset
parser.add_argument("--unlabeled_da", type=str, default='no')  # data aumentation  unlabeled dataset
args = parser.parse_args()

# ------------------------
# Prepare labeled datasets
# ------------------------
(train_gen, valid_gen, test_gen), (n_train, n_valid, n_test) = \
    lib.soja_dataset_labeled.load_train_valid_test(args.entornos_train,
                                                   args.entornos_test,
                                                   args.labels_train,
                                                   args.labels_test,
                                                   args.batch_size,
                                                   cel_size=224)
valid_batches = n_valid / args.batch_size
test_batches = n_test / args.batch_size


def inf_train_gen():
    while True:
        for images, targets in train_gen():
            yield images, targets


def inf_valid_gen():
    while True:
        for images, targets in valid_gen():
            yield images, targets


def inf_test_gen():
    while True:
        for images, targets in test_gen():
            yield images, targets


gen_train = inf_train_gen()
gen_valid = inf_valid_gen()
gen_test = inf_test_gen()

# --------------------------
# Prepare unlabeled datasets
# --------------------------
train_unlab_gen_p = lib.soja_dataset_unlabeled.load_all(args.parches_u_fot, args.parches_u_robot, args.batch_size)


def inf_unlab_train_gen():
    while True:
        for images in train_unlab_gen_p():
            yield images


gen_unlab_train = inf_unlab_train_gen()

# -----------------------
# Build folders and files
# -----------------------
OUT_PATH = os.getcwd() + '/out/'
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)

date = (time.strftime("%Y_%m_%d")) + '_' + str(datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S'))
THIS_RUN_PATH = OUT_PATH + date + '_wgan_softmax_entornos_' + "model_dim={}_n_clases={}_n_iters={}_bs={}_wd={}". \
    format(
    args.model_dim,
    args.n_classes,
    args.n_iters,
    args.batch_size,
    args.wd)

CHECKPOINTS_FOLDER_PATH = THIS_RUN_PATH + '/checkpoints/'
BEST_CHECKPOINTS_FOLDER_PATH = THIS_RUN_PATH + '/best_checkpoints/'
LOG_FOLDER_PATH = THIS_RUN_PATH + '/logs/'
SAMPLES_PATH = THIS_RUN_PATH + '/samples/'

os.mkdir(THIS_RUN_PATH)
if not os.path.exists(BEST_CHECKPOINTS_FOLDER_PATH):
    os.mkdir(BEST_CHECKPOINTS_FOLDER_PATH)
if not os.path.exists(CHECKPOINTS_FOLDER_PATH):
    os.mkdir(CHECKPOINTS_FOLDER_PATH)
if not os.path.exists(LOG_FOLDER_PATH):
    os.mkdir(LOG_FOLDER_PATH)
if not os.path.exists(SAMPLES_PATH):
    os.mkdir(SAMPLES_PATH)

with open(LOG_FOLDER_PATH + '/errors_p.log', 'w') as f:
    f.write('# iter data_seen epoch gen_loss disc_loss cl_loss')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/errors_e.log', 'w') as f:
    f.write('# iter data_seen epoch gen_loss disc_loss cl_loss')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/accuracies.log', 'w') as f:
    f.write('# iter data_seen epoch train_acc valid_acc test_acc')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/best_acc.log', 'w') as f:
    f.write('# iter data_seen epoch train_acc valid_acc test_acc')
    f.write('\n')

print("Arguments:")
for arg in vars(args):
    print("\t{}: {}".format(arg, getattr(args, arg)))
print("")



def print_batch(batch, name):
    batch = ((batch + 1.) * (255.99 / 2)).astype('int32')
    save_images(batch.reshape((args.batch_size, 3, 224, 224)), os.path.join(SAMPLES_PATH, name))

# -------------------------
# Data Aumentation
# -------------------------
rotation_range = 20
width_shift_range = 0.028
height_shift_range = 0.016
shear_range = 0.14
zoom_range_center = 0.97
zoom_range_range = 0.18
zoom_range = (zoom_range_center - zoom_range_range * 0.5, zoom_range_center + zoom_range_range * 0.5)
random_curves_strength = 0.58
seed = 17
augm = Augmentation(rotation_range=rotation_range,  # In degrees
                    width_shift_range=width_shift_range,
                    height_shift_range=height_shift_range,
                    horizontal_flip=True,
                    shear_range=shear_range,  # In radians
                    zoom_range=zoom_range,  # >1 zoom out; <1 zoom in
                    channel_shift_range=0.0,  # 0-255
                    fill_mode='constant',  # 'nearest',
                    random_curves_strength=random_curves_strength,
                    seed=seed)

# ----------------
# Create the model
# ----------------
Generator_p, Generator_e, Discriminator_p, Discriminator_e = GeneratorAndDiscriminator_pe(model_arch='dcgan')

all_real_data_conv_p = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])
all_real_data_conv_e = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])

real_data_p = tf.reshape(2 * ((tf.cast(all_real_data_conv_p, tf.float32) / 255.) - .5),
                         [args.batch_size, args.output_dim])
real_data_e = tf.reshape(2 * ((tf.cast(all_real_data_conv_e, tf.float32) / 255.) - .5),
                         [args.batch_size, args.output_dim])

real_labels = tf.placeholder(tf.float32, [args.batch_size, args.n_classes])

fake_data_p = Generator_p(args.batch_size, model_dim=args.model_dim)
fake_data_e = Generator_e(args.batch_size, model_dim=args.model_dim)

disc_real_p, flat_p = Discriminator_p(real_data_p, model_dim=args.model_dim)
disc_real_e, flat_e = Discriminator_e(real_data_e, model_dim=args.model_dim)

disc_fake_p, _ = Discriminator_p(fake_data_p, model_dim=args.model_dim)
disc_fake_e, _ = Discriminator_e(fake_data_e, model_dim=args.model_dim)

# --------------------------
# CONCAT and CLASS
# --------------------------
concat = tf.concat([flat_p, flat_e], 1)
lib.ops.linear_c.set_weights_stdev(0.02)
generated_labels = lib.ops.linear_c.Linear('Disc_c.Class', 4 * 4 * 8 * args.model_dim * 2, args.n_classes, concat)
lib.ops.linear_c.unset_weights_stdev()
correct_prediction = tf.equal(tf.argmax(generated_labels, 1), tf.argmax(real_labels, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))


# Cls loss
cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(labels=real_labels, logits=generated_labels)
cross_entropy = tf.reduce_mean(cross_entropy)
w = lib.params_with_name('Disc_c.Class.W')[0]
reg = tf.nn.l2_loss(w)
cls_loss = cross_entropy + reg * args.wd

# Disc_e loss
gen_cost_p = -tf.reduce_mean(disc_fake_p)
disc_cost_p = tf.reduce_mean(disc_fake_p) - tf.reduce_mean(disc_real_p)

alpha_p = tf.random_uniform(shape=[args.batch_size, 1], minval=0., maxval=1.)
differences_p = fake_data_p - real_data_p
interpolates_p = real_data_p + (alpha_p * differences_p)
dis_interpolates_p, _ = Discriminator_p(interpolates_p, model_dim=args.model_dim)
gradients_p = tf.gradients(dis_interpolates_p, [interpolates_p])[0]
slopes_p = tf.sqrt(tf.reduce_sum(tf.square(gradients_p), reduction_indices=[1]))
gradient_penalty_p = tf.reduce_mean((slopes_p - 1.) ** 2)
disc_cost_p += args.lambda_gp * gradient_penalty_p

# Disc_p loss
gen_cost_e = -tf.reduce_mean(disc_fake_e)
disc_cost_e = tf.reduce_mean(disc_fake_e) - tf.reduce_mean(disc_real_e)

alpha_e = tf.random_uniform(shape=[args.batch_size, 1], minval=0., maxval=1.)
differences_e = fake_data_e - real_data_e
interpolates_e = real_data_e + (alpha_e * differences_e)
dis_interpolates_e, _ = Discriminator_e(interpolates_e, model_dim=args.model_dim)
gradients_e = tf.gradients(dis_interpolates_e, [interpolates_e])[0]
slopes_e = tf.sqrt(tf.reduce_sum(tf.square(gradients_e), reduction_indices=[1]))
gradient_penalty_e = tf.reduce_mean((slopes_e - 1.) ** 2)
disc_cost_e += args.lambda_gp * gradient_penalty_e


# Trainers
if args.back_class == 'yes':  # Entreno todas las capas o solo la ultima
    class_train_op = tf.train.AdamOptimizer(1e-4).minimize(cls_loss, var_list=lib.params_with_name('Disc'))
else:
    class_train_op = tf.train.AdamOptimizer(1e-4).minimize(cls_loss, var_list=lib.params_with_name('Disc_c.Class'))

gen_train_op_p = tf.train.AdamOptimizer(learning_rate=1e-4, beta1=0., beta2=0.9).minimize(gen_cost_p,
                                                                                          var_list=lib.params_with_name(
                                                                                              'Generator_p'),
                                                                                          colocate_gradients_with_ops=True)
disc_train_op_p = tf.train.AdamOptimizer(learning_rate=1e-4, beta1=0., beta2=0.9).minimize(disc_cost_p,
                                                                                           var_list=lib.params_with_name(
                                                                                               'Discriminator_p'),
                                                                                           colocate_gradients_with_ops=True)
gen_train_op_e = tf.train.AdamOptimizer(learning_rate=1e-4, beta1=0., beta2=0.9).minimize(gen_cost_e,
                                                                                          var_list=lib.params_with_name(
                                                                                              'Generator_e'),
                                                                                          colocate_gradients_with_ops=True)
disc_train_op_e = tf.train.AdamOptimizer(learning_rate=1e-4, beta1=0., beta2=0.9).minimize(disc_cost_e,
                                                                                           var_list=lib.params_with_name(
                                                                                               'Discriminator_e'),
                                                                                           colocate_gradients_with_ops=True)

# --------------
# Init session
# --------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())

# --------------
# Savers
# --------------
saver = tf.train.Saver(save_relative_paths=True)  # all session: gen + disc + class
best_disc_saver_p = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Discriminator_p'))
best_disc_saver_e = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Discriminator_e'))
best_class_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))  # disc + class

# ----------
# Train loop
# ----------
if args.checkpoint_path is None:
    START_ITER = 0
else:
    START_ITER = args.start_iter
    saver.restore(session, save_path=args.checkpoint_path)
    print("\nStarting in iter={}:".format(START_ITER))

best_acc = 0.
it_best = 0
last_it = 0
t = time.time()
for iteration in range(START_ITER, args.n_iters):
    epoch = iteration * args.batch_size / n_train
    start_time = time.time()

    # Train Gnerator
    _gen_cost_p, _, _gen_cost_e, _ = session.run([gen_cost_p, gen_train_op_p, gen_cost_e, gen_train_op_e])

    # Data Aumentation
    batch_xs_unlab = next(gen_unlab_train)
    if args.unlabeled_da == 'yes':
        batch_xs_unlab = augm.random_transform(batch_xs_unlab)
    batch_xs_unlab_192 = crop_224_to_192(batch_xs_unlab)

    # Train Discriminador entornos
    batch_xs_unlab_e = resize_192_to_64(batch_xs_unlab_192)
    _disc_cost_e, _ = session.run([disc_cost_e, disc_train_op_e],
                                  feed_dict={all_real_data_conv_e: batch_xs_unlab_e})

    # Train Discriminador parches
    batch_xs_unlab_p = crop_192_to_64(batch_xs_unlab_192)
    _disc_cost_p, _ = session.run([disc_cost_p, disc_train_op_p],
                                  feed_dict={all_real_data_conv_p: batch_xs_unlab_p})

    # Train Clasificador
    batch_xs, batch_ys = next(gen_train)
    if args.labeled_da == 'yes':
        batch_xs = augm.random_transform(batch_xs)
    batch_xs_192 = crop_224_to_192(batch_xs)
    batch_xs_e = resize_192_to_64(batch_xs_192)
    batch_xs_p = crop_192_to_64(batch_xs_192)
    _cls_cost, _, _train_acc = session.run([cls_loss, class_train_op, accuracy],
                                           feed_dict={all_real_data_conv_p: batch_xs_p,
                                                      all_real_data_conv_e: batch_xs_e, real_labels: batch_ys})

    # ----------------------------------------------------
    # Log, Sample and Check
    # ----------------------------------------------------
    if iteration >= args.checkpoint_start_iter and iteration % args.checkpoint_gen_im == args.checkpoint_gen_im - 1:
        generate_image_entornos(session, iteration + 1,args.batch_size,Generator_p,Generator_e,args.model_dim,SAMPLES_PATH)

    if iteration >= args.checkpoint_start_iter and iteration % args.checkpoint_iter == args.checkpoint_iter - 1:

        # Valid accuracy
        _valid_acc = 0.
        valid_it = 0
        for valid_it in range(int(valid_batches)):
            batch_xs_valid, batch_ys_valid = next(gen_valid)
            batch_xs_valid_e = resize_192_to_64(batch_xs_valid)
            batch_xs_valid_p = crop_192_to_64(batch_xs_valid)
            _valid_acc += session.run(accuracy, feed_dict={all_real_data_conv_p: batch_xs_valid_p,
                                                           all_real_data_conv_e: batch_xs_valid_e,
                                                           real_labels: batch_ys_valid})
        _valid_acc = _valid_acc / valid_batches

        # Test accuracy
        _test_acc = 0.
        test_it = 0
        for test_it in range(int(test_batches)):
            batch_xs_test, batch_ys_test =next(gen_test)
            batch_xs_test_e = resize_192_to_64(batch_xs_test)
            batch_xs_test_p = crop_192_to_64(batch_xs_test)
            _test_acc += session.run(accuracy, feed_dict={all_real_data_conv_p: batch_xs_test_p,
                                                          all_real_data_conv_e: batch_xs_test_e,
                                                          real_labels: batch_ys_test})
        _test_acc = _test_acc / test_batches

        # Log and save best model
        if best_acc < _valid_acc:



            best_acc = _valid_acc
            it_best = iteration

            # Save best model
            #best_disc_saver_p.save(sess=session, save_path=os.path.join(BEST_CHECKPOINTS_FOLDER_PATH, "best_iter_p"),
            #                       global_step=iteration + 1)
            #best_disc_saver_e.save(sess=session, save_path=os.path.join(BEST_CHECKPOINTS_FOLDER_PATH, "best_iter_e"),
            #                       global_step=iteration + 1)
            best_class_saver.save(sess=session, save_path=os.path.join(BEST_CHECKPOINTS_FOLDER_PATH, "best_iter_class"),
                                   global_step=iteration + 1)

            # Log best model
            with open(LOG_FOLDER_PATH + '/best_acc.log', 'a') as f:
                np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size,
                                epoch, _train_acc, _valid_acc, _test_acc]], fmt='%1.3e')

        with open(LOG_FOLDER_PATH + '/errors_p.log', 'a') as f:
            np.savetxt(f, [
                [iteration + 1, (iteration + 1) * args.batch_size, epoch, _gen_cost_p, _disc_cost_p, _cls_cost]],
                       fmt='%1.3e')
        with open(LOG_FOLDER_PATH + '/errors_e.log', 'a') as f:
            np.savetxt(f, [
                [iteration + 1, (iteration + 1) * args.batch_size, epoch, _gen_cost_e, _disc_cost_e, _cls_cost]],
                       fmt='%1.3e')
        with open(LOG_FOLDER_PATH + '/accuracies.log', 'a') as f:
            np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size, epoch, _train_acc, _valid_acc,
                            _test_acc]], fmt='%1.3e')

        t2 = time.time() - t
        t += t2
        horas = t2 / (1 + iteration - last_it) / 3600. * 10000
        print("iter:%d/%d; epoch:%d; %4.2f horas. para 10000 iteraciones" % (iteration + 1, args.n_iters, epoch, horas))
        last_it = iteration + 1
        # saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter"), global_step=iteration + 1)

#saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "last_iter"), global_step=args.n_iters + 1)
print("Train gan ended!\n")
