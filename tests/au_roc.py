import numpy as np
from numpy import trapz



f = open('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/experimentos/[2000]out/2019_02_25_16:12:59_wgan_softmax_entornos_model_dim=4_n_clases=3_n_iters=150000_bs=128_wd=0.01/2019_03_15_12:37:11_class_roc_np_FOT/logs/'
         'roc.log')
l = f.readlines()
tpr_list = []
fpr_list = []
for i in range(1,len(l)):
    tpr_list.append(np.float32(l[i].split(' ')[6]) )
    fpr_list.append(np.float32(l[i].split(' ')[7]))



y = np.asarray(tpr_list, dtype=np.float64)
x1 = np.asarray(fpr_list, dtype=np.float64)
print 'AUR-roc' + str(trapz(y=y, x=x1))
print "class ended!\n"
