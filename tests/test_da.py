import os
import sys
import time
import tflib.soja_dataset_labeled
import tflib.soja_dataset_unlabeled
from tflib.soja_dataset_labeled import crop_224_to_192 as cut_224_to_192
from tflib.soja_dataset_labeled import resize_192_to_64 as resize_192_to_64
from tflib.soja_dataset_labeled import crop_192_to_64 as cut_192_to_64
import numpy as np
import tensorflow as tf
import tflib as lib
import tflib.save_images
from gan import GeneratorAndDiscriminator
import argparse
import datetime
from tflib.augmentation import Augmentation
from PIL import Image



# ------------------------------------
# WGAN + softmax clasificador
# ------------------------------------
sys.path.append(os.getcwd())

parser = argparse.ArgumentParser()
parser.add_argument("--patchpath_u", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/soja/parches_fotografos_u_1200878_3_224_224_utin8.h5')
parser.add_argument("--batch_size", type=int, default=16)  # Batch size. Must be a multiple of N_GPUS

args = parser.parse_args()


THIS_RUN_PATH = '/home/jmesuro/da/'


# ----------------------------------
# Image generation into SAMPLES_PATH
# ----------------------------------
def generate_image(session, iteration):
    # For generating samples
    fixed_noise = tf.constant(np.random.normal(size=(args.batch_size, 128)).astype('float32'))
    all_fixed_noise_samples = []
    n_samples = args.batch_size
    all_fixed_noise_samples.append(
        Generator(n_samples, args.model_dim, noise=fixed_noise[0:n_samples]))
    if tf.__version__.startswith('1.'):
        all_fixed_noise_samples = tf.concat(all_fixed_noise_samples, axis=0)
    else:
        all_fixed_noise_samples = tf.concat(0, all_fixed_noise_samples)

    samples = session.run(all_fixed_noise_samples)
    samples = ((samples + 1.) * (255.99 / 2)).astype('int32')
    lib.save_images.save_images(samples.reshape((args.batch_size, 3, 64, 64)),
                                os.path.join(THIS_RUN_PATH, 'samples_{}.jpg'.format(iteration)))


# -------------------------
# Data Aumentation
# -------------------------
rotation_range = 20
width_shift_range = 0.028
height_shift_range = 0.016
shear_range = 0.14
zoom_range_center = 0.97
zoom_range_range = 0.18
zoom_range = (zoom_range_center - zoom_range_range * 0.5, zoom_range_center + zoom_range_range * 0.5)
random_curves_strength = 0.58
seed = 17
augm = Augmentation(rotation_range=rotation_range,  # In degrees
                    width_shift_range=width_shift_range,
                    height_shift_range=height_shift_range,
                    horizontal_flip=False,
                    shear_range=shear_range,  # In radians
                    zoom_range=zoom_range,  # >1 zoom out; <1 zoom in
                    channel_shift_range=0.0,  # 0-255
                    fill_mode='constant',  # 'nearest',
                    random_curves_strength=random_curves_strength,
                    seed=seed)

# ------------------------
# Prepare unlabeled datasets
# ------------------------
train_unlab_gen = lib.soja_dataset_unlabeled.load(args.patchpath_u, args.batch_size)


def inf_unlab_train_gen():
    while True:
        for images in train_unlab_gen():
            yield images


gen_unlab_train = inf_unlab_train_gen()


x = inf_unlab_train_gen().next()
x1 = ((x + 1.) * (255.99 / 2)).astype('int32')
lib.save_images.save_images(x1.reshape((args.batch_size, 3, 224, 224)),
                            os.path.join(THIS_RUN_PATH, 'samples_groundtruth_224.jpg'))


x1_192 = crop_224_to_192(x)
x1_192 = ((x1_192 + 1.) * (255.99 / 2)).astype('int32')
lib.save_images.save_images(x1_192.reshape((args.batch_size, 3, 192, 192)),
                            os.path.join(THIS_RUN_PATH, 'samples_groundtruth_192.jpg'))

x1_aum = augm.random_transform(x)
x1_192 = crop_224_to_192(x1_aum)
x1_e = resize_192_to_64(x1_192)
x1_p = crop_192_to_64(x1_192)


x1_192 = ((x1_192 + 1.) * (255.99 / 2)).astype('int32')
lib.save_images.save_images(x1_192.reshape((args.batch_size, 3, 192, 192)),
                            os.path.join(THIS_RUN_PATH, 'samples_groundtruth_192_aum.jpg'))

x1_aum = ((x1_aum + 1.) * (255.99 / 2)).astype('int32')
lib.save_images.save_images(x1_aum.reshape((args.batch_size, 3, 224, 224)),
                            os.path.join(THIS_RUN_PATH, 'samples_groundtruth_aum.jpg'))

x1_e = ((x1_e + 1.) * (255.99 / 2)).astype('int32')
lib.save_images.save_images(x1_e.reshape((args.batch_size, 3, 64, 64)),
                            os.path.join(THIS_RUN_PATH, 'samples_groundtruth_e.jpg'))

x1_p = ((x1_p + 1.) * (255.99 / 2)).astype('int32')
lib.save_images.save_images(x1_p.reshape((args.batch_size, 3, 64, 64)),
                            os.path.join(THIS_RUN_PATH, 'samples_groundtruth_p.jpg'))




x = 3