import h5py
import os
import sys
import tests.soja_cifasis_robot_ROI_dataset_u
import numpy as np
import tensorflow as tf
import tflib as lib
from gan import GeneratorAndDiscriminator
import argparse
import time

# ------------------------------------------------------------------------
# Carga un clasificador entrenado y clasifica todos los datos etiquetados
# ------------------------------------------------------------------------
sys.path.append(os.getcwd())
parser = argparse.ArgumentParser()
parser.add_argument("--model_arch", type=str, default='dcgan')  # model arch: 'good', 'dcgan'
parser.add_argument("--batch_size", type=int, default=1)  # Batch size. Must be a multiple of N_GPUS
parser.add_argument("--n_classes", type=int, default=2)  # How many classes to train for
parser.add_argument("--output_dim", type=int, default=64 * 64 * 3)  # Number of pixels in each image
parser.add_argument("--checkpoint_path", type=str, default='/home/jmesuro/chkpoint/best_iter-73100')
args = parser.parse_args()


# -------------------------------------
# Create the model
# -------------------------------------
_, Discriminator = GeneratorAndDiscriminator(model_arch='dcgan')
all_real_data_conv = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])
real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [args.batch_size, args.output_dim])
real_labels = tf.placeholder(tf.float32, [args.batch_size, args.n_classes])
disc_real, generated_labels, _ = Discriminator(real_data, n_classes=args.n_classes)

correct_prediction = tf.equal(tf.argmax(generated_labels, 1), tf.argmax(real_labels, 1))
# ------------------
# Get data iterators
# ------------------
n_images = 26
data_gen = tests.soja_cifasis_robot_ROI_dataset_u.load(n_images=207 * n_images, batch_size=args.batch_size)
data_len = 207*26


def inf_data_gen():
    while True:
        for images in data_gen():
            yield images


gen_data = inf_data_gen()
t = time.time()

# ----------------------
# Restore trained model
# ----------------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())
disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
disc_saver.restore(session, save_path=args.checkpoint_path)

all_generated_labels = []
all_accs = []

ims_asarrays = []
i = 0
for iteration in range(0, 207*n_images):
    batch_xs = gen_data.next()

    a = np.array((batch_xs[0:1][0]), dtype=np.uint8)
    ims_asarrays.append(a)
    i = i + 1
    t = time.time()


    _generated_labels = session.run([generated_labels], feed_dict={all_real_data_conv: batch_xs})
    print time.time() - t
    all_generated_labels.append(_generated_labels)


OUT_PATH = '/home/jmesuro/videos_soja/robot_videos_MOV/test/'
fp = h5py.File(OUT_PATH + 'parches_ROIT2frames_robot_u_m.h5', 'w')
fp.create_dataset('X', (207*n_images, 3, 64, 64), dtype=np.int8)

final_labels = []
i = 0
for i_label_batch in all_generated_labels:
    for i_label in i_label_batch:
        aux_list = i_label.tolist()[0]
        fl = aux_list.index(max(aux_list))
        final_labels.append(fl)

        ar = ims_asarrays[i]
        r = np.array(ar, dtype=np.uint8)
        #
        if fl == 1:
            r[1, :, :] = 0
            r[2, :, :] = 0

        #im2 = Image.fromarray(r.transpose((1,2,0)))

        #im2.save('/home/jmesuro/videos_soja/robot_videos_MOV/test/' + '_' + str(i) + 'image' + str(fl) + '.png')

        fp['X'][i] = r
        i = i+1

fp.close()
np.savetxt('/home/jmesuro/final_labels.npy', final_labels, fmt='%s')
print "class ended!\n"
