from PIL import Image
import numpy as np
import matplotlib.colors as colors
import cv2
from skimage import feature
from matplotlib import pyplot as plt

i = Image.open('/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/2420.png')
a = np.asarray(i,dtype=np.double)

ex_green =  2*a[:,:,1] - a[:,:,0] - a[:,:,2]
ex_reed = 1.4*a[:,:,0] - a[:,:,1]
cive = 0.881 * a[:, :, 1] - 0.441 * a[:, :, 0] - 0.385 * a[:, :, 2] - 18.78745
ndi = ((( a[:,:,1] - a[:,:,0] ) / ( a[:,:,1] + a[:,:,0]  ) ) + 1) * 128


hsv = colors.rgb_to_hsv(a)

laplacian = cv2.Laplacian(ex_green,cv2.CV_64F)
sobelx = cv2.Sobel(ex_green,cv2.CV_64F,1,0,ksize=5)
sobely = cv2.Sobel(ex_green,cv2.CV_64F,0,1,ksize=5)