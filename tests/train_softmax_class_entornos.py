import os
import sys
import time
import tflib.soja_dataset_labeled
from tflib.soja_dataset_labeled import crop_192_to_64 as cut_192_to_64
from tflib.soja_dataset_labeled import resize_192_to_64 as resize_192_to_64
from tflib.soja_dataset_labeled import crop_224_to_192 as cut_224_to_192
import numpy as np
import tensorflow as tf
import tflib as lib
from gan import GeneratorAndDiscriminator_pe
import argparse
import datetime
from tflib.augmentation import Augmentation
import tflib.ops.linear_c

# -----------------------------------------
# WGAN + softmax clasificador con entornos
# -----------------------------------------
sys.path.append(os.getcwd())

parser = argparse.ArgumentParser()

# train dataset
parser.add_argument("--labels_train", type=str,
                    default='/home/jmesuro/datasets/soja/labeled_224/labels_(9585,3)_3_clases_float64_BALANCEADO_TRAIN.npy')
parser.add_argument("--entornos_train", type=str,
                    default='/home/jmesuro/datasets/soja/labeled_224/parches_labeled_(9585,224,224,3)_uint8_BALANCEADO_TRAIN.npy')

# test dataset
parser.add_argument("--labels_test", type=str,
                    default='/home/jmesuro/datasets/soja/labeled_224/labels_(1728,3)_3_clases_float64_TEST.npy')
parser.add_argument("--entornos_test", type=str,
                    default='/home/jmesuro/datasets/soja/labeled_224/parches_labeled_(1728,224,224,3)_uint8_TEST.npy')

# unlabeled dataset
parser.add_argument("--parches_u", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/soja'
                            '/parches_fotografos_u_1200878_3_224_224_utin8.h5')

parser.add_argument("--model_arch", type=str, default='dcgan')  # model arch: 'good', 'dcgan'
parser.add_argument("--critic_iters", type=int, default=1)  # How many iterations to train the critic for
parser.add_argument("--model_dim", type=int, default=16)  # How many iterations to train the critic forn
parser.add_argument("--batch_size", type=int, default=128)  # Batch size. Must be a multiple of N_GPUS
parser.add_argument("--n_iters", type=int, default=500)  # How many iterations to train for
parser.add_argument("--n_classes", type=int, default=3)  # How many classes to train for
parser.add_argument("--start_iter", type=int, default=0)  # Start iter num when restoring model
parser.add_argument("--lambda_gp", type=int, default=10)  # Gradient penalty lambda hyperparameter
parser.add_argument("--output_dim", type=int, default=64 * 64 * 3)  # Number of pixels in each image
parser.add_argument("--checkpoint_start_iter", type=int, default=0)
parser.add_argument("--checkpoint_iter", type=int, default=10)  # Save model vars every checkpoint_iter iterations
parser.add_argument("--checkpoint_path", type=str, default=None)  # Path to recover a model to keep training
parser.add_argument("--rand_seed", type=int, default=2017)  # Random seed
parser.add_argument("--wd", type=float, default=0.01)  # Weight decay
args = parser.parse_args()

# ----------------------------------------------
# Check_path of the previously trained gan model
# ----------------------------------------------
BEST_DISC_SAVER_P = '/home/jmesuro/best_checkpoints/best_iter_p-10600'
BEST_DISC_SAVER_E = '/home/jmesuro/best_checkpoints/best_iter_e-10600'

# ------------------------
# Prepare labeled datasets
# ------------------------
(train_gen, test_gen), (n_train, n_test) = lib.soja_dataset_labeled.load_train_test(
    args.entornos_train,
    args.entornos_test,
    args.labels_train,
    args.labels_test,
    args.batch_size,
    cel_size=224)

test_batches = n_test / args.batch_size


def inf_train_gen():
    while True:
        for images, targets in train_gen():
            yield images, targets


def inf_test_gen():
    while True:
        for images, targets in test_gen():
            yield images, targets


gen_train = inf_train_gen()
gen_test = inf_test_gen()

# -----------------------
# Build folders and files
# -----------------------
OUT_PATH = os.getcwd() + '/out/'
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)

date = (time.strftime("%Y_%m_%d")) + '_' + str(datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S'))
THIS_RUN_PATH = OUT_PATH + date + '_wgan_softmax_class_entornos_' + "model_dim={}_n_iters={}_bs={}_wd={}". \
    format(
    args.model_dim,
    args.n_iters,
    args.batch_size,
    args.wd)

CHECKPOINTS_FOLDER_PATH = THIS_RUN_PATH + '/checkpoints/'
BEST_CHECKPOINTS_FOLDER_PATH = THIS_RUN_PATH + '/best_checkpoints/'
LOG_FOLDER_PATH = THIS_RUN_PATH + '/logs/'


os.mkdir(THIS_RUN_PATH)
if not os.path.exists(BEST_CHECKPOINTS_FOLDER_PATH):
    os.mkdir(BEST_CHECKPOINTS_FOLDER_PATH)
if not os.path.exists(CHECKPOINTS_FOLDER_PATH):
    os.mkdir(CHECKPOINTS_FOLDER_PATH)
if not os.path.exists(LOG_FOLDER_PATH):
    os.mkdir(LOG_FOLDER_PATH)


with open(LOG_FOLDER_PATH + '/errors.log', 'w') as f:
    f.write('# iter data_seen epoch cl_loss')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/accuracies.log', 'w') as f:
    f.write('# iter data_seen epoch train_acc test_acc')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/best_acc.log', 'w') as f:
    f.write('# iter data_seen epoch train_acc test_acc')
    f.write('\n')

print("Arguments:")
for arg in vars(args):
    print("\t{}: {}".format(arg, getattr(args, arg)))
print("")


# -------------------------
# Data Aumentation
# -------------------------
rotation_range = 20
width_shift_range = 0.028
height_shift_range = 0.016
shear_range = 0.14
zoom_range_center = 0.97
zoom_range_range = 0.18
zoom_range = (zoom_range_center - zoom_range_range * 0.5, zoom_range_center + zoom_range_range * 0.5)
random_curves_strength = 0.58
seed = 17
augm = Augmentation(rotation_range=rotation_range,  # In degrees
                    width_shift_range=width_shift_range,
                    height_shift_range=height_shift_range,
                    horizontal_flip=True,
                    shear_range=shear_range,  # In radians
                    zoom_range=zoom_range,  # >1 zoom out; <1 zoom in
                    channel_shift_range=0.0,  # 0-255
                    fill_mode='constant',  # 'nearest',
                    random_curves_strength=random_curves_strength,
                    seed=seed)

# ----------------
# Create the model
# ----------------
_, _, Discriminator_p, Discriminator_e = GeneratorAndDiscriminator_pe(model_arch='dcgan')
all_real_data_conv_p = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])
all_real_data_conv_e = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])
real_data_p = tf.reshape(2 * ((tf.cast(all_real_data_conv_p, tf.float32) / 255.) - .5),
                         [args.batch_size, args.output_dim])
real_data_e = tf.reshape(2 * ((tf.cast(all_real_data_conv_e, tf.float32) / 255.) - .5),
                         [args.batch_size, args.output_dim])
real_labels = tf.placeholder(tf.float32, [args.batch_size, args.n_classes])
disc_real_p, flat_p = Discriminator_p(real_data_p, model_dim=args.model_dim)
disc_real_e, flat_e = Discriminator_e(real_data_e, model_dim=args.model_dim)

# --------------------------
# CONCAT and CLASS
# --------------------------
concat = tf.concat([flat_p, flat_e], 1)
lib.ops.linear_c.set_weights_stdev(0.02)
generated_labels = lib.ops.linear_c.Linear('Disc_c.Class', 4 * 4 * 8 * args.model_dim * 2, args.n_classes, concat)
lib.ops.linear_c.unset_weights_stdev()
correct_prediction = tf.equal(tf.argmax(generated_labels, 1), tf.argmax(real_labels, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# ---------------------
# LOSS Trainers
# ---------------------
cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(labels=real_labels, logits=generated_labels)
cross_entropy = tf.reduce_mean(cross_entropy)
w = lib.params_with_name('Disc_c.Class.W')[0]
reg = tf.nn.l2_loss(w)
cls_loss = cross_entropy + reg * args.wd
class_train_op = tf.train.AdamOptimizer(1e-4).minimize(cls_loss, var_list=lib.params_with_name('Disc_c.Class'))

# --------------
# Init session
# --------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())
best_class_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
# --------------
# Savers
# --------------
saver = tf.train.Saver(save_relative_paths=True)  # all session: gen + disc + class
best_saver = tf.train.Saver(save_relative_paths=True)
# para cargar entrenamiento
best_disc_saver_p = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Discriminator_p'))
best_disc_saver_e = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Discriminator_e'))
best_disc_saver_p.restore(session, save_path=BEST_DISC_SAVER_P)
best_disc_saver_e.restore(session, save_path=BEST_DISC_SAVER_E)
best_class_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))

# ----------
# Train loop
# ----------
if args.checkpoint_path is None:
    START_ITER = 0
else:
    START_ITER = args.start_iter
    saver.restore(session, save_path=args.checkpoint_path)
    print("\nStarting in iter={}:".format(START_ITER))

best_acc = 0.
it_best = 0
last_it = 0
t = time.time()
for iteration in range(START_ITER, args.n_iters):
    epoch = iteration * args.batch_size / n_train
    start_time = time.time()

    _train_acc_ac = 0.
    _cls_cost_ac = 0.
    _disc_cost_ac_p = 0.
    _disc_cost_ac_e = 0.
    for i in range(args.critic_iters):

        # Train Clasificador
        batch_xs, batch_ys = gen_train.next()
        batch_xs_aum = augm.random_transform(batch_xs)
        batch_xs_192 = crop_224_to_192(batch_xs_aum)

        batch_xs_e = resize_192_to_64(batch_xs_192)
        batch_xs_p = crop_192_to_64(batch_xs_192)
        _cls_cost, _, _train_acc = session.run([cls_loss, class_train_op, accuracy],
                                               feed_dict={all_real_data_conv_p: batch_xs_p,
                                                          all_real_data_conv_e: batch_xs_e,
                                                          real_labels: batch_ys})

        _train_acc_ac += _train_acc
        _cls_cost_ac += _cls_cost

    _cls_cost = _cls_cost_ac / args.critic_iters
    _train_acc = _train_acc_ac / args.critic_iters

    # ----------------------------------------------------
    # Log, Sample and Check
    # ----------------------------------------------------
    if iteration >= args.checkpoint_start_iter and iteration % args.checkpoint_iter == args.checkpoint_iter - 1:

        # Test accuracy
        _test_acc = 0.
        test_it = 0
        for test_it in range(test_batches):
            batch_xs_test, batch_ys_test = gen_test.next()
            #batch_xs_test_192 = cut_224_to_192(batch_xs_test)
            batch_xs_test_e = resize_192_to_64(batch_xs_test)
            batch_xs_test_p = crop_192_to_64(batch_xs_test)

            _test_acc += session.run(accuracy, feed_dict={all_real_data_conv_p: batch_xs_test_p,
                                                          all_real_data_conv_e: batch_xs_test_e,
                                                          real_labels: batch_ys_test})
        _test_acc = _test_acc / test_batches

        # Log and save best model
        if best_acc < _test_acc:
            best_acc = _test_acc
            it_best = iteration

            # Save best model
            #best_saver.save(sess=session, save_path=os.path.join(BEST_CHECKPOINTS_FOLDER_PATH, "best_iter"),
            #                       global_step=iteration + 1)
            best_class_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))

            # Log best model
            with open(LOG_FOLDER_PATH + '/best_acc.log', 'a') as f:
                np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size,
                                epoch, _train_acc, _test_acc]], fmt='%1.3e')

        with open(LOG_FOLDER_PATH + '/errors_p.log', 'a') as f:
            np.savetxt(f, [
                [iteration + 1, (iteration + 1) * args.batch_size, epoch, _cls_cost]],
                       fmt='%1.3e')
        with open(LOG_FOLDER_PATH + '/errors_e.log', 'a') as f:
            np.savetxt(f, [
                [iteration + 1, (iteration + 1) * args.batch_size, epoch, _cls_cost]],
                       fmt='%1.3e')
        with open(LOG_FOLDER_PATH + '/accuracies.log', 'a') as f:
            np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size, epoch, _train_acc,
                            _test_acc]], fmt='%1.3e')

        t2 = time.time() - t
        t += t2
        horas = t2 / (1 + iteration - last_it) / 3600. * 10000
        print "iter:%d/%d; epoch:%d; %4.2f horas. para 10000 iteraciones" % (iteration + 1, args.n_iters, epoch, horas)
        last_it = iteration + 1
        # saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter"), global_step=iteration + 1)

#saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter"), global_step=args.n_iters + 1)
print "Train gan ended!\n"
