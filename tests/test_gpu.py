import tensorflow as tf
from tensorflow.python.client import device_lib

sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))


def supports_device(device='CUDA'):
    if device == "CUDA":
        local_device_protos = device_lib.list_local_devices()
        return len([x.name for x in
                    local_device_protos if x.device_type == 'GPU']) > 0
    elif device == "CPU":
        return True
    return False

print supports_device()