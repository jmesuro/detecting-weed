import os
import sys
import time
import tflib.soja_dataset_labeled
import numpy as np
import tensorflow as tf
import tflib as lib
from gan import GeneratorAndDiscriminator
import argparse
import time
import datetime

# ------------------------------------------------------------------------
# Carga un clasificador entrenado y clasifica todos los datos etiquetados
# ------------------------------------------------------------------------
sys.path.append(os.getcwd())
parser = argparse.ArgumentParser()
parser.add_argument("--labelpath", type=str,
                    default='/home/jmesuro/datasets/soja_cifasis/labels_(8749, 2)_2_clases_float64_maleza_NO_ROBOT.npy')
parser.add_argument("--patchpath_l", type=str,
                    default='/home/jmesuro/datasets/soja_cifasis/parches_labeled_(8749, 96, 96, 3)_3_clases_uint8_NO_ROBOT.npy')
parser.add_argument("--labelpath_robot", type=str,
                    default='/home/jmesuro/datasets/soja_cifasis/labels_(2134, 2)_2_clases_float64_maleza_JUST_ROBOT.npy')
parser.add_argument("--patchpath_l_robot", type=str,
                    default='/home/jmesuro/datasets/soja_cifasis/parches_labeled_(2134, 96, 96, 3)_3_clases_uint8_JUST_ROBOT.npy')
parser.add_argument("--model_arch", type=str, default='dcgan')  # model arch: 'good', 'dcgan'
parser.add_argument("--mode", type=str, default='wgan-gp')  # mode: 'dcgan', 'wgan-gp'
parser.add_argument("--model_dim", type=int, default=16)  # How many iterations to train the critic for
parser.add_argument("--batch_size", type=int, default=194)  # Batch size. Must be a multiple of N_GPUS
parser.add_argument("--n_classes", type=int, default=2)  # How many classes to train for
parser.add_argument("--start_iter", type=int, default=0)  # Start iter num when restoring model
parser.add_argument("--lambda_gp", type=int, default=10)  # Gradient penalty lambda hyperparameter
parser.add_argument("--output_dim", type=int, default=64 * 64 * 3)  # Number of pixels in each image
parser.add_argument("--checkpoint_start_iter", type=int, default=0)
parser.add_argument("--checkpoint_iter", type=int, default=1)  # Save model vars every checkpoint_iter iterations
parser.add_argument("--checkpoint_path", type=str, default='/home/jmesuro/checkpoint/best_iter-127000')
parser.add_argument("--rand_seed", type=int, default=2017)  # Random seed
parser.add_argument("--wd", type=float, default=0.01)  # Weight decay
parser.add_argument("--tr_step", type=float, default=0.1)  # threshold step size
args = parser.parse_args()

# -----------------------
# Build folders and files
# -----------------------
OUT_PATH = os.getcwd() + '/out/'
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)

DATE = (time.strftime("%Y_%m_%d")) + '_' + str(datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S'))
THIS_RUN_PATH = OUT_PATH + DATE + '_class_roc_tf'

LOG_FOLDER_PATH = THIS_RUN_PATH + '/logs/'

os.mkdir(THIS_RUN_PATH)

if not os.path.exists(LOG_FOLDER_PATH):
    os.mkdir(LOG_FOLDER_PATH)

with open(LOG_FOLDER_PATH + '/roc.log', 'w') as f:
    f.write('# thres   acc       tp        fp        tn        fn        tpr       fpr')
    f.write('\n')


# -------------------------------------
# Create the model
# -------------------------------------
_, Discriminator = GeneratorAndDiscriminator(model_arch='dcgan')
all_real_data_conv = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])
real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [args.batch_size, args.output_dim])
real_labels = tf.placeholder(tf.float32, [args.batch_size, args.n_classes])
disc_real, generated_labels = Discriminator(real_data, n_classes=args.n_classes, model_dim=args.model_dim)

generated_labels_sig = tf.nn.softmax(generated_labels)
x_thresholds = tf.placeholder(tf.float32, shape=args.batch_size)

pred_label_m = tf.greater_equal((generated_labels_sig[:, 0]), x_thresholds)  # si es mayor al tr es maleza
real_label_m = tf.equal(real_labels[:, 0], [1] * args.batch_size)  # es maleza?
real_label_not_m = tf.not_equal(real_labels[:, 0], [1] * args.batch_size)

correct_prediction = tf.equal(real_label_m, pred_label_m)
miss_prediction = tf.not_equal(real_label_m, pred_label_m)

true_positive = tf.logical_and(correct_prediction, real_label_m)  # predice bien y era maleza
false_negative = tf.logical_and(miss_prediction, real_label_m)  # predice mal y era maleza
true_negative = tf.logical_and(correct_prediction, real_label_not_m)  # predice bien y no era maleza
false_positive = tf.logical_and(miss_prediction, real_label_not_m)  # predice mal y no rea maleza

tp = tf.reduce_sum(tf.cast(true_positive, tf.float32))
fn = tf.reduce_sum(tf.cast(false_negative, tf.float32))
fp = tf.reduce_sum(tf.cast(false_positive, tf.float32))
tn = tf.reduce_sum(tf.cast(true_negative, tf.float32))

acc = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# ------------------
# Get data iterators
# ------------------
n_patches, data_gen = lib.soja_dataset_labeled.load_all_data_robot(args.patchpath_l_robot, args.labelpath_robot,
                                                                   args.batch_size)


def inf_data_gen():
    while True:
        for images, targets in data_gen():
            yield images, targets


gen_data = inf_data_gen()
t = time.time()

# ----------------------
# Restore trained model
# ----------------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())
disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
disc_saver.restore(session, save_path=args.checkpoint_path)

n_thresholds = int(1 + 1 / args.tr_step)

for tr in range(0, n_thresholds):
    np_a = np.array([tr * args.tr_step] * args.batch_size, dtype=np.float32)

    ac_tpr = 0.
    ac_fpr = 0.
    ac_acc = 0.
    ac_tp = 0.
    ac_fp = 0.
    ac_tn = 0.
    ac_fn = 0.
    for iteration in range(0, n_patches / args.batch_size):
        batch_xs, batch_ys = gen_data.next()
        batch_xs = batch_xs
        t = time.time()

        _generated_labels_sig, _tp, _tn, _fp, _fn, _acc = \
            session.run([generated_labels_sig, tp, tn, fp, fn, acc],
                        feed_dict={all_real_data_conv: batch_xs, real_labels: batch_ys, x_thresholds: np_a})

        print time.time() - t

        ac_acc += _acc
        ac_tp += _tp
        ac_fp += _fp
        ac_tn += _tn
        ac_fn += _fn

    mean_acc = ac_acc / iteration
    TPR = ac_tp / (ac_tp + ac_fn)
    FPR = ac_fp / (ac_fp + ac_tn)

    with open(LOG_FOLDER_PATH + '/roc.log', 'a') as f:
        np.savetxt(f, [[tr * args.tr_step, mean_acc, ac_tp, ac_fp, ac_tn, ac_fn, TPR, FPR]], fmt='%1.3e')

print "class ended!\n"
