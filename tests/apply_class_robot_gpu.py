import h5py
import os
import numpy as np
import tensorflow as tf
import tflib as lib
from gan import GeneratorAndDiscriminator
import time
from PIL import Image

# ------------------------------------------------------------------------
# Carga un clasificador entrenado y clasifica todos los datos etiquetados
# ------------------------------------------------------------------------

checkpoint_path = '/home/jmesuro/chkpoint/best_iter-73100'
model_arch = 'dcgan'
n_images = 26
n_classes = 2
output_dim = 64*64*3
c = 3
batch_size = 207  # es igual a la cantidad de parchs por imagen
H = 576
W = 1472
h = 64
w = 64


def list_img_files(directory):
    files = []
    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            path = os.path.abspath(os.path.join(dirpath, f))
            if os.path.isfile(path) and (".jpg" in f or ".png" in f):
                files.append(path)
    return files, len(files)


def im_to_array(path):
    im = Image.open(path)
    a = np.asarray(im)[504:, 223:223 + 1472]
    return a.transpose(2, 0, 1)


paths, _ = list_img_files('/home/jmesuro/videos_soja/robot_videos_MOV/test3/')
ims_np = [im_to_array(path) for path in paths]

# REAL DATA
images_crudas_t = tf.placeholder(tf.uint8, shape=[c, H, W])  # imagen entera cHN
a1 = tf.reshape(images_crudas_t, shape=[c, H / h, h, W / w, w])
a2 = tf.transpose(a1, perm=[1, 3, 0, 2, 4])
all_real_data_conv = tf.reshape(a2, shape=[H / h * W / w, c, h, w])
real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [batch_size, output_dim])

# REAL LABEL
real_labels = tf.placeholder(tf.float32, [batch_size, n_classes])

_, Discriminator = GeneratorAndDiscriminator(model_arch)
disc_real, generated_labels, _ = Discriminator(real_data, n_classes=n_classes)
correct_prediction = tf.equal(tf.argmax(generated_labels, 1), tf.argmax(real_labels, 1))

# ----------------------
# Restore trained model
# ----------------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())
disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
disc_saver.restore(session, save_path=checkpoint_path)

all_generated_labels = []
ims_asarrays = []
i = 0
t = time.time()

for iteration in range(0, n_images):
    batch_xs = ims_np[iteration]

    _generated_labels, _all_real_data_conv = session.run([generated_labels, all_real_data_conv],
                                                         feed_dict={images_crudas_t: batch_xs})

    all_generated_labels.append(_generated_labels)
    ims_asarrays.append(_all_real_data_conv)

i = i + 1
t = time.time()
print time.time() - t

OUT_PATH = '/home/jmesuro/videos_soja/robot_videos_MOV/test3/'
fp = h5py.File(OUT_PATH + 'parches_ROIT2frames_robot_u_m.h5', 'w')
fp.create_dataset('X', (207 * n_images, 3, 64, 64), dtype=np.int8)

final_labels = []
i_batch = 0
i = 0
for i_label_batch in all_generated_labels:
    j = 0
    ar = ims_asarrays[i_batch]
    for i_label in i_label_batch:
        final_labels.append(i_label.argmax())
        r = np.array(ar[j], dtype=np.uint8)

        if i_label.argmax() == 1:
            r[1, :, :] = 0
            r[2, :, :] = 0
        fp['X'][i] = r

        i = i + 1
        j = j + 1
    i_batch = i_batch + 1

fp.close()
print "class ended!\n"
