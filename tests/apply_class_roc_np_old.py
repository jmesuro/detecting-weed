import os
import sys
import tflib.soja_dataset_labeled
import numpy as np
import tensorflow as tf
import tflib as lib
from gan import GeneratorAndDiscriminator
import argparse
import time
import datetime
from numpy import trapz

# ------------------------------------------------------------------------
# Carga un clasificador entrenado y datasets etiquetados
# y da como salida una cuva rock  moviendo el trheshold de a tr_step
# ------------------------------------------------------------------------


sys.path.append(os.getcwd())
parser = argparse.ArgumentParser()

parser.add_argument("--labels", type=str,
                    default='/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/datasets/soja/labeled_96/'
                            'labels_(1230,3)_3_clases_float64_ROB_BALANCEADO.npy')
parser.add_argument("--parches", type=str,
                    default='/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/datasets/soja/labeled_96/'
                            'parches_labeled_(1230,96,96,3)_uint8_ROB_BALANCEADO.npy')

parser.add_argument("--model_arch", type=str, default='dcgan')       # model arch: 'good', 'dcgan'
parser.add_argument("--model_dim", type=int, default=4)             # model size
parser.add_argument("--batch_size", type=int, default=128)

parser.add_argument("--n_classes", type=int, default=3)
parser.add_argument("--output_dim", type=int, default=64 * 64 * 3)
parser.add_argument("--checkpoint_path", type=str, default='/home/joaquin/checkpoints/best_iter_c-12300')     # path to save the check for continue training
parser.add_argument("--tr_step", type=float, default=0.01)  # threshold step size

args = parser.parse_args()

# -----------------------
# Build folders and files
# -----------------------
OUT_PATH = os.getcwd() + '/out/'
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)

DATE = (time.strftime("%Y_%m_%d")) + '_' + str(datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S'))
THIS_RUN_PATH = OUT_PATH + DATE + '_class_roc_np'

LOG_FOLDER_PATH = THIS_RUN_PATH + '/logs/'

os.mkdir(THIS_RUN_PATH)

if not os.path.exists(LOG_FOLDER_PATH):
    os.mkdir(LOG_FOLDER_PATH)

with open(LOG_FOLDER_PATH + '/roc.log', 'w') as f:
    f.write('# thres   acc       tp        fp        tn        fn        tpr       fpr')
    f.write('\n')

# -------------------------------------
# Create the model
# -------------------------------------
_, Discriminator = GeneratorAndDiscriminator(model_arch='dcgan')
all_real_data_conv = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])
real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [args.batch_size, args.output_dim])
real_labels = tf.placeholder(tf.float32, [args.batch_size, args.n_classes])
disc_real, generated_labels = Discriminator(real_data, n_classes=args.n_classes, model_dim=args.model_dim)

generated_labels_sig = tf.nn.softmax(generated_labels)
x_thresholds = tf.placeholder(tf.float32, shape=args.batch_size)

# ------------------
# Get data iterators
# ------------------
all_parches, all_labels = lib.soja_dataset_labeled.load_all_dataset(args.parches, args.labels)
n_patches = all_labels.shape[0]


def get_next(it, all_parches=all_parches, all_labels=all_labels, bs=args.batch_size):
    return all_parches[it * bs:(it + 1) * bs, :, :, :], all_labels[it * bs:(it + 1) * bs, :]


t = time.time()

# ----------------------
# Restore trained model
# ----------------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())
disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
disc_saver.restore(session, save_path=args.checkpoint_path)
n_thresholds = int(1 + 1 / args.tr_step)

tpr_list = []
fpr_list = []
p_lucas_list = []
for tr in range(0, n_thresholds):
    all_generated_labels = []
    all_real_labels = []

    np_a = np.array([tr * args.tr_step] * args.batch_size, dtype=np.float32)

    for iteration in range(0, n_patches / args.batch_size):
        batch_xs, batch_ys = get_next(iteration)

        t = time.time()

        _generated_labels_sig = session.run([generated_labels_sig],
                                            feed_dict={all_real_data_conv: batch_xs, real_labels: batch_ys})

        all_generated_labels.extend(np.ndarray.tolist(_generated_labels_sig[0][:, 0]))
        all_real_labels.extend(np.ndarray.tolist(batch_ys[:, 0]))

        print time.time() - t

    malezas_generadas = [a >= (tr * args.tr_step) for a in all_generated_labels]
    malezas_reales = [int(a) == 1 for a in all_real_labels]

    tp = sum([mg and mr for (mg, mr) in zip(malezas_generadas, malezas_reales)])
    fp = sum([mg and (not mr) for (mg, mr) in zip(malezas_generadas, malezas_reales)])
    tn = sum([(not mg) and (not mr) for (mg, mr) in zip(malezas_generadas, malezas_reales)])
    fn = sum([(not mg) and mr for (mg, mr) in zip(malezas_generadas, malezas_reales)])
    acc = np.float32(sum([mg == mr for (mg, mr) in zip(malezas_generadas, malezas_reales)])) / n_patches

    tpr = np.float32(tp) / (np.float32(tp) + np.float32(fn))
    fpr = np.float32(fp) / (np.float32(fp) + np.float32(tn))
    p_lucas = (np.float32(tp) + np.float32(fp)) / (np.float32(tp) + np.float32(fp) + np.float32(tn) + np.float32(fn))

    tpr_list.append(tpr)
    fpr_list.append(fpr)
    p_lucas_list.append(p_lucas)

    with open(LOG_FOLDER_PATH + '/roc.log', 'a') as f:
        np.savetxt(f, [[tr * args.tr_step, acc, tp, fp, tn, fn, tpr, fpr]], fmt='%1.3e')

    with open(LOG_FOLDER_PATH + '/roc_lucas.log', 'a') as f:
        np.savetxt(f, [[tr * args.tr_step, acc, tp, fp, tn, fn, tpr, p_lucas]], fmt='%1.3e')


y = np.asarray(tpr_list, dtype=np.float64)
x1 = np.asarray(fpr_list, dtype=np.float64)
x2 = np.asarray(p_lucas_list, dtype=np.float64)
print 'AUR-roc' + str(trapz(y=y, x=x1))
print 'AUR-roc_lucas' + str(trapz(y=y, x=x2))
print "class ended!\n"
