import os
import numpy as np
import tensorflow as tf
import tflib as lib
from gan import GeneratorAndDiscriminator
from gan import GeneratorAndDiscriminator, GeneratorAndDiscriminator_pe
import time
import tflib.ops.linear_c
from tflib.soja_dataset_labeled import resize_192_to_64 as resize_192_to_64
from PIL import Image

"""Carga un clasificador entrenado y clasifica todos los datos etiquetados

Leave one blank line.  The rest of this docstring should contain an
overall description of the module or program.  Optionally, it may also
contain a brief description of exported classes and functions and/or usage
examples.

  Typical usage example:

  foo = ClassFoo()
  bar = foo.FunctionBar()
"""


# ------------------------------------------------------------------------
# Carga un clasificador entrenado y clasifica todos los datos etiquetados
# input: clasificador + folder imagenes + folder output + threshold
# clasifica un ROI en cada imagenes parches de tamanio h=w, las marca
# y las guarda en folder output
# ------------------------------------------------------------------------

checkpoint_path = '/home/jmesuro/checkpoints/best_iter_class-66600'  # path del clasificador entrenado, ojo tamnio gan
images_path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left/'  # path images a clasificar
output_marked_images_path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_[enrRRR_88]/'  # path imagenes clasificadaas
log_folder = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_marked/'



if not os.path.exists(output_marked_images_path):
    os.mkdir(output_marked_images_path)
if not os.path.exists(log_folder):
    os.mkdir(log_folder)

model_arch = 'dcgan'
threshold = 0.80

model_dim = 16
n_classes = 3
h = w = 64
h_e = w_e = h * 3

c = 3
output_dim = h * w * c
batch_size = 207  # cantidad de parches que entran en un ROI, es decir en una imagen
parches_w = 23  # cantidad de parches por ancho del ROI
parches_h = 9  # cantidad de parches por alto del ROI

#     W 1920
# ----------------------------------
# |                                |H 1080
# |          W_ROI 1472            |
# |       -----------------        |
# |       | ROI           |H_ROI   |
# |       |               |576     |
# |       |               |        |
# |marg_l |               |marg_l  |
# ----------------------------------

H = 1080
W = 1920
H_ROI = 576
W_ROI = 1472
H_ROI_e = 576 + h
W_ROI_e = 1472 + w + w
marg_l = (W - W_ROI) / 2 - 1
marg_s = H - H_ROI

marg_l_e = (W - W_ROI_e) / 2 - 1
marg_s_e = H - H_ROI_e

# Se arma una lista de longitud batch_size. Con las coordenadas de cada parche
# coord_l[0] es el parche de arriba a la izquierda, coord_l[parches_h-1] es el de arriba a la derecha
# coord_l[batch_size-1] es el de abajo a la derecha.
# coord_l[0] = [ x ,y ]
# x crece a la derecha, y y de arrua
coord_l = []
for i in range(0,parches_h):
    for j in range(0,parches_w):
        par = [ j*h + marg_l , i*h + marg_s  - h ]
        coord_l.append(par)


# -----------------------------------------------------------
# marca los bordes de rojo de un parche de 64x64
# -----------------------------------------------------------
def mark_patch(patch):
    borde = 3
    patch[0, 0:h - 1, 0:borde] = 255
    patch[1, 0:h - 1, 0:borde] = 0
    patch[2, 0:h - 1, 0:borde] = 0

    patch[0, 0:h - 1, h - borde - 1:h - 1] = 255
    patch[1, 0:h - 1, h - borde - 1:h - 1] = 0
    patch[2, 0:h - 1, h - borde - 1:h - 1] = 0

    patch[0, 0:borde, 0:h - 1] = 255
    patch[1, 0:borde, 0:h - 1] = 0
    patch[2, 0:borde, 0:h - 1] = 0

    patch[0, h - borde - 1:h - 1, 0:h - 1] = 255
    patch[1, h - borde - 1:h - 1, 0:h - 1] = 0
    patch[2, h - borde - 1:h - 1, 0:h - 1] = 0
    return patch


# -----------------------------------------------------------
# marca los bordes de rojo de un parche de 64x64
# -----------------------------------------------------------
def mark_patch_rojo(patch,  p):
    patch[0, 0:h - 1, 0:h-1] = p * 255
    return patch

# -----------------------------------------------------------
# patch_list: lista de parches correspondiente a un ROI
# arma el ROI con los parches y lo marca con un marco negro
# -----------------------------------------------------------
def make_roi(patch_list):
    b = np.zeros(shape=(H_ROI, W_ROI, c), dtype=np.uint8)
    for p in range(0, parches_h):
        for q in range(0, parches_w):
            im = patch_list.pop()
            im = im.transpose(1, 2, 0)  # channel last
            b[h * p:h * (p + 1), h * q:h * (q + 1), :] = im

    borde = 5
    b[0:H_ROI - 1, 0:borde, 0] = 0
    b[0:H_ROI - 1, 0:borde, 1] = 0
    #b[0:H_ROI - 1, 0:borde, 2] = 0

    b[0:H_ROI - 1, W_ROI - borde - 1:W_ROI - 1, 0] = 0
    b[0:H_ROI - 1, W_ROI - borde - 1:W_ROI - 1, 1] = 0
    #b[0:H_ROI - 1, W_ROI - borde - 1:W_ROI - 1, 2] = 0

    b[0:borde, 0:W_ROI - 1, 0] = 0
    b[0:borde, 0:W_ROI - 1, 1] = 0
    #b[0:borde, 0:W_ROI - 1, 2] = 0


    return b


# -----------------------------------------------------------
# patch_list: lista de parches correspondiente a un ROI
# arma el ROI con los parches y lo marca con un marco negro
# -----------------------------------------------------------
def make_roi_ent(patch_list):
    b = np.zeros(shape=(H_ROI, W_ROI, c), dtype=np.uint8)
    for p in range(0, parches_h):
        for q in range(0, parches_w):
            im = patch_list.pop()
            im = im.transpose(1, 2, 0)  # channel last
            b[h * p:h * (p + 1), h * q:h * (q + 1), :] = im

    borde = 5
    b[0:H_ROI - 1, 0:borde, 0] = 0
    b[0:H_ROI - 1, 0:borde, 1] = 0
    #b[0:H_ROI - 1, 0:borde, 2] = 0

    b[0:H_ROI - 1, W_ROI - borde - 1:W_ROI - 1, 0] = 0
    b[0:H_ROI - 1, W_ROI - borde - 1:W_ROI - 1, 1] = 0
    #b[0:H_ROI - 1, W_ROI - borde - 1:W_ROI - 1, 2] = 0

    b[0:borde, 0:W_ROI - 1, 0] = 0
    b[0:borde, 0:W_ROI - 1, 1] = 0
    #b[0:borde, 0:W_ROI - 1, 2] = 0

    b[H_ROI - borde - 1:H_ROI - 1, 0:W_ROI - 1, 0] = 0
    b[H_ROI - borde - 1:H_ROI - 1, 0:W_ROI - 1, 1] = 0

    return b

# -------------------------------------------------
# labels: etiquetas correspondientes a parche_list
# parche_list: parches de un ROI
# arma una imagen con los parches marcados
# -------------------------------------------------
def make_marked_image(labels, parches_list):
    roi_l = []
    i_parche = 0

    for label in labels:

        parche = np.array(parches_list[i_parche], dtype=np.uint8)
        label = 1 - label
        parche[1, :, :] = parche[1, :, :] * label
        parche[2, :, :] = parche[2, :, :] * label

        if (1 - label) > threshold:  # si es maleza
            parche = mark_patch_rojo(parche,1-label)
        # else:

        roi_l.append(parche)
        i_parche = i_parche + 1

    roi_l.reverse()
    b = make_roi(roi_l)
    a = np.zeros(shape=(H, W, c), dtype=np.uint8)
    a[:, :, :] = np.asarray(im_original)
    a[marg_s:, marg_l:marg_l + W_ROI] = b
    im = Image.fromarray(a)
    return im

# -------------------------------------------------
# labels: etiquetas correspondientes a parche_list
# parche_list: parches de un ROI
# arma una imagen con los parches marcados
# -------------------------------------------------
def make_marked_image2(labels, parches_list, tr):
    roi_l = []
    i_parche = 0

    for label in labels:
        parche = np.array(parches_list[i_parche], dtype=np.uint8)
        if label[0] >= tr:  # si es maleza supera el tr
            parche = mark_patch(parche)
        roi_l.append(parche)
        i_parche = i_parche + 1

    roi_l.reverse()
    b = make_roi(roi_l)
    a = np.zeros(shape=(H, W, c), dtype=np.uint8)
    a[:, :, :] = np.asarray(im_original)
    a[marg_s: , marg_l:marg_l + W_ROI] = b
    im = Image.fromarray(a)
    return im

# -------------------------------------------------
# labels: etiquetas correspondientes a parche_list
# parche_list: parches de un ROI
# arma una imagen con los parches marcados
# -------------------------------------------------
def make_marked_image2_ent(labels, parches_list, tr):
    roi_l = []
    i_parche = 0

    for label in labels:
        parche = np.array(parches_list[i_parche], dtype=np.uint8)
        if label[0] >= tr:  # si es maleza supera el tr
            parche = mark_patch(parche)
        roi_l.append(parche)
        i_parche = i_parche + 1

    roi_l.reverse()
    b = make_roi_ent(roi_l)
    a = np.zeros(shape=(H, W, c), dtype=np.uint8)
    a[:, :, :] = np.asarray(im_original)
    a[marg_s - h: -h , marg_l:marg_l + W_ROI] = b
    im = Image.fromarray(a)
    return im

# -------------------------------------------------
# labels: etiquetas correspondientes a parche_list
# parche_list: parches de un ROI
# arma una imagen con los parches marcados
# -------------------------------------------------
def make_marked_image3(labels, parches_list, tr):
    roi_l = []
    i_parche = 0

    for label in labels:
        parche = np.array(parches_list[i_parche], dtype=np.uint8)

        parche = mark_patch_rojo(parche,label[0])
        roi_l.append(parche)
        i_parche = i_parche + 1

    roi_l.reverse()
    b = make_roi(roi_l)
    a = np.zeros(shape=(H, W, c), dtype=np.uint8)
    a[:, :, :] = np.asarray(im_original)
    a[marg_s:, marg_l:marg_l + W_ROI] = b
    im = Image.fromarray(a)
    return im


def make_patches_list_p(roi,H,W):
    l = []
    for i in range(0, H, h):
        for j in range (0, W, w):
            l.append( roi[i:i+h, j:j+w,:] )

    ims = np.array(l,dtype=np.uint8)
    return ims


def make_patches_list_e(roi,H,W):
    l = []
    for i in range(0, H-h, h):
        for j in range (0, W-w*2, w):
            l.append( roi[i:i+h*3, j:j+w*3,:] )

    ims = np.array(l,dtype=np.uint8)
    return ims


with open(log_folder + '/time.log', 'w') as f:
    f.write('\n')


# ----------------
# Create the model
# ----------------
_, _, Discriminator_p, Discriminator_e = GeneratorAndDiscriminator_pe(model_arch='dcgan')


batch_in_p = tf.placeholder(tf.uint8, shape=[207, 64, 64, 3])
all_real_data_conv_p = tf.transpose(batch_in_p, perm=[0,3,1,2])

batch_in_e = tf.placeholder(tf.uint8, shape=[207, 3, 64, 64])
all_real_data_conv_e = batch_in_e


real_data_p = tf.reshape(2 * ((tf.cast(all_real_data_conv_p, tf.float32) / 255.) - .5),
                         [batch_size, output_dim])
real_data_e = tf.reshape(2 * ((tf.cast(all_real_data_conv_e, tf.float32) / 255.) - .5),
                         [batch_size, output_dim])


disc_real_p, flat_p = Discriminator_p(real_data_p, model_dim=model_dim)
disc_real_e, flat_e = Discriminator_e(real_data_e, model_dim=model_dim)

real_labels = tf.placeholder(tf.float32, [batch_size, n_classes])

# --------------------------
# CONCAT and CLASS
# --------------------------
concat = tf.concat([flat_p, flat_e], 1)
lib.ops.linear_c.set_weights_stdev(0.02)
generated_labels = lib.ops.linear_c.Linear('Disc_c.Class', 4 * 4 * 8 * model_dim * 2, n_classes, concat)
lib.ops.linear_c.unset_weights_stdev()
correct_prediction = tf.equal(tf.argmax(generated_labels, 1), tf.argmax(real_labels, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))


generated_labels_sof = tf.nn.softmax(generated_labels)


# ----------------------
# Restore trained model
# ----------------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())
disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
disc_saver.restore(session, save_path=checkpoint_path)


ims_asarrays = []
i_image = 0
# labels = []



def int_to_string(i):
    if i < 10:
        return '0000' + str(i)
    if i >= 10 and i < 100:
        return '000' + str(i)
    if i >= 100 and i < 1000:
        return '00' + str(i)
    if i >= 1000 and i < 10000:
        return '0' + str(i)
    return str(i)


file = open(log_folder + "log.txt","w")
tiempos2 = []
tiempos1 = []

for dirpath, _, filenames in os.walk(images_path):
    for f in filenames:
        path = os.path.abspath(os.path.join(dirpath, f))

        if not os.path.exists(output_marked_images_path + 'm_' + f[:-3] + 'jpg'):

            #im_original_cv = (cv2.imread(path))[:,:,[2,1,0]] # bgr to rgb
            #roi_original = im_original_cv[marg_s - h:-h, marg_l: marg_l + W_ROI]  # obtengo el ROI de la imagen
            #roi_entornos = im_original_cv[marg_s_e - h:, marg_l_e: marg_l_e + W_ROI_e]  # obtengo el ROI para entornos

            im_original = Image.open(path)
            roi_original = np.asarray(im_original)[marg_s - h:-h, marg_l: marg_l + W_ROI]  # obtengo el ROI de la imagen
            roi_entornos = np.asarray(im_original)[marg_s_e - h:, marg_l_e: marg_l_e + W_ROI_e]  # obtengo el ROI para entornos

            bs_p = make_patches_list_p(roi_original,H_ROI,W_ROI)
            bs_e_192 = make_patches_list_e(roi_entornos, H_ROI_e, W_ROI_e)

            bs_e_192 = np.transpose(bs_e_192,  (0,3,1,2))

            bs_e = resize_192_to_64(bs_e_192)

            t = time.time()
            _generated_labels, parches = session.run([generated_labels_sof, all_real_data_conv_p],
                                                     feed_dict={batch_in_p: bs_p,batch_in_e: bs_e })

            t1 = time.time() - t

            im = make_marked_image2_ent(_generated_labels, parches, tr=threshold)  # marco la imagen
            im.save(output_marked_images_path + 'm_' + f[:-3] + 'jpg')

            i_image = i_image + 1
            # labels.append(_generated_labels)
            t2 = time.time() - t
            tiempos2.append(t2)
            tiempos1.append(t1)
            l = 'imagen numero ' + str(i_image) + ' tiempo1 ' + str(t1) + ' tiempo2 ' + str(t2)

            print l
            file.write(l + '\n')


print "class ended!\n"
#file.write( 'promedio tiempos1, con escritura en disco: ' + str(sum(tiempos1) / len(tiempos1)) + '\n')
#file.write( 'promedio tiempos2, con escritura en disco: ' + str(sum(tiempos2) / len(tiempos2)))
file.close()