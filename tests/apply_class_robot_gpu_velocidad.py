import os
import numpy as np
import tensorflow as tf
import tflib as lib
from gan import GeneratorAndDiscriminator
import time
from PIL import Image

# ------------------------------------------------------------------------
# Carga un clasificador entrenado y clasifica todos los datos etiquetados
# ------------------------------------------------------------------------

checkpoint_path = '/home/jmesuro/chkpoint/best_iter-400'  # path del clasificador entrenado, ojo tamnio gan
images_path = '/home/jmesuro/videos_soja/robot_videos_MOV/test3/originals/'  # path images a clasificar
output_marked_images_path = '/home/jmesuro/videos_soja/robot_videos_MOV/test3/marked/'  # path imagenes clasificadaas
model_arch = 'dcgan'
n_classes = 2
h = w = 64
c = 3
output_dim = h * w * c
batch_size = 414  # cantidad de parches que entran en un ROI, es decir en una imagen
parches_w = 23  # cantidad de parches por ancho del ROI
parches_h = 9  # cantidad de parches por alto del ROI

#     W 1920
# ----------------------------------
# |                                |H 1080
# |          W_ROI 1472            |
# |       -----------------        |
# |       | ROI           |H_ROI   |
# |       |               |576     |
# |       |               |        |
# |marg_l |               |marg_l  |
# ----------------------------------

H = 1080
W = 1920
H_ROI = 576
W_ROI = 1472
marg_l = (W-W_ROI)/2 - 1
marg_s = H - H_ROI


# -----------------------------------------------------------
# marca los bordes de rojo de un parche de 64x64
# -----------------------------------------------------------
def mark_patch(patch):
    borde = 5
    patch[1, 0:h-1, 0:borde] = 0
    patch[2, 0:h-1, 0:borde] = 0
    patch[1, 0:h-1, h-borde-1:h-1] = 0
    patch[2, 0:h-1, h-borde-1:h-1] = 0
    patch[1, 0:borde, 0:h-1] = 0
    patch[2, 0:borde, 0:h-1] = 0
    patch[1, h-borde-1:h-1, 0:h-1] = 0
    patch[2, h-borde-1:h-1, 0:h-1] = 0
    return patch


# -----------------------------------------------------------
# patch_list: lista de parches correspondiente a un ROI
# arma el ROI con los parches y lo marca con un marco negro
# -----------------------------------------------------------
def make_roi(patch_list):
    b = np.zeros(shape=(H_ROI, W_ROI, c), dtype=np.uint8)
    for p in range(0, parches_h):
        for q in range(0, parches_w):
            im = patch_list.pop()
            im = im.transpose(1, 2, 0)  # channel last
            b[h * p:h * (p + 1), h * q:h * (q + 1), :] = im

    borde = 5
    b[0:H_ROI-1, 0:borde, 0] = 0
    b[0:H_ROI-1, 0:borde, 1] = 0
    b[0:H_ROI-1, 0:borde, 2] = 0

    b[0:H_ROI-1, W_ROI-borde-1:W_ROI-1, 0] = 0
    b[0:H_ROI-1, W_ROI-borde-1:W_ROI-1, 1] = 0
    b[0:H_ROI-1, W_ROI-borde-1:W_ROI-1, 2] = 0

    b[0:borde, 0:W_ROI-1, 0] = 0
    b[0:borde, 0:W_ROI-1, 1] = 0
    b[0:borde, 0:W_ROI-1, 2] = 0

    return b


# -------------------------------------------------
# labels: etiquetas correspondientes a parche_list
# parche_list: parches de un ROI
# arma una imagen con los parches marcados
# -------------------------------------------------
def make_marked_image(labels, parches_list):
    roi_l = []
    i_parche = 0

    for label in labels:
        parche = np.array(parches_list[i_parche], dtype=np.uint8)
        if label > 0:  # si es maleza
            parche = mark_patch(parche)
        roi_l.append(parche)
        i_parche = i_parche + 1

    roi_l.reverse()
    b = make_roi(roi_l)
    a = np.zeros(shape=(H, W, c), dtype=np.uint8)
    a[:, :, :] = np.asarray(im_original)
    a[marg_s:, marg_l:marg_l + W_ROI] = b
    im = Image.fromarray(a)
    return im


# ---------------------------------------------------
# Armo el clasificador, toma de una imagen a la vez
# ---------------------------------------------------
images_crudas_t = tf.placeholder(tf.uint8, shape=[c, H_ROI, W_ROI])  # imagen entera cHN
a1 = tf.reshape(images_crudas_t, shape=[c, H_ROI / h, h, W_ROI / w, w])
a2 = tf.transpose(a1, perm=[1, 3, 0, 2, 4])
all_real_data_conv = tf.reshape(a2, shape=[H_ROI / h * W_ROI / w, c, h, w])
real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [batch_size, output_dim])
_, Discriminator = GeneratorAndDiscriminator(model_arch)
disc_real, generated_labels = Discriminator(real_data, n_classes=n_classes)
# ----------------------
# Restore trained model
# ----------------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())
disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
disc_saver.restore(session, save_path=checkpoint_path)

tiempos = []
i_image = 0
for dirpath, _, filenames in os.walk(images_path):
    for f in filenames:
        path = os.path.abspath(os.path.join(dirpath, f))
        if os.path.isfile(path) and (".png" in f):

            im_original = Image.open(path)
            roi_original = np.asarray(im_original)[H - H_ROI:, marg_l: marg_l + W_ROI]  # obtengo el ROI de la imagen
            batch_xs = roi_original.transpose(2, 0, 1)  # channel first



            t = time.time()
            _generated_labels = session.run([generated_labels], feed_dict={images_crudas_t: batch_xs})
            t1 = time.time() - t
            tiempos.append(t1)
            i_image = i_image + 1

            print('imagen numero ' + str(i_image) + ' tiempo1 ' + str(t1) + ' tiempo2 ' + str(time.time() - t))
print "class ended!\n"
print('tiempo medio ' + str(np.mean((tiempos))))