import os
import sys
import time
import tflib.soja_dataset_labeled
import tflib.soja_dataset_unlabeled
from tflib.soja_dataset_labeled import crop_96_to_64 as to_64
from tflib.soja_dataset_labeled import crop_224_to_64 as to_64_from_224
import numpy as np
import tensorflow as tf
import tflib as lib
import tflib.save_images
from gan import Only_Discriminator_comp_gan
import argparse
import datetime
import random
from tflib.augmentation import Augmentation
import tflib.ops.linear_c

# tf.enable_eager_execution()
# ------------------------------------
#  classifier + comparator (no es gan, es solo disc)
# el comparator es un softmax al final domo el clasificador
# entrena con las no etiquetadas
# ------------------------------------
sys.path.append(os.getcwd())

parser = argparse.ArgumentParser()

parser.add_argument("--labels", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ALL_IDX/'
                            'labels_(14031,3)_3_clases_float64_BALANCEADO_TRAIN.npy')

parser.add_argument("--parches", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ALL_IDX/'
                            'parches_labeled_(14031,96,96,3)_uint8_BALANCEADO_TRAIN.npy')


parser.add_argument("--parches_u_fot", type=str,
                    default='/home/jmesuro/dataset'
                            '/parches_fotografos_u_1200878_3_224_224_utin8.h5')

# unlabeled dataset
parser.add_argument("--parches_u_robot", type=str,
                    default='/home/jmesuro/dataset'
                            '/parches_robot_u_376200_3_224_244_uint8.h5')

parser.add_argument("--model_arch", type=str, default='dcgan')  # model arch: 'good', 'dcgan'
parser.add_argument("--model_dim", type=int, default=32)  # model size
parser.add_argument("--batch_size", type=int, default=128)
parser.add_argument("--n_iters", type=int, default=100000)
parser.add_argument("--n_classes", type=int, default=3)
parser.add_argument("--start_iter", type=int, default=0)  # for continue training from chechpoint
parser.add_argument("--lambda_gp", type=int, default=10)  # wgan param
parser.add_argument("--output_dim", type=int, default=64 * 64 * 3)
parser.add_argument("--checkpoint_start_iter", type=int, default=0)  # iter from where to start to check
parser.add_argument("--valid_factor", type=int, default=200)  # to many validation batches, factor to reduce
parser.add_argument("--test_factor", type=int, default=200)  # to many validation batches, factor to reduce1
parser.add_argument("--checkpoint_iter", type=int, default=1000)  # freq to validate
parser.add_argument("--checkpoint_gen_im", type=int, default=1000)  # freq to gen im
parser.add_argument("--checkpoint_path", type=str, default=None)  # path to save the check for continue training
parser.add_argument("--rand_seed", type=int, default=2017)
parser.add_argument("--wd", type=float, default=0.01)
parser.add_argument("--back_class", type=str, default='no')  # indicate the deep of backprop signal in class
parser.add_argument("--labeled_da", type=str, default='no')  # data aumentation  labeled dataset
parser.add_argument("--unlabeled_da", type=str, default='no')  # data aumentation  unlabeled dataset
args = parser.parse_args()

# -------------------------
# Prepare labeled datasets
# ------------------------
bs = args.batch_size / 2



# --------------------------
# Prepare unlabeled datasets
# --------------------------
train_unlab_gen2, valid_unlab_gen, test_unlab_gen, (n_train, n_valid, n_test) = \
    lib.soja_dataset_unlabeled.load_all_comp(args.parches_u_fot, args.parches_u_robot, bs)

valid_batches2 = n_valid / (bs * args.valid_factor)
test_batches2 = n_test / (bs * args.test_factor)


def inf_train_gen2():
    while True:
        for images2 in train_unlab_gen2():
            yield images2


def inf_valid_gen2():
    while True:
        for images2 in valid_unlab_gen():
            yield images2


def inf_test_gen2():
    while True:
        for images2 in test_unlab_gen():
            yield images2


gen_train2 = inf_train_gen2()
gen_valid2 = inf_valid_gen2()
gen_test2 = inf_test_gen2()



# -----------------------
# Build folders and files
# -----------------------
OUT_PATH = os.getcwd() + '/out/'
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)

date = (time.strftime("%Y_%m_%d")) + '_' + str(datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S'))
THIS_RUN_PATH = OUT_PATH + date + '_wgan_softmax_' + "model_dim={}_n_clases={}_n_iters={}_bs={}_wd={}". \
    format(
    args.model_dim,
    args.n_classes,
    args.n_iters,
    args.batch_size,
    args.wd)

CHECKPOINTS_FOLDER_PATH = THIS_RUN_PATH + '/checkpoints/'
BEST_CHECKPOINTS_FOLDER_PATH = THIS_RUN_PATH + '/best_checkpoints/'
LOG_FOLDER_PATH = THIS_RUN_PATH + '/logs/'
SAMPLES_PATH = THIS_RUN_PATH + '/samples/'

os.mkdir(THIS_RUN_PATH)
if not os.path.exists(BEST_CHECKPOINTS_FOLDER_PATH):
    os.mkdir(BEST_CHECKPOINTS_FOLDER_PATH)
if not os.path.exists(CHECKPOINTS_FOLDER_PATH):
    os.mkdir(CHECKPOINTS_FOLDER_PATH)
if not os.path.exists(LOG_FOLDER_PATH):
    os.mkdir(LOG_FOLDER_PATH)
if not os.path.exists(SAMPLES_PATH):
    os.mkdir(SAMPLES_PATH)

with open(LOG_FOLDER_PATH + '/errors.log', 'w') as f:
    f.write('# iter data_seen epoch comp_lost')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/accuracies_comp.log', 'w') as f:
    f.write('# iter data_seen epoch train_acc valid_acc test_acc')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/best_acc_comp.log', 'w') as f:
    f.write('# iter data_seen epoch train_acc valid_acc test_acc')
    f.write('\n')

print("Arguments:")
for arg in vars(args):
    print("\t{}: {}".format(arg, getattr(args, arg)))
print("")


# -------------------------
# Data Aumentation
# -------------------------
rotation_range = 20
width_shift_range = 0.028
height_shift_range = 0.016
shear_range = 0.14
zoom_range_center = 0.97
zoom_range_range = 0.18
zoom_range = (zoom_range_center - zoom_range_range * 0.5, zoom_range_center + zoom_range_range * 0.5)
random_curves_strength = 0.58
seed = 17
augm = Augmentation(rotation_range=rotation_range,  # In degrees
                    width_shift_range=width_shift_range,
                    height_shift_range=height_shift_range,
                    horizontal_flip=False,
                    shear_range=shear_range,  # In radians
                    zoom_range=zoom_range,  # >1 zoom out; <1 zoom in
                    channel_shift_range=0.0,  # 0-255
                    fill_mode='constant',  # 'nearest',
                    random_curves_strength=random_curves_strength,
                    seed=seed)


def make_labels(bs):
    # iguales [1,0]; distintos [0,1]
    eq = np.zeros(shape=(bs, 2), dtype=np.float64)
    dis = np.zeros(shape=(bs, 2), dtype=np.float64)
    for i in range(bs):
        eq[i, 0] = 1
        dis[i, 1] = 1
    return eq, dis


def generate_image_3(samples, name, bs, iteration, size):
    ims = samples
    # ims = np.transpose(samples,(0,2,3,1))
    n_samples = bs

    ims = ((ims + 1.) * (255.99 / 2)).astype('int32')
    lib.save_images.save_images(ims.reshape((bs, 3, size, size)),
                                os.path.join(THIS_RUN_PATH, 'samples_{}_' + name + '.jpg'.format(iteration)))


def make_xs1_xs2_test(gen, epoc, bs):
    label_eq, label_dis = make_labels(bs)

    batch_xs1_96 = gen.next()
    batch_xs1_96 = np.copy(batch_xs1_96)
    batch_xs12_96 = np.copy(batch_xs1_96)

    #generate_image_3(batch_xs1_96, 'uno_solo', bs , epoc,96)

    #if (random.randint(0, 9) <= 4):
    # batch_xs1_96 = augm.random_transform(batch_xs1_96)
    #generate_image_3(batch_xs1_96, 'uno_aum', bs , epoc,96)

    #if (random.randint(0, 9) <= 4):
    # batch_xs12_96 = augm.random_transform(batch_xs12_96)
    #generate_image_3(batch_xs12_96, 'uno_aum2', bs , epoc,96)

    batch_xs2_96 = gen.next()
    #if (random.randint(0, 9) <= 4):
    # batch_xs2_96 = augm.random_transform(batch_xs2_96)
    #generate_image_3(batch_xs2_96, 'dos_aum', bs , epoc,96)

    batch_xs1 = to_64(batch_xs1_96)
    batch_xs12 = to_64(batch_xs12_96)
    batch_xs2 = to_64(batch_xs2_96)

    #generate_image_3(batch_xs1, 'u_aum', bs, epoc,64)
    #generate_image_3(batch_xs12, 'u2_aum', bs, epoc,64)
    #generate_image_3(batch_xs2, 'd_aum', bs, epoc,64)

    # generate_image(batch_xs1,'uno')
    x1 = np.concatenate([batch_xs1, batch_xs1])
    x2 = np.concatenate([batch_xs12, batch_xs2])
    l = np.concatenate([label_eq, label_dis])

    random_state = np.random.RandomState([epoc])
    random_state.shuffle(x1)
    random_state = np.random.RandomState([epoc])
    random_state.shuffle(x2)
    random_state = np.random.RandomState([epoc])
    random_state.shuffle(l)

    #generate_image_3(x1, 'uno', bs*2, epoc, 64)
    #generate_image_3(x2, 'dos', bs*2, epoc,64)

    return x1, x2, l


# ----------------
# Create the model
# ----------------
# aca si se usa el batch size real, porque en realidad estoy tomando 9 veces menos imagenes en el ds loader
Discriminator = Only_Discriminator_comp_gan(model_arch='dcgan')
all_real_data_conv1 = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])
all_real_data_conv2 = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])

real_data1 = tf.reshape(2 * ((tf.cast(all_real_data_conv1, tf.float32) / 255.) - .5),
                        [args.batch_size, args.output_dim])
real_data2 = tf.reshape(2 * ((tf.cast(all_real_data_conv2, tf.float32) / 255.) - .5),
                        [args.batch_size, args.output_dim])

comp_labels = tf.placeholder(tf.float32, [args.batch_size, 2])

flat_x1 = Discriminator(real_data1, n_classes=args.n_classes, model_dim=args.model_dim)
flat_x2 = Discriminator(real_data2, n_classes=args.n_classes, model_dim=args.model_dim)

concat = tf.concat([flat_x1, flat_x2], 1)
lib.ops.linear_c.set_weights_stdev(0.02)
generated_labels_comp = lib.ops.linear_c.Linear('Comp', 4 * 4 * 8 * args.model_dim * 2, 2, concat)
lib.ops.linear_c.unset_weights_stdev()
correct_prediction_comp = tf.equal(tf.argmax(generated_labels_comp, 1), tf.argmax(comp_labels, 1))
cross_entropy_comp = tf.nn.softmax_cross_entropy_with_logits_v2(labels=comp_labels, logits=generated_labels_comp)
cross_entropy_comp = tf.reduce_mean(cross_entropy_comp)
w_comp = lib.params_with_name('Comp.W')[0]
reg = tf.nn.l2_loss(w_comp)

accuracy_comp = tf.reduce_mean(tf.cast(correct_prediction_comp, tf.float32))
comp_loss = cross_entropy_comp + reg * args.wd
comp_train_op = tf.train.AdamOptimizer(1e-4).minimize(comp_loss, var_list=lib._params)

# --------------
# Init session
# --------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())

# --------------
# Savers
# --------------
saver = tf.train.Saver(save_relative_paths=True)  # all session: gen + disc + class + comp
best_d_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Discriminator.'))  # only disc
best_comp_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Comp'))  # disc + c

# ----------
# Train loop
# ----------
if args.checkpoint_path is None:
    START_ITER = 0
else:
    START_ITER = args.start_iter
    saver.restore(session, save_path=args.checkpoint_path)
    print("\nStarting in iter={}:".format(START_ITER))

best_acc = 0.
best_acc_comp = 0.
it_best = 0
last_it = 0
t = time.time()
for iteration in range(START_ITER, args.n_iters):
    epoch = iteration * args.batch_size / n_train
    start_time = time.time()

    # Train Comparsion ver la salida de los labels
    batch_xs1, batch_xs2, labels = make_xs1_xs2_test(gen_train2, iteration, bs)
    _comp_cost, _, _train_acc_comp, _generated_labels_comp = \
        session.run([comp_loss, comp_train_op, accuracy_comp, generated_labels_comp],
                                                 feed_dict={all_real_data_conv1: batch_xs1,
                                                            all_real_data_conv2: batch_xs2,
                                                            comp_labels: labels})

    # ----------------------------------------------------
    # Log, Sample and Check
    # ----------------------------------------------------
    if iteration >= args.checkpoint_start_iter and iteration % args.checkpoint_iter == args.checkpoint_iter - 1:

        # Valid accuracy
        valid_acc_comp = 0.
        for valid_it_comp in range(valid_batches2):
            batch_xs1, batch_xs2, labels = make_xs1_xs2_test(gen_valid2, iteration, bs)
            valid_acc_comp += session.run(accuracy_comp,
                                     feed_dict={all_real_data_conv1: batch_xs1,
                                                all_real_data_conv2: batch_xs2,
                                                comp_labels: labels})
        valid_acc_comp = valid_acc_comp / valid_batches2

        # Test accuracy
        test_acc_comp = 0.
        for test_it_comp in range(test_batches2):
            batch_xs1, batch_xs2, labels = make_xs1_xs2_test(gen_test2, iteration, bs)
            test_acc_comp += session.run(accuracy_comp,
                                         feed_dict={all_real_data_conv1: batch_xs1,
                                                    all_real_data_conv2: batch_xs2,
                                                    comp_labels: labels})
        test_acc_comp = test_acc_comp / test_batches2

        # Log and save best model
        if best_acc_comp < valid_acc_comp:
            best_acc_comp = valid_acc_comp
            it_best_comp = iteration

            best_comp_saver.save(sess=session, save_path=os.path.join(BEST_CHECKPOINTS_FOLDER_PATH, "best_iter_comp"),
                                 global_step=iteration + 1)

            # Log best model
            with open(LOG_FOLDER_PATH + '/best_acc_comp.log', 'a') as f:
                np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size,
                                epoch, _train_acc_comp, valid_acc_comp, test_acc_comp]], fmt='%1.3e')

        with open(LOG_FOLDER_PATH + '/errors.log', 'a') as f:
            np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size, epoch, _comp_cost]], fmt='%1.3e')

        with open(LOG_FOLDER_PATH + '/accuracies_comp.log', 'a') as f:
            np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size, epoch, _train_acc_comp, valid_acc_comp,
                            test_acc_comp]], fmt='%1.3e')

        t2 = time.time() - t
        t += t2
        horas = t2 / (1 + iteration - last_it) / 3600. * 10000
        print "iter:%d/%d; epoch:%d; %4.2f horas. para 10000 iteraciones" % (iteration + 1, args.n_iters, epoch, horas)
        last_it = iteration + 1
        saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter"), global_step=iteration + 1)

# Check last model
# saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter"), global_step=args.n_iters + 1)

print "Train gan ended!\n"
