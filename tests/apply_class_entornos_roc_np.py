import os
import sys
import tflib.soja_dataset_labeled
import numpy as np
import tensorflow as tf
import tflib as lib
from gan import GeneratorAndDiscriminator, GeneratorAndDiscriminator_pe
import argparse
import time
import datetime
import tflib.ops.linear_c
from numpy import trapz
from PIL import Image

# ------------------------------------------------------------------------
# Carga un clasificador entrenado y datasets etiquetados
# y da como salida una cuva rock  moviendo el trheshold de a tr_step
# ------------------------------------------------------------------------


sys.path.append(os.getcwd())
parser = argparse.ArgumentParser()


# test dataset
parser.add_argument("--labels", type=str,
                    default='/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/datasets/soja/labeled_224/'
                            'labels_(999,3)_3_clases_float64_ROB_BALANCEADO.npy')
parser.add_argument("--entornos", type=str,
                    default='/media/joaquin/b7608595-32da-4f34-b12f-72f355fe3524/datasets/soja/labeled_224/'
                            'parches_labeled_(999,224,224,3)_uint8_ROB_BALANCEADO.npy')



parser.add_argument("--batch_size", type=int, default=999)


parser.add_argument("--model_arch", type=str, default='dcgan')       # model arch: 'good', 'dcgan'
parser.add_argument("--model_dim", type=int, default=32)             # model size

parser.add_argument("--n_classes", type=int, default=3)
parser.add_argument("--output_dim", type=int, default=64 * 64 * 3)
parser.add_argument("--checkpoint_path", type=str, default='/home/joaquin/checkpoints/best_iter_c-83100')
parser.add_argument("--tr_step", type=float, default=0.01)  # threshold step size

args = parser.parse_args()

# -----------------------
# Build folders and files
# -----------------------
OUT_PATH = os.getcwd() + '/out/'
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)

DATE = (time.strftime("%Y_%m_%d")) + '_' + str(datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S'))
THIS_RUN_PATH = OUT_PATH + DATE + '_class_roc_np'

LOG_FOLDER_PATH = THIS_RUN_PATH + '/logs/'

os.mkdir(THIS_RUN_PATH)

if not os.path.exists(LOG_FOLDER_PATH):
    os.mkdir(LOG_FOLDER_PATH)

with open(LOG_FOLDER_PATH + '/roc.log', 'w') as f:
    f.write('# thres   acc       tp        fp        tn        fn        tpr       fpr')
    f.write('\n')

with open(LOG_FOLDER_PATH + '/pr.log', 'w') as f:
    f.write('# thres   acc       tp        fp        tn        fn        p       r')
    f.write('\n')

# ----------------
# Create the model
# ----------------
Generator_p, Generator_e, Discriminator_p, Discriminator_e = GeneratorAndDiscriminator_pe(model_arch='dcgan')

all_real_data_conv_p = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])
all_real_data_conv_e = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])

real_data_p = tf.reshape(2 * ((tf.cast(all_real_data_conv_p, tf.float32) / 255.) - .5),
                         [args.batch_size, args.output_dim])
real_data_e = tf.reshape(2 * ((tf.cast(all_real_data_conv_e, tf.float32) / 255.) - .5),
                         [args.batch_size, args.output_dim])

real_labels = tf.placeholder(tf.float32, [args.batch_size, args.n_classes])

fake_data_p = Generator_p(args.batch_size, model_dim=args.model_dim)
fake_data_e = Generator_e(args.batch_size, model_dim=args.model_dim)

disc_real_p, flat_p = Discriminator_p(real_data_p, model_dim=args.model_dim)
disc_real_e, flat_e = Discriminator_e(real_data_e, model_dim=args.model_dim)

disc_fake_p, _ = Discriminator_p(fake_data_p, model_dim=args.model_dim)
disc_fake_e, _ = Discriminator_e(fake_data_e, model_dim=args.model_dim)

# --------------------------
# CONCAT and CLASS
# --------------------------
concat = tf.concat([flat_p, flat_e], 1)
lib.ops.linear_c.set_weights_stdev(0.02)
generated_labels = lib.ops.linear_c.Linear('Disc_c.Class', 4 * 4 * 8 * args.model_dim * 2, args.n_classes, concat)
lib.ops.linear_c.unset_weights_stdev()
correct_prediction = tf.equal(tf.argmax(generated_labels, 1), tf.argmax(real_labels, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

generated_labels_sig = tf.nn.softmax(generated_labels)
#x_thresholds = tf.placeholder(tf.float32, shape=args.batch_size)



# ------------------
# Get data iterators
# ------------------
all_parches, all_entornos, all_labels = lib.soja_dataset_labeled.load_all_dataset_entornos(args.entornos, args.labels)
n_patches = all_labels.shape[0]


def get_next(it, all_parches=all_parches, all_entornos=all_entornos, all_labels=all_labels, bs=args.batch_size):
    return all_parches[it * bs:(it + 1) * bs, :, :, :], \
           all_entornos[it * bs:(it + 1) * bs, :, :, :], \
           all_labels[it * bs:(it + 1) * bs, :]


t = time.time()





# ----------------------
# Restore trained model
# ----------------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())
disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
disc_saver.restore(session, save_path=args.checkpoint_path)
n_thresholds = int(1 + 1 / args.tr_step)

tpr_list = []
fpr_list = []
p_lucas_list = []
precision_list = []
recall_list = []

all_generated_labels = []
all_real_labels = []


for iteration in range(0, n_patches / args.batch_size):
    # batch_xs_p, batch_xs_e, batch_ys = get_next(iteration)
    batch_xs_p, batch_xs_e, batch_ys = get_next(iteration)

    t = time.time()

    _generated_labels_sig = session.run([generated_labels_sig],
                                        feed_dict={all_real_data_conv_p: batch_xs_p,
                                                   all_real_data_conv_e: batch_xs_e,
                                                   real_labels: batch_ys})

    all_generated_labels.extend(np.ndarray.tolist(_generated_labels_sig[0][:, 0]))
    all_real_labels.extend(np.ndarray.tolist(batch_ys[:, 0]))

    print time.time() - t


for tr in range(0, n_thresholds):
    np_a = np.array([tr * args.tr_step] * args.batch_size, dtype=np.float32)

    malezas_generadas = [a >= (tr * args.tr_step) for a in all_generated_labels]
    malezas_reales = [int(a) == 1 for a in all_real_labels]

    tp = sum([mg and mr for (mg, mr) in zip(malezas_generadas, malezas_reales)])
    fp = sum([mg and (not mr) for (mg, mr) in zip(malezas_generadas, malezas_reales)])
    tn = sum([(not mg) and (not mr) for (mg, mr) in zip(malezas_generadas, malezas_reales)])
    fn = sum([(not mg) and mr for (mg, mr) in zip(malezas_generadas, malezas_reales)])
    acc = np.float32(sum([mg == mr for (mg, mr) in zip(malezas_generadas, malezas_reales)])) / n_patches

    tpr = np.float32(tp) / (np.float32(tp) + np.float32(fn))
    fpr = np.float32(fp) / (np.float32(fp) + np.float32(tn))
    p_lucas = (np.float32(tp) + np.float32(fp)) / (np.float32(tp) + np.float32(fp) + np.float32(tn) + np.float32(fn))

    p = np.float32(tp) / (np.float32(tp) + np.float32(fp))  # precision tp / (tp + fp)
    r = np.float32(tp) / (np.float32(tp) + np.float32(fn))  # recall

    tpr_list.append(tpr)
    fpr_list.append(fpr)
    p_lucas_list.append(p_lucas)
    precision_list.append(p)
    recall_list.append(r)

    with open(LOG_FOLDER_PATH + '/roc.log', 'a') as f:
        np.savetxt(f, [[tr * args.tr_step, acc, tp, fp, tn, fn, tpr, fpr]], fmt='%1.3e')

    with open(LOG_FOLDER_PATH + '/pr.log', 'a') as f:
        np.savetxt(f, [[tr * args.tr_step, acc, tp, fp, tn, fn, p, r]], fmt='%1.3e')

    with open(LOG_FOLDER_PATH + '/roc_lucas.log', 'a') as f:
        np.savetxt(f, [[tr * args.tr_step, acc, tp, fp, tn, fn, tpr, p_lucas]], fmt='%1.3e')

y = np.asarray(tpr_list, dtype=np.float64)
x1 = np.asarray(fpr_list, dtype=np.float64)
x2 = np.asarray(p_lucas_list, dtype=np.float64)

y1 = np.asarray(precision_list, dtype=np.float64)
x3 = np.asarray(recall_list, dtype=np.float64)

print 'AUR-roc' + str(trapz(y=y, x=x1))
print 'AUR-roc_lucas' + str(trapz(y=y, x=x2))
print 'AUR-roc_pr' + str(trapz(y=y1, x=x3))
print "class ended!\n"

