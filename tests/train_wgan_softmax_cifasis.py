import os
import sys
import time
import tflib.soja_dataset_labeled
import tflib.soja_dataset_unlabeled
from tflib.soja_dataset_labeled import crop_96_to_64 as to_64
import numpy as np
import tensorflow as tf
import tflib as lib
import tflib.save_images
from gan import GeneratorAndDiscriminator
import argparse
import datetime
from tflib.augmentation import Augmentation

# ------------------------------------
# WGAN + softmax clasificador
# ------------------------------------
sys.path.append(os.getcwd())

parser = argparse.ArgumentParser()
parser.add_argument("--labelpath", type=str,
                    default='/home/jmesuro/datasets/soja_cifasis/labels_(10883, 2)_2_clases_float64_maleza_NO_INF.npy')
parser.add_argument("--patchpath_l", type=str,
                    default='/home/jmesuro/datasets/soja_cifasis/parches_labeled_(10883, 96, 96, 3)_3_clases_uint8_NO_INF.npy')
parser.add_argument("--patchpath_u", type=str,
                    default='/home/jmesuro/datasets/soja_cifasis/parches_unlabeled_fotografos_1200879_3_96_96_int8.h5')
parser.add_argument("--model_arch", type=str, default='dcgan')  # model arch: 'good', 'dcgan'
parser.add_argument("--mode", type=str, default='wgan-gp')  # mode: 'dcgan', 'wgan-gp'
parser.add_argument("--critic_iters", type=int, default=1)  # How many iterations to train the critic for
parser.add_argument("--model_dim", type=int, default=16)  # How many iterations to train the critic for
parser.add_argument("--batch_size", type=int, default=128)  # Batch size. Must be a multiple of N_GPUS
parser.add_argument("--n_iters", type=int, default=500)  # How many iterations to train for
parser.add_argument("--n_classes", type=int, default=2)  # How many classes to train for
parser.add_argument("--start_iter", type=int, default=0)  # Start iter num when restoring model
parser.add_argument("--lambda_gp", type=int, default=10)  # Gradient penalty lambda hyperparameter
parser.add_argument("--output_dim", type=int, default=64 * 64 * 3)  # Number of pixels in each image
parser.add_argument("--checkpoint_start_iter", type=int, default=0)
parser.add_argument("--checkpoint_iter", type=int, default=100)  # Save model vars every checkpoint_iter iterations
parser.add_argument("--checkpoint_path", type=str,
                    default=None)  # Path to recover a model to keep training
parser.add_argument("--rand_seed", type=int, default=2017)  # Random seed
parser.add_argument("--wd", type=float, default=0.01)  # Weight decay
args = parser.parse_args()

# ------------------------
# Prepare labeled datasets
# ------------------------
(train_gen, valid_gen, test_gen), (n_train, n_valid, n_test) = lib.soja_dataset_labeled.load_train_valid_test(
    args.patchpath_l, args.labelpath, args.batch_size)

valid_batches = n_valid / args.batch_size
test_batches = n_test / args.batch_size


def inf_train_gen():
    while True:
        for images, targets in train_gen():
            yield images, targets


def inf_valid_gen():
    while True:
        for images, targets in valid_gen():
            yield images, targets


def inf_test_gen():
    while True:
        for images, targets in test_gen():
            yield images, targets


gen_train = inf_train_gen()
gen_valid = inf_valid_gen()
gen_test = inf_test_gen()

# ------------------------
# Prepare unlabeled datasets
# ------------------------
train_unlab_gen = lib.soja_dataset_unlabeled.load(args.patchpath_u, args.batch_size)


def inf_unlab_train_gen():
    while True:
        for images in train_unlab_gen():
            yield images


gen_unlab_train = inf_unlab_train_gen()

# -----------------------
# Build folders and files
# -----------------------
OUT_PATH = os.getcwd() + '/out/'
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)

date = (time.strftime("%Y_%m_%d")) + '_' + str(datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S'))
THIS_RUN_PATH = OUT_PATH + date + '_wgan_softmax_' + "model_dim={}_n_clases={}_n_iters={}_bs={}_wd={}". \
    format(
    args.model_dim,
    args.n_classes,
    args.n_iters,
    args.batch_size,
    args.wd)

CHECKPOINTS_FOLDER_PATH = THIS_RUN_PATH + '/checkpoints/'
BEST_CHECKPOINTS_FOLDER_PATH = THIS_RUN_PATH + '/best_checkpoints/'
LOG_FOLDER_PATH = THIS_RUN_PATH + '/logs/'
SAMPLES_PATH = THIS_RUN_PATH + '/samples/'

os.mkdir(THIS_RUN_PATH)
if not os.path.exists(BEST_CHECKPOINTS_FOLDER_PATH):
    os.mkdir(BEST_CHECKPOINTS_FOLDER_PATH)
if not os.path.exists(CHECKPOINTS_FOLDER_PATH):
    os.mkdir(CHECKPOINTS_FOLDER_PATH)
if not os.path.exists(LOG_FOLDER_PATH):
    os.mkdir(LOG_FOLDER_PATH)
if not os.path.exists(SAMPLES_PATH):
    os.mkdir(SAMPLES_PATH)

with open(LOG_FOLDER_PATH + '/errors.log', 'w') as f:
    f.write('# iter data_seen epoch gen_loss disc_loss cl_loss')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/accuracies.log', 'w') as f:
    f.write('# iter data_seen epoch train_acc valid_acc test_acc')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/best_acc.log', 'w') as f:
    f.write('# iter data_seen epoch train_acc valid_acc test_acc')
    f.write('\n')

print("Arguments:")
for arg in vars(args):
    print("\t{}: {}".format(arg, getattr(args, arg)))
print("")


# ----------------------------------
# Image generation into SAMPLES_PATH
# ----------------------------------
def generate_image(session, iteration):
    # For generating samples
    fixed_noise = tf.constant(np.random.normal(size=(args.batch_size, 128)).astype('float32'))
    all_fixed_noise_samples = []
    n_samples = args.batch_size
    all_fixed_noise_samples.append(
        Generator(n_samples, args.model_dim, noise=fixed_noise[0:n_samples]))
    if tf.__version__.startswith('1.'):
        all_fixed_noise_samples = tf.concat(all_fixed_noise_samples, axis=0)
    else:
        all_fixed_noise_samples = tf.concat(0, all_fixed_noise_samples)

    samples = session.run(all_fixed_noise_samples)
    samples = ((samples + 1.) * (255.99 / 2)).astype('int32')
    lib.save_images.save_images(samples.reshape((args.batch_size, 3, 64, 64)),
                                os.path.join(SAMPLES_PATH, 'samples_{}.jpg'.format(iteration)))


# -------------------------
# Data Aumentation
# -------------------------
rotation_range = 20
width_shift_range = 0.028
height_shift_range = 0.016
shear_range = 0.14
zoom_range_center = 0.97
zoom_range_range = 0.18
zoom_range = (zoom_range_center - zoom_range_range * 0.5, zoom_range_center + zoom_range_range * 0.5)
random_curves_strength = 0.58
seed = 17
augm = Augmentation(rotation_range=rotation_range,  # In degrees
                    width_shift_range=width_shift_range,
                    height_shift_range=height_shift_range,
                    horizontal_flip=True,
                    shear_range=shear_range,  # In radians
                    zoom_range=zoom_range,  # >1 zoom out; <1 zoom in
                    channel_shift_range=0.0,  # 0-255
                    fill_mode='constant',  # 'nearest',
                    random_curves_strength=random_curves_strength,
                    seed=seed)

# ----------------
# Create the model
# ----------------
Generator, Discriminator = GeneratorAndDiscriminator(model_arch='dcgan')

all_real_data_conv = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])
real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [args.batch_size, args.output_dim])
real_labels = tf.placeholder(tf.float32, [args.batch_size, args.n_classes])
fake_data = Generator(args.batch_size, model_dim=args.model_dim)

disc_real, generated_labels = Discriminator(real_data, n_classes=args.n_classes, model_dim=args.model_dim)
disc_fake, _ = Discriminator(fake_data, n_classes=args.n_classes, model_dim=args.model_dim)

correct_prediction = tf.equal(tf.argmax(generated_labels, 1), tf.argmax(real_labels, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
# ---------------------
# LOSS
# ---------------------
# cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(labels=real_labels, logits=generated_labels)
cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(labels=real_labels, logits=generated_labels)
cross_entropy = tf.reduce_mean(cross_entropy)

w = lib.params_with_name('Disc.Class.W')[0]
reg = tf.nn.l2_loss(w)
# cls_loss = tf.reduce_mean(cross_entropy + reg * args.wd)
cls_loss = cross_entropy + reg * args.wd

if args.mode == 'wgan-gp':
    gen_cost = -tf.reduce_mean(disc_fake)
    disc_cost = tf.reduce_mean(disc_fake) - tf.reduce_mean(disc_real)

    alpha = tf.random_uniform(shape=[args.batch_size, 1], minval=0., maxval=1.)
    differences = fake_data - real_data
    interpolates = real_data + (alpha * differences)
    dis_interpolates, _ = Discriminator(interpolates, model_dim=args.model_dim)
    gradients = tf.gradients(dis_interpolates, [interpolates])[0]
    slopes = tf.sqrt(tf.reduce_sum(tf.square(gradients), reduction_indices=[1]))
    gradient_penalty = tf.reduce_mean((slopes - 1.) ** 2)
    disc_cost += args.lambda_gp * gradient_penalty
elif args.mode == 'dcgan':
    try:  # tf pre-1.0 (bottom) vs 1.0 (top)
        gen_cost = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=disc_fake,
                                                                          labels=tf.ones_like(disc_fake)))
        disc_cost = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=disc_fake,
                                                                           labels=tf.zeros_like(disc_fake)))
        disc_cost += tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=disc_real,
                                                                            labels=tf.ones_like(disc_real)))
    except Exception as e:
        gen_cost = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(disc_fake, tf.ones_like(disc_fake)))
        disc_cost = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(disc_fake, tf.zeros_like(disc_fake)))
        disc_cost += tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(disc_real, tf.ones_like(disc_real)))
    disc_cost /= 2.
else:
    raise Exception()

# --------
# Trainers
# --------
class_train_op = tf.train.AdamOptimizer(1e-3).minimize(cls_loss, var_list=lib.params_with_name('Disc.Class'))
if args.mode == 'wgan-gp':
    gen_train_op = tf.train.AdamOptimizer(learning_rate=1e-4, beta1=0., beta2=0.9).minimize(gen_cost,
                                                                                            var_list=lib.params_with_name(
                                                                                                'Generator'),
                                                                                            colocate_gradients_with_ops=True)
    disc_train_op = tf.train.AdamOptimizer(learning_rate=1e-4, beta1=0., beta2=0.9).minimize(disc_cost,
                                                                                             var_list=lib.params_with_name(
                                                                                                 'Discriminator.'),
                                                                                             colocate_gradients_with_ops=True)
elif args.mode == 'dcgan':
    gen_train_op = tf.train.AdamOptimizer(learning_rate=2e-4, beta1=0.5).minimize(gen_cost,
                                                                                  var_list=lib.params_with_name(
                                                                                      'Generator'),
                                                                                  colocate_gradients_with_ops=True)
    disc_train_op = tf.train.AdamOptimizer(learning_rate=2e-4, beta1=0.5).minimize(disc_cost,
                                                                                   var_list=lib.params_with_name(
                                                                                       'Discriminator.'),
                                                                                   colocate_gradients_with_ops=True)
else:
    raise Exception()

# --------------
# Init session
# --------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())

# --------------
# Savers
# --------------
disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Discriminator.'))  # just disc
gen_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Generator.'))  # gen
best_disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Discriminator.'))
best_disc_class_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))  # disc + class
best_gen_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Generator.'))
saver = tf.train.Saver(save_relative_paths=True)  # all session: gen + disc + class
# ------------------------------------
# Save a batch of ground-truth samples
# ------------------------------------

# _x1_96, _ = inf_train_gen().next()
# _x1 = to_64(_x1_96)
# _x1_aum_96 = augm.random_transform(_x1_96)
# _x1_aum = to_64(_x1_aum_96)
#
# _x_r = session.run(real_data, feed_dict={all_real_data_conv: _x1[:args.batch_size]})
# _x_r = ((_x_r + 1.) * (255.99 / 2)).astype('int32')
# lib.save_images.save_images(_x_r.reshape((args.batch_size, 3, 64, 64)),
#                             os.path.join(SAMPLES_PATH, 'samples_groundtruth_' + str(i) + '.jpg'))
#
# _x_r = session.run(real_data, feed_dict={all_real_data_conv: _x1_aum[:args.batch_size]})
# _x_r = ((_x_r + 1.) * (255.99 / 2)).astype('int32')
# lib.save_images.save_images(_x_r.reshape((args.batch_size, 3, 64, 64)),
#                             os.path.join(SAMPLES_PATH, 'samples_groundtruth_aum_to_64_' + str(i) + '.jpg'))

# ----------
# Train loop
# ----------
if args.checkpoint_path is None:
    START_ITER = 0
else:
    START_ITER = args.start_iter
    saver.restore(session, save_path=args.checkpoint_path)
    print("\nStarting in iter={}:".format(START_ITER))

best_acc = 0.
it_best = 0
last_it = 0
t = time.time()
for iteration in range(START_ITER, args.n_iters):
    epoch = iteration * args.batch_size / n_train
    start_time = time.time()

    # Train generator
    _gen_cost = 0.
    if iteration > 0:
        _gen_cost, _ = session.run([gen_cost, gen_train_op])

    # Train critic and clasificator
    train_acc = 0.
    _cls_cost_ac = 0.
    _disc_cost_ac = 0.
    if (args.mode == 'dcgan') or (args.mode == 'lsgan'):
        disc_iters = 1
    else:
        disc_iters = args.critic_iters
    for i in range(args.critic_iters):
        batch_xs_unlab = to_64(gen_unlab_train.next())
        batch_xs, batch_ys = gen_train.next()
        batch_xs = augm.random_transform(batch_xs)
        batch_xs = to_64(batch_xs)
        _disc_cost, _ = session.run([disc_cost, disc_train_op], feed_dict={all_real_data_conv: batch_xs_unlab})
        _cls_cost, _ = session.run([cls_loss, class_train_op],
                                   feed_dict={all_real_data_conv: batch_xs, real_labels: batch_ys})
        train_acc += session.run(accuracy, feed_dict={all_real_data_conv: batch_xs, real_labels: batch_ys})
        _cls_cost_ac += _cls_cost
        _disc_cost_ac += _disc_cost

    _cls_cost = _cls_cost_ac / disc_iters
    _disc_cost = _disc_cost_ac / disc_iters
    train_acc = train_acc / disc_iters

    # Log, Sample and Check
    if iteration >= args.checkpoint_start_iter and iteration % args.checkpoint_iter == args.checkpoint_iter - 1:
        # Sample generated images
        generate_image(session, iteration + 1)

        # Valid accuracy
        valid_acc = 0.
        for test_it in range(valid_batches):
            batch_xs_valid, batch_ys_valid = gen_valid.next()
            valid_acc += session.run(accuracy, feed_dict={all_real_data_conv: batch_xs_valid,
                                                          real_labels: batch_ys_valid})
        valid_acc = valid_acc / valid_batches

        # Test accuracy
        test_acc = 0.
        for test_it in range(test_batches):
            batch_xs_test, batch_ys_test = gen_test.next()
            test_acc += session.run(accuracy, feed_dict={all_real_data_conv: batch_xs_test, real_labels: batch_ys_test})
        test_acc = test_acc / test_batches

        # Log and save best model
        if best_acc < valid_acc:
            best_acc = valid_acc
            it_best = iteration

            # Save best model
            best_disc_saver.save(sess=session, save_path=os.path.join(BEST_CHECKPOINTS_FOLDER_PATH, "iter_d"),
                                 global_step=iteration + 1)
            best_disc_class_saver.save(sess=session, save_path=os.path.join(BEST_CHECKPOINTS_FOLDER_PATH, "iter_dc"),
                                       global_step=iteration + 1)
            # Log best model
            with open(LOG_FOLDER_PATH + '/best_acc.log', 'a') as f:
                np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size,
                                epoch, train_acc, valid_acc, test_acc]], fmt='%1.3e')

        with open(LOG_FOLDER_PATH + '/errors.log', 'a') as f:
            np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size, epoch, _gen_cost, _disc_cost, _cls_cost]],
                       fmt='%1.3e')

        with open(LOG_FOLDER_PATH + '/accuracies.log', 'a') as f:
            np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size, epoch, train_acc, valid_acc,
                            test_acc]], fmt='%1.3e')

        t2 = time.time() - t
        t += t2
        horas = t2 / (1 + iteration - last_it) / 3600. * 10000
        print "iter:%d/%d; epoch:%d; %4.2f horas. para 10000 iteraciones" % (iteration + 1, args.n_iters, epoch, horas)
        last_it = iteration + 1
        #saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter"), global_step=iteration + 1)

# Check last model
gen_saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter_g"), global_step=args.n_iters + 1)
disc_saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter_d"), global_step=args.n_iters + 1)
best_disc_class_saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter_dc"),
                           global_step=args.n_iters + 1)
saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter"), global_step=args.n_iters + 1)

print "Train gan ended!\n"
