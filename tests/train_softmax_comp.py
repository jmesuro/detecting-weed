import os
import sys
import time
import tflib.soja_dataset_labeled
import numpy as np
import tensorflow as tf
from gan import GeneratorAndDiscriminator_comp
import argparse
import datetime
from tflib.augmentation import Augmentation
from tflib.soja_dataset_labeled import crop_96_to_64 as to_64
import tflib.ops.linear_c
import random
import tensorboard
from PIL import Image
import tflib as lib
import tflib.save_images

# ------------------------------------------------------------------------
# Toma un GAN y entrena un comparador de parches. te dice si dos parches
# son iguales, usamos DA porque queremos que sea robusto a movimientos
# es para tracking
# -------------------------------------------------------------------------
sys.path.append(os.getcwd())
parser = argparse.ArgumentParser()

parser.add_argument("--labels", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/FOT_96_BALANCEADO_IDX/labels_(10986,3)_3_clases_float64_BALANCEADO_TRAIN.npy')
parser.add_argument("--parches", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/FOT_96_BALANCEADO_IDX/parches_labeled_(10986,96,96,3)_uint8_BALANCEADO_TRAIN.npy')

parser.add_argument("--model_arch", type=str, default='dcgan')  # model arch: 'good', 'dcgan'
parser.add_argument("--critic_iters", type=int, default=1)  # How many iterations to train the critic for
parser.add_argument("--model_dim", type=int, default=4)  # How many iterations to train the critic for
parser.add_argument("--batch_size", type=int, default=4)  # Batch size. Must be a multiple of N_GPUS
parser.add_argument("--n_iters", type=int, default=1)  # How many iterations to train for
parser.add_argument("--n_classes", type=int, default=3)  # How many classes to train for
parser.add_argument("--start_iter", type=int, default=0)  # Start iter num when restoring model
parser.add_argument("--lambda_gp", type=int, default=10)  # Gradient penalty lambda hyperparameter
parser.add_argument("--output_dim", type=int, default=64 * 64 * 3)  # Number of pixels in each image
parser.add_argument("--checkpoint_start_iter", type=int, default=0)
parser.add_argument("--checkpoint_iter", type=int, default=1)  # Save model vars every checkpoint_iter iterations
parser.add_argument("--checkpoint_path", type=str, default=None)  # Path to recover a model to keep training
parser.add_argument("--rand_seed", type=int, default=2017)  # Random seed
parser.add_argument("--wd", type=float, default=0.01)  # Weight decay
args = parser.parse_args()

# ----------------------------------------------
# Check_path of the previously trained gan model
# ----------------------------------------------
GAN_CHECKPOINT_D = None

# ------------------------
# Prepare labeled datasets
# ------------------------
(train_gen, valid_gen, test_gen), (n_train, n_valid, n_test) = \
    lib.soja_dataset_labeled.load_train_valid_test_shuffled_comp(args.parches, args.labels, args.batch_size,
                                                                 cel_size=96)

valid_batches = n_valid / args.batch_size
test_batches = n_test / args.batch_size


def inf_train_gen():
    while True:
        for images, targets in train_gen():
            yield images, targets


def inf_valid_gen():
    while True:
        for images, targets in valid_gen():
            yield images, targets


def inf_test_gen():
    while True:
        for images, targets in test_gen():
            yield images, targets


gen_train = inf_train_gen()
gen_valid = inf_valid_gen()
gen_test = inf_test_gen()

# -----------------------
# Build folders and files
# -----------------------
OUT_PATH = os.getcwd() + '/out/'
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)

date = (time.strftime("%Y_%m_%d")) + '_' + str(datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S'))
THIS_RUN_PATH = OUT_PATH + date + '_wgan_softmax_comp_' + "model_dim={}_n_clases={}_n_iters={}_bs={}_wd={}". \
    format(
    args.model_dim,
    args.n_classes,
    args.n_iters,
    args.batch_size,
    args.wd)

CHECKPOINTS_FOLDER_PATH = THIS_RUN_PATH + '/checkpoints/'
BEST_CHECKPOINTS_FOLDER_PATH = THIS_RUN_PATH + '/best_checkpoints/'
LOG_FOLDER_PATH = THIS_RUN_PATH + '/logs/'
SAMPLES_PATH = THIS_RUN_PATH + '/samples/'

os.mkdir(THIS_RUN_PATH)
if not os.path.exists(CHECKPOINTS_FOLDER_PATH):
    os.mkdir(CHECKPOINTS_FOLDER_PATH)
if not os.path.exists(BEST_CHECKPOINTS_FOLDER_PATH):
    os.mkdir(BEST_CHECKPOINTS_FOLDER_PATH)
if not os.path.exists(LOG_FOLDER_PATH):
    os.mkdir(LOG_FOLDER_PATH)
if not os.path.exists(SAMPLES_PATH):
    os.mkdir(SAMPLES_PATH)

with open(LOG_FOLDER_PATH + '/errors.log', 'w') as f:
    f.write('# iter data_seen epoch cl_loss')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/accuracies.log', 'w') as f:
    f.write('# iter data_seen epoch train_acc test_acc')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/best_acc.log', 'w') as f:
    f.write('# iter data_seen epoch train_acc test_acc')
    f.write('\n')

print("Arguments:")
for arg in vars(args):
    print("\t{}: {}".format(arg, getattr(args, arg)))
print("")

# -------------------------
# Data Aumentation
# -------------------------
rotation_range = 20
width_shift_range = 0.028
height_shift_range = 0.016
shear_range = 0.14
zoom_range_center = 0.97
zoom_range_range = 0.18
zoom_range = (zoom_range_center - zoom_range_range * 0.5, zoom_range_center + zoom_range_range * 0.5)
random_curves_strength = 0.58
seed = 17
augm = Augmentation(rotation_range=rotation_range,  # In degrees
                    width_shift_range=width_shift_range,
                    height_shift_range=height_shift_range,
                    horizontal_flip=False,
                    shear_range=shear_range,  # In radians
                    zoom_range=zoom_range,  # >1 zoom out; <1 zoom in
                    channel_shift_range=0.0,  # 0-255
                    fill_mode='constant',  # 'nearest',
                    random_curves_strength=random_curves_strength,
                    seed=seed)
# -------------------------------------
# Create the model
# -------------------------------------
_, Discriminator = GeneratorAndDiscriminator_comp(model_arch='dcgan')

bs4 = args.batch_size * 8

all_real_data_conv_x1 = tf.placeholder(tf.int32, shape=[bs4 , 3, 64, 64])
all_real_data_conv_x2 = tf.placeholder(tf.int32, shape=[bs4 , 3, 64, 64])

real_data_x1 = tf.reshape(2 * ((tf.cast(all_real_data_conv_x1, tf.float32) / 255.) - .5),
                          [bs4 , args.output_dim])
real_data_x2 = tf.reshape(2 * ((tf.cast(all_real_data_conv_x2, tf.float32) / 255.) - .5),
                          [bs4 , args.output_dim])

comp_labels = tf.placeholder(tf.float32, [bs4, 2])

flat_x1 = Discriminator(real_data_x1, args.model_dim,n_classes=args.n_classes)
flat_x2 = Discriminator(real_data_x2, args.model_dim,n_classes=args.n_classes)

# --------------------------
# CONCAT and CLASS
# --------------------------
concat = tf.concat([flat_x1, flat_x2], 1)
lib.ops.linear_c.set_weights_stdev(0.02)
generated_labels = lib.ops.linear_c.Linear('Comp', 4 * 4 * 8 * args.model_dim * 2,2, concat)
lib.ops.linear_c.unset_weights_stdev()
correct_prediction = tf.equal(tf.argmax(generated_labels, 1), tf.argmax(comp_labels, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))



# ---------------------
# Trainers y LOSS
# ---------------------
cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(labels=comp_labels, logits=generated_labels)
cross_entropy = tf.reduce_mean(cross_entropy)
w = lib.params_with_name('Comp.W')[0]
reg = tf.nn.l2_loss(w)
cls_loss = cross_entropy + reg * args.wd
class_train_op = tf.train.AdamOptimizer(1e-4).minimize(cls_loss, var_list=lib.params_with_name('Comp'))



# ----------------------
# Restore trained model
# ----------------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))

#writer = tf.summary.FileWriter("/home/jmesuro/tb")
#writer.add_graph(session.graph)


init1 = tf.variables_initializer(var_list=tf.global_variables())
session.run(init1)
disc_saver = tf.train.Saver(save_relative_paths=True, var_list=tf.global_variables()[0:18])
disc_saver.restore(session, save_path=GAN_CHECKPOINT_D)
saver = tf.train.Saver(save_relative_paths=True)  # para continuar este entrenamiento
# ---------------------
# Train loop
# ---------------------

def make_labels():
    # iguales [1,0]; distintos [0,1]
    eq = np.zeros(shape=(args.batch_size, 2), dtype=np.float64)
    dis = np.zeros(shape=(args.batch_size, 2), dtype=np.float64)
    for i in range(args.batch_size):
        eq[i, 0] = 1
        dis[i, 1] = 1
    return eq, dis


def generate_image(samples, name, bs):
    ims = samples
    #ims = np.transpose(samples,(0,2,3,1))
    n_samples = bs

    ims = ((ims + 1.) * (255.99 / 2)).astype('int32')
    lib.save_images.save_images(ims.reshape((bs, 3, 64, 64)),
                                os.path.join(THIS_RUN_PATH, 'samples_{}_' + name + '.jpg'.format(iteration)))


def make_xs1_xs2(gen):

    label_eq, label_dis = make_labels()
    batch_xs1_96, _ = gen.next()
    batch_xs2_96 = batch_xs1_96
    batch_xs1_96 = np.copy(batch_xs1_96)
    labels = label_eq

    if (random.randint(0, 9) <= 4):
        # iguales
        if (random.randint(0, 9) <= 4):
            batch_xs1_96_aum = augm.random_transform(batch_xs1_96)
            batch_xs1 = to_64(batch_xs1_96_aum)
        else:
            batch_xs1 = to_64(batch_xs1_96)

        if (random.randint(0, 9) <= 4):
            batch_xs2_96_aum = augm.random_transform(batch_xs2_96)
            batch_xs2 = to_64(batch_xs2_96_aum)
        else:
            batch_xs2 = to_64(batch_xs2_96)
    else:
        # distintos
        batch_xs2_96 , _ = gen.next()
        labels = label_dis
        if (random.randint(0, 9) <= 4):
            batch_xs1_96_aum = augm.random_transform(batch_xs1_96)
            batch_xs1 = to_64(batch_xs1_96_aum)
        else:
            batch_xs1 = to_64(batch_xs1_96)

        if (random.randint(0, 9) <= 4):
            batch_xs2_96_aum = augm.random_transform(batch_xs2_96)
            batch_xs2 = to_64(batch_xs2_96_aum)
        else:
            batch_xs2 = to_64(batch_xs2_96)

    return batch_xs1, batch_xs2, labels


def make_xs1_xs2_test(gen,epoc):
    label_eq, label_dis = make_labels()

    batch_xs1_96, _ = gen.next()
    batch_xs1_96 = np.copy(batch_xs1_96)
    batch_xs12_96 = np.copy(batch_xs1_96)
    batch_xs13_96 = np.copy(batch_xs1_96)
    # batch_xs12_96_aum = augm.random_transform(batch_xs12_96)
    # batch_xs13_96_aum = augm.random_transform(batch_xs13_96)
    batch_xs12_96_aum = batch_xs12_96
    batch_xs13_96_aum = batch_xs13_96

    batch_xs2_96, _ = gen.next()
    batch_xs2_96 = np.copy(batch_xs2_96)
    batch_xs22_96 = np.copy(batch_xs2_96)
    batch_xs23_96 = np.copy(batch_xs2_96)
    #batch_xs22_96_aum = augm.random_transform(batch_xs22_96)
    #batch_xs23_96_aum = augm.random_transform(batch_xs23_96)
    batch_xs22_96_aum = batch_xs22_96
    batch_xs23_96_aum = batch_xs23_96

    batch_xs1 = to_64(batch_xs1_96)
    batch_xs12 = to_64(batch_xs12_96_aum)
    batch_xs13 = to_64(batch_xs13_96_aum)

    batch_xs2 = to_64(batch_xs2_96)
    batch_xs22 = to_64(batch_xs22_96_aum)
    batch_xs23 = to_64(batch_xs23_96_aum)

    # generate_image(batch_xs1,'uno')
    # generate_image(batch_xs12, 'uno dos')
    # generate_image(batch_xs13, 'uno tres')
    #
    # generate_image(batch_xs2, 'dos')
    # generate_image(batch_xs22, 'dos dos')
    # generate_image(batch_xs23, 'dos tres')

    x1 = np.concatenate([batch_xs1, batch_xs1,  batch_xs13, batch_xs12, batch_xs1, batch_xs1,  batch_xs13, batch_xs12])
    x2 = np.concatenate([batch_xs1, batch_xs12, batch_xs1,  batch_xs13, batch_xs2, batch_xs22, batch_xs2,  batch_xs23])
    l = np.concatenate([label_eq, label_eq, label_eq, label_eq, label_dis,label_dis, label_dis, label_dis])


    random_state = np.random.RandomState([epoc])
    random_state.shuffle(x1)
    random_state = np.random.RandomState([epoc])
    random_state.shuffle(x2)
    random_state = np.random.RandomState([epoc])
    random_state.shuffle(l)

    #generate_image(x1, 'uno', bs4)
    #generate_image(x2, 'dos', bs4)

    return x1, x2, l


#def shuffle_data(x1,x2,l):


if args.checkpoint_path != None:
    saver.restore(session, save_path=args.checkpoint_path)
    print("\nStarting in iter={}:".format(args.start_iter))

t = time.time()
best_acc = 0.
it_best = 0
last_it = 0
for iteration in range(args.start_iter, args.n_iters):

    epoch = iteration * args.batch_size / n_train
    start_time = time.time()

    # Train clasificator
    # tiro la moneda porque hay cuatro posibilidades de entrenamiento
    batch_xs1, batch_xs2, labels = make_xs1_xs2_test(gen_train,iteration)

    cls_cost, _, train_acc = session.run([cls_loss, class_train_op, accuracy],
                                         feed_dict={all_real_data_conv_x1: batch_xs1,
                                                    all_real_data_conv_x2: batch_xs2,
                                                    comp_labels: labels})

    # Check
    if iteration >= args.checkpoint_start_iter and iteration % args.checkpoint_iter == args.checkpoint_iter - 1:

        # Valid accuracy
        valid_acc = 0.
        for valid_it in range(valid_batches):
            batch_xs1, batch_xs2, labels = make_xs1_xs2_test(gen_valid,iteration)
            valid_acc += session.run(accuracy,
                                    feed_dict={all_real_data_conv_x1: batch_xs1,
                                               all_real_data_conv_x2: batch_xs2,
                                               comp_labels: labels})
        valid_acc = valid_acc / valid_batches

        # Test accuracy
        test_acc = 0.
        for test_it in range(test_batches):
            batch_xs1, batch_xs2, labels = make_xs1_xs2_test(gen_test,iteration)
            test_acc += session.run(accuracy,
                                   feed_dict={all_real_data_conv_x1: batch_xs1,
                                              all_real_data_conv_x2: batch_xs2,
                                              comp_labels: labels})
        test_acc = test_acc / test_batches

        # Log and save best model
        if best_acc < test_acc:
            best_acc = test_acc
            it_best = iteration

            # Save best model
            disc_saver.save(sess=session, save_path=os.path.join(BEST_CHECKPOINTS_FOLDER_PATH, "best_iter"),
                            global_step=iteration + 1)

            # Log best model
            with open(LOG_FOLDER_PATH + '/best_acc.log', 'a') as f:
                np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size,
                                epoch, train_acc, test_acc]], fmt='%1.3e')

        # Log model
        with open(LOG_FOLDER_PATH + '/errors.log', 'a') as f:
            np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size, epoch, cls_cost]],
                       fmt='%1.3e')

        with open(LOG_FOLDER_PATH + '/accuracies.log', 'a') as f:
            np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size, epoch,
                            train_acc, test_acc]], fmt='%1.3e')

        t2 = time.time() - t
        t += t2
        horas = t2 / (1 + iteration - last_it) / 3600. * 10000
        print("iter:%d/%d; epoch:%d; %4.2f horas. para 10000 iteraciones" % (iteration + 1,
                                                                             args.n_iters, epoch, horas))
        last_it = iteration + 1

        # saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter"), global_step=iteration + 1)

# Check last model
# saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter"), global_step=args.n_iters + 1)
print "Train comp ended!\n"
