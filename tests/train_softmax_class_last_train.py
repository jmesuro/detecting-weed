import os
import sys
import time
import tflib.soja_dataset_labeled
import numpy as np
import tensorflow as tf
import tflib as lib
from gan import GeneratorAndDiscriminator
import argparse
import datetime
from tflib.augmentation import Augmentation
from tflib.soja_dataset_labeled import crop_96_to_64 as to_64

# ------------------------------------------------------------------------
# Le da un poco mas de rosca al clasificador softmax, de un discriminador
# con clasificador ya entrenado
# -------------------------------------------------------------------------
sys.path.append(os.getcwd())
parser = argparse.ArgumentParser()

parser.add_argument("--labels", type=str,
                    default='/home/jmesuro/datasets/soja/labeled_96/labels_(875, 3)_3_clases_float64_maleza.npy')
parser.add_argument("--parches", type=str,
                    default='/home/jmesuro/datasets/soja/labeled_96/parches_labeled_(875, 96, 96, 3)_uint8.npy')

parser.add_argument("--model_arch", type=str, default='dcgan')       # model arch: 'good', 'dcgan'
parser.add_argument("--model_dim", type=int, default=16)             # model size
parser.add_argument("--batch_size", type=int, default=128)
parser.add_argument("--n_iters", type=int, default=3000)
parser.add_argument("--n_classes", type=int, default=3)
parser.add_argument("--start_iter", type=int, default=0)             # for continue training from chechpoint
parser.add_argument("--lambda_gp", type=int, default=10)             # wgan param
parser.add_argument("--output_dim", type=int, default=64 * 64 * 3)
parser.add_argument("--checkpoint_start_iter", type=int, default=0)  # iter from where to start to check
parser.add_argument("--checkpoint_iter", type=int, default=100)      # freq to validate
parser.add_argument("--checkpoint_gen_im", type=int, default=1000)     # freq to gen im
parser.add_argument("--checkpoint_path", type=str, default=None)     # path to save the check for continue training
parser.add_argument("--rand_seed", type=int, default=2017)
parser.add_argument("--wd", type=float, default=0.01)
parser.add_argument("--back_class", type=str, default='no')          # indicate the deep of backprop signal in class
args = parser.parse_args()

# ----------------------------------------------
# Check_path of the previously trained gan model
# ----------------------------------------------
GAN_CHECKPOINT_D = '/home/jmesuro/checkpoint/best_checkpoints/best_iter_class-81900'

# ------------------------
# Prepare labeled datasets
# ------------------------
(train_gen, valid_gen, test_gen), (n_train, n_valid, n_test) = \
    lib.soja_dataset_labeled.load_train_valid_test_shuffled(args.parches, args.labels, args.batch_size, cel_size=96)

test_batches = n_test / args.batch_size


def inf_train_gen():
    while True:
        for images, targets in train_gen():
            yield images, targets


def inf_test_gen():
    while True:
        for images, targets in test_gen():
            yield images, targets


gen_train = inf_train_gen()
gen_test = inf_test_gen()


# -----------------------
# Build folders and files
# -----------------------
OUT_PATH = os.getcwd() + '/out/'
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)

date = (time.strftime("%Y_%m_%d")) + '_' + str(datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S'))
THIS_RUN_PATH = OUT_PATH + date + '_wgan_softmax_class_' + "model_dim={}_n_clases={}_n_iters={}_bs={}_wd={}". \
    format(
    args.model_dim,
    args.n_classes,
    args.n_iters,
    args.batch_size,
    args.wd)

CHECKPOINTS_FOLDER_PATH = THIS_RUN_PATH + '/checkpoints/'
BEST_CHECKPOINTS_FOLDER_PATH = THIS_RUN_PATH + '/best_checkpoints/'
LOG_FOLDER_PATH = THIS_RUN_PATH + '/logs/'
SAMPLES_PATH = THIS_RUN_PATH + '/samples/'

os.mkdir(THIS_RUN_PATH)
if not os.path.exists(CHECKPOINTS_FOLDER_PATH):
    os.mkdir(CHECKPOINTS_FOLDER_PATH)
if not os.path.exists(BEST_CHECKPOINTS_FOLDER_PATH):
    os.mkdir(BEST_CHECKPOINTS_FOLDER_PATH)
if not os.path.exists(LOG_FOLDER_PATH):
    os.mkdir(LOG_FOLDER_PATH)
if not os.path.exists(SAMPLES_PATH):
    os.mkdir(SAMPLES_PATH)

with open(LOG_FOLDER_PATH + '/errors.log', 'w') as f:
    f.write('# iter data_seen epoch cl_loss')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/accuracies.log', 'w') as f:
    f.write('# iter data_seen epoch train_acc test_acc')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/best_acc.log', 'w') as f:
    f.write('# iter data_seen epoch train_acc test_acc')
    f.write('\n')

print("Arguments:")
for arg in vars(args):
    print("\t{}: {}".format(arg, getattr(args, arg)))
print("")


# -------------------------
# Data Aumentation
# -------------------------
rotation_range = 20
width_shift_range = 0.028
height_shift_range = 0.016
shear_range = 0.14
zoom_range_center = 0.97
zoom_range_range = 0.18
zoom_range = (zoom_range_center - zoom_range_range * 0.5, zoom_range_center + zoom_range_range * 0.5)
random_curves_strength = 0.58
seed = 17
augm = Augmentation(rotation_range=rotation_range,  # In degrees
                    width_shift_range=width_shift_range,
                    height_shift_range=height_shift_range,
                    horizontal_flip=True,
                    shear_range=shear_range,  # In radians
                    zoom_range=zoom_range,  # >1 zoom out; <1 zoom in
                    channel_shift_range=0.0,  # 0-255
                    fill_mode='constant',  # 'nearest',
                    random_curves_strength=random_curves_strength,
                    seed=seed)
# -------------------------------------
# Create the model
# -------------------------------------
_, Discriminator = GeneratorAndDiscriminator(model_arch='dcgan')
all_real_data_conv = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])
real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [args.batch_size, args.output_dim])
real_labels = tf.placeholder(tf.float32, [args.batch_size, args.n_classes])
_, generated_labels = Discriminator(real_data, args.model_dim, n_classes=args.n_classes)

correct_prediction = tf.equal(tf.argmax(generated_labels, 1), tf.argmax(real_labels, 1))
#correct_prediction = tf.equal((tf.sigmoid(generated_labels)>0.5), real_labels>0.5)
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# ---------------------
# Trainers y LOSS
# ---------------------
#cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(labels=real_labels, logits=generated_labels)
cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(labels=real_labels, logits=generated_labels)
cross_entropy = tf.reduce_mean(cross_entropy)
w = lib.params_with_name('Disc.Class.W')[0]
reg = tf.nn.l2_loss(w)
cls_loss = cross_entropy + reg * args.wd
class_train_op = tf.train.AdamOptimizer(1e-4).minimize(cls_loss, var_list=lib.params_with_name('Disc.Class'))

# ----------------------
# Restore trained model
# ----------------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())
saver = tf.train.Saver(save_relative_paths=True)  # para continuar este entrenamiento
best_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))  # clasificador final
# para cargar entrenamiento anterior
disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Discriminator.'))
disc_saver.restore(session, save_path=GAN_CHECKPOINT_D)

# ---------------------
# Train loop
# ---------------------
if args.checkpoint_path != None:
    saver.restore(session, save_path=args.checkpoint_path)
    print("\nStarting in iter={}:".format(args.start_iter))

t = time.time()
best_acc = 0.
it_best = 0
last_it = 0
for iteration in range(args.start_iter, args.n_iters):

    epoch = iteration * args.batch_size / n_train
    start_time = time.time()

    # Train clasificator
    batch_xs, batch_ys = gen_train.next()
    batch_xs = augm.random_transform(batch_xs)
    batch_xs = to_64(batch_xs)
    cls_cost, _, train_acc = session.run([cls_loss, class_train_op, accuracy],
                                         feed_dict={all_real_data_conv: batch_xs, real_labels: batch_ys})


    # Check
    if iteration >= args.checkpoint_start_iter and iteration % args.checkpoint_iter == args.checkpoint_iter - 1:

        # Test accuracy
        test_acc = 0.
        for test_it in range(test_batches):
            batch_xs_test, batch_ys_test = gen_test.next()
            test_acc += session.run(accuracy, feed_dict={all_real_data_conv: batch_xs_test, real_labels: batch_ys_test})
        test_acc = test_acc / test_batches

        # Log and save best model
        if best_acc < test_acc:
            best_acc = test_acc
            it_best = iteration

            # Save best model
            best_saver.save(sess=session, save_path=os.path.join(BEST_CHECKPOINTS_FOLDER_PATH, "best_iter"),
                       global_step=iteration + 1)

            # Log best model
            with open(LOG_FOLDER_PATH + '/best_acc.log', 'a') as f:
                np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size,
                                epoch, train_acc, test_acc]], fmt='%1.3e')

        # Log model
        with open(LOG_FOLDER_PATH + '/errors.log', 'a') as f:
            np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size, epoch, cls_cost]],
                       fmt='%1.3e')

        with open(LOG_FOLDER_PATH + '/accuracies.log', 'a') as f:
            np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size, epoch,
                            train_acc, test_acc]], fmt='%1.3e')

        t2 = time.time() - t
        t += t2
        horas = t2 / (1 + iteration - last_it) / 3600. * 10000
        print "iter:%d/%d; epoch:%d; %4.2f horas. para 10000 iteraciones" % (iteration + 1,
                                                                             args.n_iters, epoch, horas)
        last_it = iteration + 1

        # saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter"), global_step=iteration + 1)

# Check last model
# saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter"), global_step=args.n_iters + 1)
print "Train class ended!\n"
