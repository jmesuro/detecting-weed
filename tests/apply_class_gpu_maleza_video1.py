import os
import numpy as np
import tensorflow as tf
import tflib as lib
from gan import GeneratorAndDiscriminator
import time
from PIL import Image

# ------------------------------------------------------------------------
# Carga un clasificador entrenado y clasifica todos los datos etiquetados
# input: clasificador + folder imagenes + folder output + threshold
# clasifica un ROI en cada imagenes parches de tamanio h=w, las marca
# y las guarda en folder output
# ------------------------------------------------------------------------

checkpoint_path = '/home/jmesuro/checkpoints/best_iter_c-93900'  # path del clasificador entrenado, ojo tamnio gan
images_path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left/'  # path images a clasificar
output_marked_images_path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_[PALLR_050_2020]/'  # path imagenes clasificadaas
log_folder = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_marked/'

if not os.path.exists(output_marked_images_path):
    os.mkdir(output_marked_images_path)
if not os.path.exists(log_folder):
    os.mkdir(log_folder)

model_arch = 'dcgan'
threshold = 0.50

#     W 1920
# ----------------------------------
# |                                |H 1080
# |          W_ROI 1472            |
# |       -----------------        |
# |       | ROI           |H_ROI   |
# |       |               |576     |
# |       |               |        |
# |marg_l |               |marg_l  |
# ----------------------------------
H = 1080
W = 1920

model_dim = 32
n_classes = 3
h = w = 64
c = 3
output_dim = h * w * c

parches_w = int(W/w)  # cantidad de parches por ancho del ROI, 23 original, 30 nuevo
parches_h = int(H/h) # cantidad de parches por alto del ROI, 9 original, 5 nuevo
batch_size = parches_w * parches_h  # cantidad de parches que entran en un ROI, es decir en una imagen


H_ROI = h*parches_h  # 576 original, 320 nuevo
W_ROI = w*parches_w  # 1472 original, 1920 nuevo

if W_ROI >= 1920:
    marg_l = 0
else:
    marg_l = (W - W_ROI) / 2 - 1

if H_ROI >= 1080:
    marg_s = 0
else:
    marg_s = H - H_ROI



with open(log_folder + '/time.log', 'w') as f:
    f.write('\n')

# -----------------------------------------------------------
# marca los bordes de rojo de un parche de 64x64
# -----------------------------------------------------------
def mark_patch(patch):
    borde = 3
    patch[0, 0:h - 1, 0:borde] = 255
    patch[1, 0:h - 1, 0:borde] = 0
    patch[2, 0:h - 1, 0:borde] = 0

    patch[0, 0:h - 1, h - borde - 1:h - 1] = 255
    patch[1, 0:h - 1, h - borde - 1:h - 1] = 0
    patch[2, 0:h - 1, h - borde - 1:h - 1] = 0

    patch[0, 0:borde, 0:h - 1] = 255
    patch[1, 0:borde, 0:h - 1] = 0
    patch[2, 0:borde, 0:h - 1] = 0

    patch[0, h - borde - 1:h - 1, 0:h - 1] = 255
    patch[1, h - borde - 1:h - 1, 0:h - 1] = 0
    patch[2, h - borde - 1:h - 1, 0:h - 1] = 0
    return patch


# -----------------------------------------------------------
# marca los bordes de rojo de un parche de 64x64
# -----------------------------------------------------------
def mark_patch_rojo(patch,  p):
    patch[0, 0:h - 1, 0:h-1] = p * 255
    return patch

# -----------------------------------------------------------
# patch_list: lista de parches correspondiente a un ROI
# arma el ROI con los parches y lo marca con un marco negro
# -----------------------------------------------------------
def make_roi(patch_list):
    b = np.zeros(shape=(H_ROI, W_ROI, c), dtype=np.uint8)
    for p in range(0, parches_h):
        for q in range(0, parches_w):
            im = patch_list.pop()
            im = im.transpose(1, 2, 0)  # channel last
            b[h * p:h * (p + 1), h * q:h * (q + 1), :] = im

    borde = 5
    b[0:H_ROI - 1, 0:borde, 0] = 0
    b[0:H_ROI - 1, 0:borde, 1] = 0
    #b[0:H_ROI - 1, 0:borde, 2] = 0

    b[0:H_ROI - 1, W_ROI - borde - 1:W_ROI - 1, 0] = 0
    b[0:H_ROI - 1, W_ROI - borde - 1:W_ROI - 1, 1] = 0
    #b[0:H_ROI - 1, W_ROI - borde - 1:W_ROI - 1, 2] = 0

    b[0:borde, 0:W_ROI - 1, 0] = 0
    b[0:borde, 0:W_ROI - 1, 1] = 0
    #b[0:borde, 0:W_ROI - 1, 2] = 0

    return b


# -------------------------------------------------
# labels: etiquetas correspondientes a parche_list
# parche_list: parches de un ROI
# arma una imagen con los parches marcados
# -------------------------------------------------
def make_marked_image(labels, parches_list):
    roi_l = []
    i_parche = 0

    for label in labels:

        parche = np.array(parches_list[i_parche], dtype=np.uint8)
        label = 1 - label
        parche[1, :, :] = parche[1, :, :] * label
        parche[2, :, :] = parche[2, :, :] * label

        if (1 - label) > threshold:  # si es maleza
            parche = mark_patch_rojo(parche,1-label)
        # else:

        roi_l.append(parche)
        i_parche = i_parche + 1

    roi_l.reverse()
    b = make_roi(roi_l)
    a = np.zeros(shape=(H, W, c), dtype=np.uint8)
    a[:, :, :] = np.asarray(im_original)
    a[marg_s:, marg_l:marg_l + W_ROI] = b
    im = Image.fromarray(a)
    return im


# -------------------------------------------------
# labels: etiquetas correspondientes a parche_list
# parche_list: parches de un ROI
# arma una imagen con los parches marcados
# -------------------------------------------------
def make_marked_image2(labels, parches_list, tr):
    roi_l = []
    i_parche = 0

    for label in labels:
        parche = np.array(parches_list[i_parche], dtype=np.uint8)
        if label[0] >= tr:  # si es maleza supera el tr
            parche = mark_patch(parche)
        roi_l.append(parche)
        i_parche = i_parche + 1

    roi_l.reverse()
    b = make_roi(roi_l)
    a = np.zeros(shape=(H, W, c), dtype=np.uint8)
    a[:, :, :] = np.asarray(im_original)
    a[marg_s:, marg_l:marg_l + W_ROI] = b
    im = Image.fromarray(a)
    return im

# -------------------------------------------------
# labels: etiquetas correspondientes a parche_list
# parche_list: parches de un ROI
# arma una imagen con los parches marcados
# -------------------------------------------------
def make_marked_image3(labels, parches_list, tr):
    roi_l = []
    i_parche = 0

    for label in labels:
        parche = np.array(parches_list[i_parche], dtype=np.uint8)

        parche = mark_patch_rojo(parche,label[0])
        roi_l.append(parche)
        i_parche = i_parche + 1

    roi_l.reverse()
    b = make_roi(roi_l)
    a = np.zeros(shape=(H, W, c), dtype=np.uint8)
    a[:, :, :] = np.asarray(im_original)
    a[marg_s:, marg_l:marg_l + W_ROI] = b
    im = Image.fromarray(a)
    return im

with open(log_folder + '/time.log', 'w') as f:
    f.write('\n')


# ---------------------------------------------------
# Armo el clasificador, toma de una imagen a la vez
# ---------------------------------------------------
images_crudas_t = tf.placeholder(tf.uint8, shape=[c, H_ROI, W_ROI])  # imagen entera cHN
a1 = tf.reshape(images_crudas_t, shape=[c, H_ROI / h, h, W_ROI / w, w])
a2 = tf.transpose(a1, perm=[1, 3, 0, 2, 4])

all_real_data_conv = tf.reshape(a2, shape=[H_ROI / h * W_ROI / w, c, h, w])
real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [batch_size, output_dim])
_, Discriminator = GeneratorAndDiscriminator(model_arch)
disc_real, generated_labels = Discriminator(real_data, model_dim, n_classes=n_classes)

generated_labels_sof = tf.nn.softmax(generated_labels)
# ----------------------
# Restore trained model
# ----------------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())
disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
disc_saver.restore(session, save_path=checkpoint_path)



ims_asarrays = []
i_image = 0
# labels = []



def int_to_string(i):
    if i < 10:
        return '0000' + str(i)
    if i >= 10 and i < 100:
        return '000' + str(i)
    if i >= 100 and i < 1000:
        return '00' + str(i)
    if i >= 1000 and i < 10000:
        return '0' + str(i)
    return str(i)


file = open(log_folder + "log.txt","w")
tiempos2 = []
tiempos1 = []

def prev_pos_frame(punto_fuga,frames,pos): # retorna la posicion anterior, aca esta la magia
    return 0

# n es el parametro recursivo
def calc_label(pos,prev_labels,n):

    label = 0
    for j in range(0,n):
        label = label +   # una lista por cada frame



    if (n == 0):
        return label/n
    else
        prev_labels


def calc_labels(parches,labels,prev_labels):
    # prev_labels  lista de labels de n frames anteriores

    pos = 0
    n = 5
    new_labels = []
    for label in labels:
        new_labels[pos] = calc_label(label,pos,prev_labels,n)

    return new_labels




for dirpath, _, filenames in os.walk(images_path):
    for f in filenames:
        path = os.path.abspath(os.path.join(dirpath, f))

        if not os.path.exists(output_marked_images_path + 'm_' + f[:-3] + 'jpg'):

            # l_imagenes_pasadas = open(...) aquí abro la lista de imágenes pasada
            
            im_original = Image.open(path) # imagen a clasificar
            roi_original = np.asarray(im_original)[H - H_ROI:, marg_l: marg_l + W_ROI]  # obtengo el ROI de la imagen
            batch_xs = roi_original.transpose(2, 0, 1)  # channel first

            t = time.time()
            _generated_labels, parches = session.run([generated_labels_sof, all_real_data_conv],
                                                     feed_dict={images_crudas_t: batch_xs})

            t1 = time.time() - t


            im = make_marked_image2(_generated_labels, parches, tr=threshold)  # marco la imagen
            im.save(output_marked_images_path + 'm_' + f[:-3] + 'jpg')

            i_image = i_image + 1
            # labels.append(_generated_labels)
            t2 = time.time() - t
            tiempos2.append(t2)
            tiempos1.append(t1)
            l = 'imagen numero ' + str(i_image) + ' tiempo1 ' + str(t1) + ' tiempo2 ' + str(t2)

            print l
            file.write(l + '\n')


print "class ended!\n"
file.write( 'promedio tiempos1, con escritura en disco: ' + str(sum(tiempos1) / len(tiempos1)) + '\n')
file.write( 'promedio tiempos2, con escritura en disco: ' + str(sum(tiempos2) / len(tiempos2)))
file.close()