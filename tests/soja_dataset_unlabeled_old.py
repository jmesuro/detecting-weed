"""Modulo de utilidades para el manejo de los dataset de soja no etiquetados.

Permite cargar los dataset en memoria batch a batch y usarlos a traves de un
iterador.
"""

import numpy as np
import h5py

from utils import crop_224_to_64


# ----------------------------------------------------
# Es un iterador que no se carga eficiente en memoria
# porque son mas de 1 millon de imagenes.
# ----------------------------------------------------
def make_generator(data_path, batch_size):
    epoch_count = [1]
    fp = h5py.File(data_path, 'r')
    all_unlab_data_h5 = fp["X"]
    n_images = all_unlab_data_h5.shape[0]
    im_size = all_unlab_data_h5.shape[2]

    def get_epoch():
        images = np.zeros((batch_size, 3, im_size, im_size), dtype='int32')
        files = range(n_images)
        random_state = np.random.RandomState(epoch_count[0])
        random_state.shuffle(files)
        epoch_count[0] += 1
        for n, idx in enumerate(files):
            image = all_unlab_data_h5[idx].astype('int32')
            images[n % batch_size] = image
            if n > 0 and n % batch_size == 0:
                yield images
    return get_epoch

def load(dataset_path, batch_size):
    """Permite hacer uso del dataset h5py del disco
    con iteradores, que solo cargane en memoria
    una iteracion a la vez.
    Asume que los en disco estan NCHW.


    Args:
      dataset_path:
       path del dataset h5py

    Returns:
      Un iterador random de paso batch_size
    """
    return make_generator(dataset_path, batch_size)

# ----------------------------------------------------
# Es un iterador que no se carga eficiente en memoria
# porque son mas de 1 millon de imagenes.
# ----------------------------------------------------
def make_generator_two_files(data_path1, data_path2, batch_size):
    epoch_count = [1]
    fp1 = h5py.File(data_path1, 'r')
    fp2 = h5py.File(data_path2, 'r')

    all_unlab_data_h5_1 = fp1["X"]
    all_unlab_data_h5_2 = fp2["X"]

    n_images_1 = all_unlab_data_h5_1.shape[0]
    n_images_2 = all_unlab_data_h5_2.shape[0]

    im_size = all_unlab_data_h5_1.shape[2]

    def get_epoch():
        images = np.zeros((batch_size, 3, im_size, im_size), dtype='int32')
        files = range(n_images_1 + n_images_2)
        random_state = np.random.RandomState(epoch_count[0])
        random_state.shuffle(files)
        epoch_count[0] += 1
        for n, idx in enumerate(files):
            if idx >= n_images_1:
                image = all_unlab_data_h5_2[idx - n_images_1].astype('int32')
            else:
                image = all_unlab_data_h5_1[idx].astype('int32')
            images[n % batch_size] = image
            if n > 0 and n % batch_size == 0:
                yield images
    return get_epoch







# ----------------------------------------------------
# Es un iterador que no se carga eficiente en memoria
# porque son mas de 1 millon de imagenes.
# ----------------------------------------------------
def make_generator_two_files_train_valid_test(all_unlab_data_h5_1, all_unlab_data_h5_2, batch_size,cell_size, files):
    epoch_count = [1]
    n_images_1 = all_unlab_data_h5_1.shape[0]
    im_size = all_unlab_data_h5_1.shape[2]

    def get_epoch():
        images = np.zeros((batch_size, 3, im_size, im_size), dtype='int32')
        images2 = np.zeros((batch_size, 3, cell_size, cell_size), dtype='int32')
        random_state = np.random.RandomState(epoch_count[0])
        random_state.shuffle(files)
        epoch_count[0] += 1

        for n, idx in enumerate(files):
            if idx >= n_images_1:
                image = all_unlab_data_h5_2[idx - n_images_1].astype('int32')
            else:
                image = all_unlab_data_h5_1[idx].astype('int32')
            images[n % batch_size] = image
            if n > 0 and n % batch_size == 0:
                if (cell_size == 64):
                    images2 = crop_224_to_64(images)
                else:
                    if (cell_size == 96):
                        images2 = crop_224_to_64(images)
                yield images2
    return get_epoch

# ----------------------------------------------------
# Es un iterador que no se carga eficiente en memoria
# porque son mas de 1 millon de imagenes.
# ----------------------------------------------------
def make_generator_two_files_d(data_path1, data_path2, batch_size):
    epoch_count = [1]
    fp1 = h5py.File(data_path1, 'r')
    fp2 = h5py.File(data_path2, 'r')

    all_unlab_data_h5_1 = fp1["X"]
    all_unlab_data_h5_2 = fp2["X"]

    n_images_1 = all_unlab_data_h5_1.shape[0]
    n_images_2 = all_unlab_data_h5_2.shape[0]

    im_size = all_unlab_data_h5_1.shape[2]

    def get_epoch():
        images = np.zeros((batch_size, 3, im_size, im_size), dtype='int32')
        domains = np.zeros((batch_size, 2), dtype='float64')
        files = range(n_images_1 + n_images_2)
        random_state = np.random.RandomState(epoch_count[0])
        random_state.shuffle(files)
        epoch_count[0] += 1
        for n, idx in enumerate(files):
            if idx >= n_images_1:
                domain = np.asanyarray([1., 0.])
                image = all_unlab_data_h5_2[idx - n_images_1].astype('int32')
            else:
                domain = np.asanyarray([0., 1.])
                image = all_unlab_data_h5_1[idx].astype('int32')
            images[n % batch_size] = image
            domains[n % batch_size] = domain
            if n > 0 and n % batch_size == 0:
                yield images, domains
    return get_epoch

# ----------------------------------------------------
# Es un iterador que no se carga eficiente en memoria
# porque son mas de 1 millon de imagenes.
# ----------------------------------------------------
def make_generator_no_shuffle(data_path, batch_size):
    epoch_count = [1]
    fp = h5py.File(data_path, 'r')
    all_unlab_data_h5 = fp["X"]
    n_images = all_unlab_data_h5.shape[0]
    im_size = all_unlab_data_h5.shape[2]

    def get_epoch():
        images = np.zeros((batch_size, 3, im_size, im_size), dtype='int32')
        files = range(n_images)
        epoch_count[0] += 1
        for n, idx in enumerate(files):
            image = all_unlab_data_h5[idx].astype('int32')
            images[n % batch_size] = image
            if n > 0 and n % batch_size == 0:
                yield images
    return get_epoch


# ----------------------------------------------------
# Es un iterador que no se carga eficiente en memoria
# porque son mas de 1 millon de imagenes.
# ----------------------------------------------------
def make_generator_e(data_path, batch_size):
    epoch_count = [1]
    fp = h5py.File(data_path, 'r')
    all_unlab_data_h5 = fp["X"]
    n_images = all_unlab_data_h5.shape[0]
    im_size = all_unlab_data_h5.shape[2]

    def get_epoch():
        images = np.zeros((batch_size, 3, im_size, im_size), dtype='int32')
        files = range(n_images)
        random_state = np.random.RandomState(epoch_count[0])
        random_state.shuffle(files)
        epoch_count[0] += 1
        for n, idx in enumerate(files):
            image = all_unlab_data_h5[idx].astype('int32')
            images[n % batch_size] = image
            if n > 0 and n % batch_size == 0:
                yield images
    return get_epoch

def load_no_shuffle(patchpath_u, batch_size):
    return make_generator_no_shuffle(patchpath_u, batch_size)



def load_all_comp(patchpath_u_fot, patchpath_u_robot, batch_size):

    fp1 = h5py.File(patchpath_u_fot, 'r')
    fp2 = h5py.File(patchpath_u_robot, 'r')

    all_unlab_data_h5_1 = fp1["X"]
    all_unlab_data_h5_2 = fp2["X"]

    n_images_1 = all_unlab_data_h5_1.shape[0]
    n_images_2 = all_unlab_data_h5_2.shape[0]

    p_train = 0.63
    p_valid = 0.21
    n_total = n_images_1 + n_images_2
    n_train = int(p_train * n_total)
    n_valid = int(p_valid * n_total)
    n_test = n_total - n_train - n_valid

    files_train = range(n_train)
    files_valid = range(n_train, n_train + n_valid)
    files_test = range(n_train + n_valid, n_total)

    return (
        make_generator_two_files_train_valid_test(all_unlab_data_h5_1, all_unlab_data_h5_2, batch_size,96, files_train),
        make_generator_two_files_train_valid_test(all_unlab_data_h5_1, all_unlab_data_h5_2, batch_size,64, files_valid),
        make_generator_two_files_train_valid_test(all_unlab_data_h5_1, all_unlab_data_h5_2, batch_size,64, files_test),
        (n_train,n_valid,n_test)
    )


def load_all(patchpath_u_fot, patchpath_u_robot, batch_size):
    return make_generator_two_files(patchpath_u_fot, patchpath_u_robot, batch_size)


def load_all_domain(patchpath_u_fot, patchpath_u_robot, batch_size):
    return make_generator_two_files_d(patchpath_u_fot, patchpath_u_robot, batch_size)


