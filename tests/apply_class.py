import os
import sys
import time
import tflib.soja_dataset_labeled
import numpy as np
import tensorflow as tf
import tflib as lib
from gan import GeneratorAndDiscriminator
import argparse
import time

# ------------------------------------------------------------------------
# Carga un clasificador entrenado y clasifica todos los datos etiquetados
# ------------------------------------------------------------------------
sys.path.append(os.getcwd())
parser = argparse.ArgumentParser()
parser.add_argument("--datapath", type=str, default='/home/jmesuro/datasets/soja_cifasis/')
parser.add_argument("--model_arch", type=str, default='dcgan')  # model arch: 'good', 'dcgan'
parser.add_argument("--batch_size", type=int, default=128)  # Batch size. Must be a multiple of N_GPUS
parser.add_argument("--n_classes", type=int, default=2)  # How many classes to train for
parser.add_argument("--output_dim", type=int, default=64 * 64 * 3)  # Number of pixels in each image
parser.add_argument("--checkpoint_path", type=str, default='/home/jmesuro/chkpoint/best_iter-73100')
args = parser.parse_args()


# -------------------------------------
# Create the model
# -------------------------------------
_, Discriminator = GeneratorAndDiscriminator(model_arch='dcgan')
all_real_data_conv = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])
real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [args.batch_size, args.output_dim])
real_labels = tf.placeholder(tf.float32, [args.batch_size, args.n_classes])
disc_real, generated_labels, _ = Discriminator(real_data, n_classes=args.n_classes)

correct_prediction = tf.equal(tf.argmax(generated_labels, 1), tf.argmax(real_labels, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# ------------------
# Get data iterators
# ------------------
data_gen = lib.soja_dataset_labeled.load_all_data(args.datapath, args.batch_size)
data_len = lib.soja_dataset_labeled.get_all_data_len()


def inf_data_gen():
    while True:
        for images, targets in data_gen():
            yield images, targets


gen_data = inf_data_gen()
t = time.time()

# ----------------------
# Restore trained model
# ----------------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())
disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
disc_saver.restore(session,save_path=args.checkpoint_path)

all_generated_labels = []
all_accs = []

for iteration in range(0, data_len / args.batch_size):
    batch_xs, batch_ys = gen_data.next()
    batch_xs = batch_xs
    t = time.time()
    _generated_labels, acc = session.run([generated_labels, accuracy],
                                         feed_dict={all_real_data_conv: batch_xs, real_labels: batch_ys})
    print time.time() - t
    all_generated_labels.append(_generated_labels)
    all_accs.append(acc)

final_labels = []
for i_label_batch in all_generated_labels:
    for i_label in i_label_batch:
        aux_list = list(i_label)
        final_labels.append(aux_list.index(max(aux_list)))

np.savetxt('/home/jmesuro/final_labels.npy', final_labels, fmt='%s')
print "class ended!\n"
