"""Modulo de utilidades para el manejo de los dataset de soja etiquetados.

Permite cargar los dataset en memoria y usarlos a traves de un iterador.
"""

import numpy as np
from PIL import Image


def labeled_generator(data, batch_size, size, random=False):
    """Genera un iterador sobre data.

    Args:
      data:
       dataset numpy
      batch_size:
      size:
       tamanio de la patch
      random:
       para obtener o no de forma aleatoria los datos del batch

    Returns:
      un iterador sobre data
    """

    all_images, all_labels = data
    labels_dim = all_labels.shape[1]
    epoch_count = [1]
    n_images = len(data[0])
    def get_epoch():
        images = np.zeros((batch_size, 3, size, size), dtype='int32')
        labels = np.zeros((batch_size, labels_dim), dtype='float64')
        files = range(n_images)

        if random == True:
            random_state = np.random.RandomState(epoch_count[0])
            random_state.shuffle(files)

        epoch_count[0] += 1
        for n, idx in enumerate(files):
            image = all_images[idx].astype('int32')
            label = all_labels[idx].astype('float64')
            images[n % batch_size] = image
            labels[n % batch_size] = label
            if n > 0 and (n+1) % batch_size == 0:
                yield (images, labels)

    return get_epoch


def load_train_valid_test(train_path, test_path, labels_train_path, labels_test_path, batch_size, cel_size):
    """Carga los distintos dataset del disco y devuelve iteradores.
    Asume que los en disco estan en NHWC y los pasa a NCHW.
    Sobre el dataset de train saca una porcion para validacion

    Args:
      train_path:
       path del dataset para train, archivo npy
      test_path:
       path del dataset para test, archivo npy
      labels_train_path:
       path con los label de entrenamiento
      labels_test_path:
       path con los label de test
      batch_size:
      cel_size:

    Returns:
      Tres iteradores, train, val, test.
      Con la respectiva cantidad de elementos totales para cada uno.
    """

    train = np.load(train_path).transpose((0, 3, 1, 2))  # from channel last to channel first
    test = np.load(test_path).transpose((0, 3, 1, 2))  # from channel last to channel first

    labels_train = np.load(labels_train_path)
    labels_test = np.load(labels_test_path)

    p_train = 0.75
    n_total = train.shape[0]
    n_train = int(p_train * n_total)
    n_valid = int(n_total - n_train)
    n_test = test.shape[0]

    train_data = (train[:n_train, :, :, :], labels_train[:n_train, ])

    if cel_size == 224:
        valid_data = (cut_224_to_192(train[n_train:, :, :, :]), labels_train[n_train:, ])
        test_data = (cut_224_to_192(test), labels_test)
    else:
        valid_data = (cut_96_to_64(train[n_train:, :, :, :]), labels_train[n_train:, ])
        test_data = (cut_96_to_64(test[:, :, :, :]), labels_test[:, ])

    return (
               labeled_generator(train_data, batch_size, size=cel_size, random=True),
               labeled_generator(valid_data, batch_size, size=cel_size-32, random=False),
               labeled_generator(test_data, batch_size, size=cel_size-32, random=False),
           ), (n_train, n_valid, n_test)


def load_train_test(train_path, test_path, labels_train_path, labels_test_path, batch_size, cel_size):
    """Carga los distintos dataset del disco y devuelve iteradores.
        Asume que los en disco estan en NHWC y los pasa a NCHW.

    Args:
      train_path:
       path del dataset para train, archivo npy
      test_path:
       path del dataset para test, archivo npy
      labels_train_path:
       path con los label de entrenamiento
      labels_test_path:
       path con los label de test
      batch_size:
      cel_size:

    Returns:
      Dos iteradores, train, test.
      Con la respectiva cantidad de elementos totales para cada uno.
    """

    train = np.load(train_path).transpose((0, 3, 1, 2))  # from channel last to channel first
    test = np.load(test_path).transpose((0, 3, 1, 2))  # from channel last to channel first

    labels_train = np.load(labels_train_path)
    labels_test = np.load(labels_test_path)

    n_train = train.shape[0]
    n_test = test.shape[0]

    train_data = (train, labels_train)
    if cel_size == 224:
        test_data = (cut_224_to_192(test), labels_test)
    else:
        test_data = (cut_96_to_64(test), labels_test)

    return (
               labeled_generator(train_data, batch_size, size=cel_size, random=True),
               labeled_generator(test_data, batch_size, size=cel_size-32, random=False),
           ), (n_train, n_test)


# para calcular ROC
def all_data_robot(pachpath_l_robot, labelpath_robot):
    patches_robot = np.load(pachpath_l_robot)
    patches_robot = patches_robot.transpose((0, 3, 1, 2))  # from channel last to channel first
    labelpath_robot = np.load(labelpath_robot)

    return cut_96_to_64(patches_robot), labelpath_robot


# para calcular ROC
def all_data_entornos_robot(pachpath_l_robot, labelpath_robot):
    patches_robot = np.load(pachpath_l_robot)
    patches_robot = patches_robot.transpose((0, 3, 1, 2))  # from channel last to channel first
    labelpath_robot = np.load(labelpath_robot)

    patches_robot_192 = cut_224_to_192(patches_robot)
    patches_robot_e = resize_192_to_64(patches_robot_192)
    patches_robot_p = cut_192_to_64(patches_robot_192)

    return patches_robot_p, patches_robot_e, labelpath_robot


# Hace un crop en un batch de imagenes de 96x96 a 64x64
def cut_96_to_64(batch):
    if (batch.shape[2] == 96):
        return batch[:, :, 16:80, 16:80]
    return batch

# Hace un crop en un batch de imagenes de 224x224 a 64x64
def cut_224_to_64(batch):
    return cut_192_to_64(cut_224_to_192(batch))

# Hace un crop en un batch de imagenes de 224x224 a 192x192
def cut_224_to_192(batch):
    return batch[:, :, 16:208, 16:208]


# Hace un crop en un batch de imagenes de 192x192 a 64x64
def cut_192_to_64(batch):
    return batch[:, :, 64:128, 64:128]


# Achica un batch de imagenes de 192x192 a 64x64
def resize_192_to_64(batch):
    a = np.zeros(shape=(batch.shape[0], 3, 64, 64), dtype=batch.dtype)
    for i in range(0, batch.shape[0]):
        ai = batch[i].transpose((1, 2, 0))  # from channel first to channel last
        ai = np.array(ai, dtype=np.uint8)
        imi = (Image.fromarray(ai)).resize((64, 64))
        ai = (np.asarray(imi)).transpose(2, 0, 1)  # from channel last to channel first
        ai = np.array(ai, dtype=batch.dtype)
        a[i] = ai
    return a

# def load_train_test_comp(train_path, test_path, labels_train_path, labels_test_path, batch_size, cel_size, random=False):
#     train = np.load(train_path).transpose((0, 3, 1, 2))  # from channel last to channel first
#     test = np.load(test_path).transpose((0, 3, 1, 2))  # from channel last to channel first
#
#     labels_train = np.load(labels_train_path)
#     labels_test = np.load(labels_test_path)
#
#     n_train = train.shape[0]
#     n_test = test.shape[0]
#
#     train_data = (train, labels_train)
#     if cel_size == 224:
#         test_data = (cut_224_to_192(test), labels_test)
#     else:
#         test_data = (cut_96_to_64(test), labels_test)
#
#     return (
#                labeled_generator(train_data, batch_size, size=cel_size, random=random),
#                labeled_generator(test_data, batch_size, size=cel_size-32, random=False),
#            ), (n_train, n_test)
#
#
# def load_train_valid_test_shuffled_comp(parches, labels, batch_size, cel_size, random=False):
#     parches = np.load(parches).transpose((0, 3, 1, 2))  # from channel last to channel first
#
#     labels = np.load(labels)
#
#     p_train = 0.63
#     p_valid = 0.21
#     n_total = parches.shape[0]
#     n_train = int(p_train * n_total)
#     n_valid = int(p_valid * n_total)
#     n_test = n_total - n_train - n_valid
#
#     train_data = (parches[:n_train, :, :, :], labels[:n_train, ])
#
#     if cel_size == 224:
#         valid_data = (cut_224_to_192(parches[n_train:n_train + n_valid, :, :, :]), labels[n_train:n_train + n_valid, ])
#         test_data = (cut_224_to_192(parches[n_train + n_valid:, :, :, :]), labels[n_train + n_valid:, ])
#     else:
#         valid_data = (cut_96_to_64(parches[n_train:n_train + n_valid, :, :, :]), labels[n_train:n_train + n_valid, ])
#         test_data = (cut_96_to_64(parches[n_train + n_valid:, :, :, :]), labels[n_train + n_valid:, ])
#
#     return (
#                labeled_generator(train_data, batch_size, size=cel_size, random=random),
#                labeled_generator(valid_data, batch_size, size=cel_size - 32, random=False),
#                labeled_generator(test_data, batch_size, size=cel_size - 32, random=False),
#            ), (n_train, n_valid, n_test)
