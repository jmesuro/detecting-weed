import numpy as np
import h5py


# ----------------------------------------------------
# Es un iterador que no se carga eficiente en memoria
# porque son mas de 1 millon de imagenes.
# ----------------------------------------------------
def make_generator(data_path, n_images, batch_size, im_size):
    epoch_count = [1]
    fp = h5py.File(data_path, 'r')
    all_unlab_data_h5 = fp["X"]

    def get_epoch():
        images = np.zeros((batch_size, 3, im_size, im_size), dtype='int32')
        files = range(n_images)
        #random_state = np.random.RandomState(epoch_count[0])
        #random_state.shuffle(files)
        epoch_count[0] += 1
        for n, idx in enumerate(files):
            image = all_unlab_data_h5[idx].astype('int32')
            images[n % batch_size] = image
            if n > 0 and n % batch_size == 0:
                yield images
    return get_epoch


def load( n_images=207, batch_size=64, im_size=64):
    data_path = '/home/jmesuro/videos_soja/robot_videos_MOV/test/'
    return make_generator(data_path + 'parches_ROIT2frames_robot_u.h5', n_images, batch_size, im_size)
