import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack
from matplotlib.colors import LogNorm
import matplotlib.image as mpimg
import math
from PIL import Image

img = plt.imread('/home/jmesuro/im.png').astype(float)


def plot_spectrum(im_fft):
    plt.imshow(np.abs(im_fft), norm=LogNorm(vmin=5))  # imprime el modulo de la amplitud
    plt.colorbar()


def rgb2gray(rgb):
    r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b

    return gray


# en realidad no es la frecuencia media, pero para el caso no importa
# por ejemplo para una imagen linealde 5 pixel,
# tenemos un vector de frequencia de 5 pixel,
# el valor de cada componente del vector corresponde a una frequencia,
# ese vector tendria las siguientes -2/5 -1/5 0 1/5 2/5
# pero yo tomo -2 -1 0 1 2  , en realidad tomo 0 1 2 porque los negativos son iguales
def mean_freq(xm):
    freqs = np.fft.fftshift( np.fft.fftfreq(xm.size))
    n = xm.size
    I = 0
    sum = 0
    for i in range(n / 2, n-1):
        sum = sum + (xm[i] * freqs[i])
        I = I + xm[i]
    return sum / I





#
# l = [int(255 * ((1 + math.sin(200000*z)) / 2)) for z in range(0, 500)]
# a = np.zeros(shape=(500, 500), dtype=np.uint8)
# a[:] = l
# im = Image.fromarray(a)
# im = im.convert('L')
# im.show()
# a_fft = np.abs(np.fft.fft2(im))
# im_fft = Image.fromarray(a_fft)
# im_fft.show()
# x = 4


N = 512

# en realidad la ferq trendria que ser esos valores dividido 512
# esos son los pixeles de distancia al cero, si el cero esta en 256 entonces esos son 266 276 286
freq1 = 10
freq2 = 20
freq3 = 30

a1 = 1
a2 = 0.5
a3 = 0.25

l = range(0, N)
l = [float(li) / N for li in l]
x1 = [a1 * math.cos(math.pi * 2 * li * freq1) for li in l]
x2 = [a2 * math.cos(math.pi * 2 * li * freq2) for li in l]
x3 = [a3 * math.cos(math.pi * 2 * li * freq3) for li in l]

x = [xi1 + xi2 + xi3 for xi1, xi2, xi3 in zip(x1, x2, x3)]

xm = np.fft.fftshift(np.abs(np.fft.fft(x)))
x = np.fft.fftshift(x)

# plt.plot(x)
# plt.show()

plt.plot(x)
#u = np.fft.fftfreq(xm.size)

m = mean_freq(xm)

plt.show()

# im.save('/home/jmesuro/im.png')
# #img = plt.imread('/home/jmesuro/im.png').astype(float)

# im = rgb2gray(img)
#
# #im = img
#
# plt.figure()
# plt.imshow(im, plt.cm.gray)
# plt.title('Original image')
#
#
# im_fft = np.fft.fft2(im)
# #im_fft = fftpack.fft2(im)
#
# plt.figure()
# plot_spectrum(im_fft)
# plt.title('Fourier transform')
