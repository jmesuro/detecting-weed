"""Modulo de utilidades para el manejo de los dataset de soja etiquetados.

Permite cargar los dataset en memoria y usarlos a traves de un iterador.
"""

import numpy as np
from tflib.utils import crop_224_to_192, crop_96_to_64, crop_192_to_64, resize_192_to_64



def labeled_generator(data, batch_size, size, random=False):
    """Genera un iterador sobre data.

    Args:
      data:
       dataset numpy
      batch_size:
      size:
       tamaño de la patch
      random:
       para obtener o no de forma aleatoria los datos del batch

    Returns:
      un iterador sobre data
    """

    all_images, all_labels = data
    labels_dim = all_labels.shape[1]
    epoch_count = [1]
    n_images = len(data[0])
    def get_epoch():
        images = np.zeros((batch_size, 3, size, size), dtype='int32')
        labels = np.zeros((batch_size, labels_dim), dtype='float64')
        files = list(range(n_images))

        if random == True:
            random_state = np.random.RandomState(epoch_count[0])
            random_state.shuffle(files)

        epoch_count[0] += 1
        for n, idx in enumerate(files):
            image = all_images[idx].astype('int32')
            label = all_labels[idx].astype('float64')
            images[n % batch_size] = image
            labels[n % batch_size] = label
            if n > 0 and (n+1) % batch_size == 0:
                yield (images, labels)

    return get_epoch


def load_train_valid_test(train_path, test_path, labels_train_path, labels_test_path, batch_size, cel_size):
    """Carga los distintos dataset del disco y devuelve iteradores.
    Asume que los en disco estan en NHWC y los pasa a NCHW.
    Sobre el dataset de train saca una porción para validación

    Args:
      train_path:
       path del dataset para train, archivo npy
      test_path:
       path del dataset para test, archivo npy
      labels_train_path:
       path con los label de entrenamiento
      labels_test_path:
       path con los label de test
      batch_size:
      cel_size:

    Returns:
      Tres iteradores, train, val, test.
      Con la respectiva cantidad de elementos totales para cada uno.
    """

    train = np.load(train_path).transpose((0, 3, 1, 2))  # from channel last to channel first
    test = np.load(test_path).transpose((0, 3, 1, 2))  # from channel last to channel first

    labels_train = np.load(labels_train_path)
    labels_test = np.load(labels_test_path)

    p_train = 0.75
    n_total = train.shape[0]
    n_train = int(p_train * n_total)
    n_valid = int(n_total - n_train)
    n_test = test.shape[0]

    train_data = (train[:n_train, :, :, :], labels_train[:n_train, ])

    if cel_size == 224:
        valid_data = (crop_224_to_192(train[n_train:, :, :, :]), labels_train[n_train:, ])
        test_data = (crop_224_to_192(test), labels_test)
    else:
        valid_data = (crop_96_to_64(train[n_train:, :, :, :]), labels_train[n_train:, ])
        test_data = (crop_96_to_64(test[:, :, :, :]), labels_test[:, ])

    return (
               labeled_generator(train_data, batch_size, size=cel_size, random=True),
               labeled_generator(valid_data, batch_size, size=cel_size-32, random=False),
               labeled_generator(test_data, batch_size, size=cel_size-32, random=False),
           ), (n_train, n_valid, n_test)


def load_train_test(train_path, test_path, labels_train_path, labels_test_path, batch_size, cel_size):
    """Carga los distintos dataset del disco y devuelve iteradores.
    Asume que los en disco estan en NHWC y los pasa a NCHW.

    Args:
      train_path:
       path del dataset para train, archivo npy
      test_path:
       path del dataset para test, archivo npy
      labels_train_path:
       path con los label de entrenamiento
      labels_test_path:
       path con los label de test
      batch_size:
      cel_size:

    Returns:
      Dos iteradores, train, test.
      Con la respectiva cantidad de elementos totales para cada uno.
    """

    train = np.load(train_path).transpose((0, 3, 1, 2))  # from channel last to channel first
    test = np.load(test_path).transpose((0, 3, 1, 2))  # from channel last to channel first

    labels_train = np.load(labels_train_path)
    labels_test = np.load(labels_test_path)

    n_train = train.shape[0]
    n_test = test.shape[0]

    train_data = (train, labels_train)
    if cel_size == 224:
        test_data = (crop_224_to_192(test), labels_test)
    else:
        test_data = (crop_96_to_64(test), labels_test)

    return (
               labeled_generator(train_data, batch_size, size=cel_size, random=True),
               labeled_generator(test_data, batch_size, size=cel_size-32, random=False),
           ), (n_train, n_test)


def load_all_dataset(dataset_path, label_path):
    """Carga un dataset con sus labels del disco y devuelve la matriz numpy.
    Asume que los en disco estan en NHWC y los pasa a NCHW.
    Y que es para el dataset de parches de tamanio NHWC <-> N,96,96,C
    Se usa para calular ROC y PR

    Args:
      dataset_path:
       path del dataset para, archivo npy
      label_path:
       path con los label

    Returns:
      Dos el dataset numpy, dataset
      de N,C,64,64
      con sus labels
    """
    patches_robot = np.load(dataset_path)
    patches_robot = patches_robot.transpose((0, 3, 1, 2))  # from channel last to channel first
    labelpath_robot = np.load(label_path)

    return crop_96_to_64(patches_robot), labelpath_robot


def load_all_dataset_entornos(dataset_path, label_path):
    """Carga un datset de entornos con sus labels y devuelve la matriz numpy.
    Asume que los en disco estan en NHWC y los pasa a NCHW.
    Y que es para el dataset de parches de tamanio NHWC <-> N,224,224,C
    Se usa para calular ROC y PR

    Args:
      dataset_path:
       path del dataset para, archivo npy
      label_path:
       path con los label

    Returns:
      Dos matrices numpy, dataset parches, dataset entornos
      de N,C,64,64 ; N,C,192,192;
      con sus labels
    """
    parches = np.load(dataset_path)
    parches = parches.transpose((0, 3, 1, 2))  # from channel last to channel first
    labels = np.load(label_path)

    parches_192 = crop_224_to_192(parches)
    parches_e = resize_192_to_64(parches_192)
    parches_p = crop_192_to_64(parches_192)

    return parches_p, parches_e, labels


def load_train_valid_test_one_file(dataset_path, label_path, batch_size, cel_size):
    """Carga un dataset del disco y devuelve iteradores.
    Asume que los en disco estan en NHWC y los pasa a NCHW.
    Sobre el dataset de train saca una porcion para validacion

    Args:
      train_path:
       path del dataset para train, archivo npy
      test_path:
       path del dataset para test, archivo npy
      labels_train_path:
       path con los label de entrenamiento
      labels_test_path:
       path con los label de test
      batch_size:
      cel_size:

    Returns:
      Tres iteradores, train, val, test.
      Con la respectiva cantidad de elementos totales para cada uno.
    """

    parches = np.load(dataset_path)
    parches = parches.transpose((0, 3, 1, 2))  # from channel last to channel first
    labels = np.load(label_path)

    n_total = labels.shape[0]

    p_train = 0.63
    p_valid = 0.21
    n_train = int(p_train * n_total)
    n_valid = int(p_valid * n_total)
    n_test = n_total - n_train - n_valid

    if cel_size == 224:
        valid_data = (crop_224_to_192(parches[n_train:n_train + n_valid, :, :, :]), labels[n_train:n_train + n_valid, ])
        test_data = (crop_224_to_192(parches[n_train + n_valid:, :, :, :]), labels[n_train + n_valid:, ])
    else:
        valid_data = (crop_96_to_64(parches[n_train:n_train + n_valid, :, :, :]), labels[n_train:n_train + n_valid, ])
        test_data = (crop_96_to_64(parches[n_train + n_valid:, :, :, :]), labels[n_train + n_valid:, ])

    train_data = (parches[:n_train, :, :, :], labels[:n_train, ])

    return (
               labeled_generator(train_data, batch_size, size=cel_size,random=True),  # 96 para data aum
               labeled_generator(valid_data, batch_size, size=cel_size-32,random=False),
               labeled_generator(test_data, batch_size,  size=cel_size-32,random=False)
           ), (n_train, n_valid, n_test)

