"""Modulo de utilidades para el manejo de los dataset de soja no etiquetados.

Permite cargar los dataset en memoria batch a batch y usarlos a traves de un
iterador.
"""

import numpy as np
import h5py

def make_generator(data_path, batch_size):
    """Genera un iterador sobre data_path de batch_size.
    Obtiene los batch de forma aleatoria
    Asume que los en disco estan NCHW.

    Returns:
      Un iterador random de paso batch_size
    """
    epoch_count = [1]
    fp = h5py.File(data_path, 'r')
    all_unlab_data_h5 = fp["X"]
    n_images = all_unlab_data_h5.shape[0]
    im_size = all_unlab_data_h5.shape[2]

    def get_epoch():
        images = np.zeros((batch_size, 3, im_size, im_size), dtype='int32')
        files = list(range(n_images))
        random_state = np.random.RandomState(epoch_count[0])
        random_state.shuffle(files)
        epoch_count[0] += 1
        for n, idx in enumerate(files):
            image = all_unlab_data_h5[idx].astype('int32')
            images[n % batch_size] = image
            if n > 0 and n % batch_size == 0:
                yield images
    return get_epoch


def make_generator_two_files(data_path1, data_path2, batch_size):
    """Genera un iterador sobre data_path1 y data_path2 de batch_size.
    Obtiene los batch de forma aleatoria
    Asume que los en disco estan NCHW.

    Returns:
      Un iterador random de paso batch_size
    """
    epoch_count = [1]
    fp1 = h5py.File(data_path1, 'r')
    fp2 = h5py.File(data_path2, 'r')

    all_unlab_data_h5_1 = fp1["X"]
    all_unlab_data_h5_2 = fp2["X"]

    n_images_1 = all_unlab_data_h5_1.shape[0]
    n_images_2 = all_unlab_data_h5_2.shape[0]

    im_size = all_unlab_data_h5_1.shape[2]

    def get_epoch():
        images = np.zeros((batch_size, 3, im_size, im_size), dtype='int32')
        files = list(range(n_images_1 + n_images_2))
        random_state = np.random.RandomState(epoch_count[0])
        random_state.shuffle(files)
        epoch_count[0] += 1
        for n, idx in enumerate(files):
            if idx >= n_images_1:
                image = all_unlab_data_h5_2[idx - n_images_1].astype('int32')
            else:
                image = all_unlab_data_h5_1[idx].astype('int32')
            images[n % batch_size] = image
            if n > 0 and n % batch_size == 0:
                yield images
    return get_epoch

def load(dataset_path, batch_size):
    """Permite hacer uso del dataset h5py del disco
    con iteradores, que solo cargane en memoria
    una iteracion a la vez.
    Asume que los en disco estan NCHW.


    Args:
      dataset_path:
       path del dataset h5py

    Returns:
      Un iterador random de paso batch_size
    """
    return make_generator(dataset_path, batch_size)

def load_all(data_path1, data_path2, batch_size):
    """Permite hacer uso de dos dataset h5py del disco
    con iteradores que solo cargane en memoria
    una iteracion a la vez.
    Asume que los en disco estan NCHW.

    Args:
      dataset_path:
       path del dataset h5py

    Returns:
      Un iterador random de paso batch_size
    """
    return make_generator_two_files(data_path1, data_path2, batch_size)

