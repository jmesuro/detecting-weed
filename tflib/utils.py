"""Modulo de utilidades para el manejo de parches y entornos de los dataset.
"""


from PIL import Image
import numpy as np

def crop_96_to_64(batch):
    """Hace un crop central en un batch de imagenes de 96x96 a 64x64
    Asume que los datos estab en NCHW

    Args:
      batch:
       batch de imagenes numpy NCHW <--> N,C,96,96

    Returns:
      batch de imagenes numpy NCHW <--> N,C,64,64
    """

    if (batch.shape[2] == 96):
        return batch[:, :, 16:80, 16:80]
    return batch



def crop_224_to_192(batch):
    """Hace un crop central en un batch de imagenes de 224x224 a 64x64
    Asume que los datos estab en NCHW

    Args:
      batch:
       batch de imagenes numpy NCHW <--> N,C,224,224

    Returns:
      batch de imagenes numpy NCHW <--> N,C,192,192
    """
    return batch[:, :, 16:208, 16:208]


def crop_192_to_64(batch):
    """Hace un crop central en un batch de imagenes de 224x224 a 64x64
    Asume que los datos estab en NCHW

    Args:
      batch:
       batch de imagenes numpy NCHW <--> N,C,192,192

    Returns:
      batch de imagenes numpy NCHW <--> N,C,64,64
    """
    return batch[:, :, 64:128, 64:128]

def crop_224_to_64(batch):
    """Hace un crop central en un batch de imagenes de 224x224 a 64x64
    Asume que los datos estab en NCHW

    Args:
      batch:
       batch de imagenes numpy NCHW <--> N,C,224,224

    Returns:
      batch de imagenes numpy NCHW <--> N,C,64,64
    """
    return crop_192_to_64(crop_224_to_192(batch))


def resize_192_to_64(batch):
    """Hace un resize de un batch de imagenes de 192x192 a 64x64
    Asume que los datos estab en NCHW

    Args:
      batch:
       batch de imagenes numpy NCHW <--> N,C,192,192

    Returns:
      batch de imagenes numpy NCHW <--> N,C,64,64
    """
    a = np.zeros(shape=(batch.shape[0], 3, 64, 64), dtype=batch.dtype)
    for i in range(0, batch.shape[0]):
        ai = batch[i].transpose((1, 2, 0))  # from channel first to channel last
        ai = np.array(ai, dtype=np.uint8)
        imi = (Image.fromarray(ai)).resize((64, 64))
        ai = (np.asarray(imi)).transpose(2, 0, 1)  # from channel last to channel first
        ai = np.array(ai, dtype=batch.dtype)
        a[i] = ai
    return a