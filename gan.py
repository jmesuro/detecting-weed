import functools
import tensorflow as tf2
tf = tf2.compat.v1
tf.disable_v2_behavior()
import tflib as lib
import tflib.ops.linear
import tflib.ops.linear_p
import tflib.ops.linear_e
import tflib.ops.conv2d
import tflib.ops.conv2d_e
import tflib.ops.conv2d_p
import tflib.ops.batchnorm
import tflib.ops.deconv2d
import tflib.ops.deconv2d_e
import tflib.ops.deconv2d_p
import tflib.ops.layernorm


def GeneratorAndDiscriminator_comp_gan(model_arch):
    """
    Choose which generator and discriminator architecture to use by
    arg: model_arch.
    """

    # For actually generating decent samples, use this one
    if model_arch == 'good':
        return GoodGenerator, GoodDiscriminator

    # Baseline (G: DCGAN, D: DCGAN)
    elif model_arch == 'dcgan':
        return DCGANGenerator, DCGANDiscriminator_comp_gan

    else:
        raise Exception('You must choose an architecture!')

def GeneratorAndDiscriminator(model_arch):
    """
    Choose which generator and discriminator architecture to use by
    arg: model_arch.
    """

    # For actually generating decent samples, use this one
    if model_arch == 'good':
        return GoodGenerator, GoodDiscriminator

    # Baseline (G: DCGAN, D: DCGAN)
    elif model_arch == 'dcgan':
        return DCGANGenerator, DCGANDiscriminator

    else:
        raise Exception('You must choose an architecture!')


def GeneratorAndDiscriminator_pe(model_arch):
    """
    Choose which generator and discriminator architecture to use by
    arg: model_arch.
    """

    # For actually generating decent samples, use this one
    if model_arch == 'good':
        return GoodGenerator, GoodDiscriminator

    # Baseline (G: DCGAN, D: DCGAN)
    elif model_arch == 'dcgan':
        return DCGANGenerator_p, DCGANGenerator_e, DCGANDiscriminator_p, DCGANDiscriminator_e

    else:
        raise Exception('You must choose an architecture!')

def GeneratorAndDiscriminator_uda(model_arch):
    """
    Choose which generator and discriminator architecture to use by
    arg: model_arch.
    """

    # For actually generating decent samples, use this one
    if model_arch == 'good':
        return GoodGenerator, GoodDiscriminator

    # Baseline (G: DCGAN, D: DCGAN)
    elif model_arch == 'dcgan':
        return DCGANGenerator, DCGANDiscriminator_uda

    else:
        raise Exception('You must choose an architecture!')

def LeakyReLU(x, alpha=0.2):
    return tf.maximum(alpha * x, x)


def Normalize(name, axes, inputs, is_training=True, mode='wgan-gp'):
    if ('Discriminator' in name) and (mode == 'wgan-gp'):
        if axes != [0, 2, 3]:
            raise Exception('Layernorm over non-standard axes is unsupported')
        return lib.ops.layernorm.Layernorm(name, [1, 2, 3], inputs)
    else:
        is_tr = tf.constant(is_training)
        return lib.ops.batchnorm.Batchnorm(name, axes, inputs, is_tr, fused=True)


def ConvMeanPool(name, input_dim, output_dim, filter_size, inputs, he_init=True, biases=True):
    output = lib.ops.conv2d.Conv2D(name, input_dim, output_dim, filter_size, inputs, he_init=he_init, biases=biases)
    output = tf.add_n(
        [output[:, :, ::2, ::2], output[:, :, 1::2, ::2], output[:, :, ::2, 1::2], output[:, :, 1::2, 1::2]]) / 4.
    return output


def MeanPoolConv(name, input_dim, output_dim, filter_size, inputs, he_init=True, biases=True):
    output = inputs
    output = tf.add_n(
        [output[:, :, ::2, ::2], output[:, :, 1::2, ::2], output[:, :, ::2, 1::2], output[:, :, 1::2, 1::2]]) / 4.
    output = lib.ops.conv2d.Conv2D(name, input_dim, output_dim, filter_size, output, he_init=he_init, biases=biases)
    return output


def UpsampleConv(name, input_dim, output_dim, filter_size, inputs, he_init=True, biases=True):
    output = inputs
    output = tf.concat([output, output, output, output], axis=1)
    output = tf.transpose(output, [0, 2, 3, 1])
    output = tf.depth_to_space(output, 2)
    output = tf.transpose(output, [0, 3, 1, 2])
    output = lib.ops.conv2d.Conv2D(name, input_dim, output_dim, filter_size, output, he_init=he_init, biases=biases)
    return output


def ResidualBlock(name, input_dim, output_dim, filter_size, inputs, resample=None, he_init=True):
    """
    resample: None, 'down', or 'up'
    """
    if resample == 'down':
        conv_shortcut = MeanPoolConv
        conv_1 = functools.partial(lib.ops.conv2d.Conv2D, input_dim=input_dim, output_dim=input_dim)
        conv_2 = functools.partial(ConvMeanPool, input_dim=input_dim, output_dim=output_dim)
    elif resample == 'up':
        conv_shortcut = UpsampleConv
        conv_1 = functools.partial(UpsampleConv, input_dim=input_dim, output_dim=output_dim)
        conv_2 = functools.partial(lib.ops.conv2d.Conv2D, input_dim=output_dim, output_dim=output_dim)
    elif resample == None:
        conv_shortcut = lib.ops.conv2d.Conv2D
        conv_1 = functools.partial(lib.ops.conv2d.Conv2D, input_dim=input_dim, output_dim=input_dim)
        conv_2 = functools.partial(lib.ops.conv2d.Conv2D, input_dim=input_dim, output_dim=output_dim)
    else:
        raise Exception('invalid resample value')

    if output_dim == input_dim and resample == None:
        shortcut = inputs  # Identity skip-connection
    else:
        shortcut = conv_shortcut(name + '.Shortcut', input_dim=input_dim, output_dim=output_dim, filter_size=1,
                                 he_init=False, biases=True, inputs=inputs)

    output = inputs
    output = Normalize(name + '.BN1', [0, 2, 3], output)
    output = tf.nn.relu(output)
    output = conv_1(name + '.Conv1', filter_size=filter_size, inputs=output, he_init=he_init, biases=False)
    output = Normalize(name + '.BN2', [0, 2, 3], output)
    output = tf.nn.relu(output)
    output = conv_2(name + '.Conv2', filter_size=filter_size, inputs=output, he_init=he_init)

    return shortcut + output


# ! Generators
def GoodGenerator(n_samples, noise=None, model_dim=64, output_dim=64*64*3):
    if noise is None:
        noise = tf.random_normal([n_samples, 128])

    output = lib.ops.linear.Linear('Generator.Input', 128, 4 * 4 * 8 * model_dim, noise)
    output = tf.reshape(output, [-1, 8 * model_dim, 4, 4])

    output = ResidualBlock('Generator.Res1', 8 * model_dim, 8 * model_dim, 3, output, resample='up')
    output = ResidualBlock('Generator.Res2', 8 * model_dim, 4 * model_dim, 3, output, resample='up')
    output = ResidualBlock('Generator.Res3', 4 * model_dim, 2 * model_dim, 3, output, resample='up')
    output = ResidualBlock('Generator.Res4', 2 * model_dim, 1 * model_dim, 3, output, resample='up')

    output = Normalize('Generator.OutputN', [0, 2, 3], output)
    output = tf.nn.relu(output)
    output = lib.ops.conv2d.Conv2D('Generator.Output', 1 * model_dim, 3, 3, output)
    output = tf.tanh(output)

    return tf.reshape(output, [-1, output_dim])

# ! Discriminators
def GoodDiscriminator(inputs, model_dim=64):
    output = tf.reshape(inputs, [-1, 3, 64, 64])
    output = lib.ops.conv2d.Conv2D('Discriminator.Input', 3, model_dim, 3, output, he_init=False)

    output = ResidualBlock('Discriminator.Res1', model_dim, 2 * model_dim, 3, output, resample='down')
    output = ResidualBlock('Discriminator.Res2', 2 * model_dim, 4 * model_dim, 3, output, resample='down')
    output = ResidualBlock('Discriminator.Res3', 4 * model_dim, 8 * model_dim, 3, output, resample='down')
    output = ResidualBlock('Discriminator.Res4', 8 * model_dim, 8 * model_dim, 3, output, resample='down')

    output = tf.reshape(output, [-1, 4 * 4 * 8 * model_dim])
    output = lib.ops.linear.Linear('Discriminator.Output', 4 * 4 * 8 * model_dim, 1, output)

    return tf.reshape(output, [-1])


def DCGANGenerator(n_samples,  model_dim, noise=None,output_dim=64*64*3, bn=True, is_training=True, nonlinearity=tf.nn.relu):
    lib.ops.conv2d.set_weights_stdev(0.02)
    lib.ops.deconv2d.set_weights_stdev(0.02)
    lib.ops.linear.set_weights_stdev(0.02)

    if noise is None:
        noise = tf.random_normal([n_samples, 128])

    output = lib.ops.linear.Linear('Generator.Input', 128, 4 * 4 * 8 * model_dim, noise)
    output = tf.reshape(output, [-1, 8 * model_dim, 4, 4])
    if bn:
        output = Normalize('Generator.BN1', [0, 2, 3], output, is_training=is_training)
    output = nonlinearity(output)

    output = lib.ops.deconv2d.Deconv2D('Generator.2', 8 * model_dim, 4 * model_dim, 5, output)
    if bn:
        output = Normalize('Generator.BN2', [0, 2, 3], output, is_training=is_training)
    output = nonlinearity(output)

    output = lib.ops.deconv2d.Deconv2D('Generator.3', 4 * model_dim, 2 * model_dim, 5, output)
    if bn:
        output = Normalize('Generator.BN3', [0, 2, 3], output, is_training=is_training)
    output = nonlinearity(output)

    output = lib.ops.deconv2d.Deconv2D('Generator.4', 2 * model_dim, model_dim, 5, output)
    if bn:
        output = Normalize('Generator.BN4', [0, 2, 3], output, is_training=is_training)
    output = nonlinearity(output)

    output = lib.ops.deconv2d.Deconv2D('Generator.5', model_dim, 3, 5, output)
    output = tf.tanh(output)

    lib.ops.conv2d.unset_weights_stdev()
    lib.ops.deconv2d.unset_weights_stdev()
    lib.ops.linear.unset_weights_stdev()

    return tf.reshape(output, [-1, output_dim])


def DCGANGenerator_p(n_samples,  model_dim, noise=None,output_dim=64*64*3, bn=True, is_training=True, nonlinearity=tf.nn.relu):
    lib.ops.conv2d.set_weights_stdev(0.02)
    lib.ops.deconv2d.set_weights_stdev(0.02)
    lib.ops.linear_p.set_weights_stdev(0.02)

    if noise is None:
        noise = tf.random_normal([n_samples, 128])

    output = lib.ops.linear_p.Linear('Generator_p.Input', 128, 4 * 4 * 8 * model_dim, noise)
    output = tf.reshape(output, [-1, 8 * model_dim, 4, 4])
    if bn:
        output = Normalize('Generator_p.BN1', [0, 2, 3], output, is_training=is_training)
    output = nonlinearity(output)

    output = lib.ops.deconv2d.Deconv2D('Generator_p.2', 8 * model_dim, 4 * model_dim, 5, output)
    if bn:
        output = Normalize('Generator_p.BN2', [0, 2, 3], output, is_training=is_training)
    output = nonlinearity(output)

    output = lib.ops.deconv2d.Deconv2D('Generator_p.3', 4 * model_dim, 2 * model_dim, 5, output)
    if bn:
        output = Normalize('Generator_p.BN3', [0, 2, 3], output, is_training=is_training)
    output = nonlinearity(output)

    output = lib.ops.deconv2d.Deconv2D('Generator_p.4', 2 * model_dim, model_dim, 5, output)
    if bn:
        output = Normalize('Generator_p.BN4', [0, 2, 3], output, is_training=is_training)
    output = nonlinearity(output)

    output = lib.ops.deconv2d.Deconv2D('Generator_p.5', model_dim, 3, 5, output)
    output = tf.tanh(output)

    lib.ops.conv2d.unset_weights_stdev()
    lib.ops.deconv2d.unset_weights_stdev()
    lib.ops.linear_p.unset_weights_stdev()

    return tf.reshape(output, [-1, output_dim])


def DCGANGenerator_e(n_samples,  model_dim, noise=None,output_dim=64*64*3, bn=True, is_training=True, nonlinearity=tf.nn.relu):
    lib.ops.conv2d_e.set_weights_stdev(0.02)
    lib.ops.deconv2d_e.set_weights_stdev(0.02)
    lib.ops.linear_e.set_weights_stdev(0.02)

    if noise is None:
        noise = tf.random_normal([n_samples, 128])

    output = lib.ops.linear_e.Linear('Generator_e.Input', 128, 4 * 4 * 8 * model_dim, noise)
    output = tf.reshape(output, [-1, 8 * model_dim, 4, 4])
    if bn:
        output = Normalize('Generator_e.BN1', [0, 2, 3], output, is_training=is_training)
    output = nonlinearity(output)

    output = lib.ops.deconv2d_e.Deconv2D('Generator_e.2', 8 * model_dim, 4 * model_dim, 5, output)
    if bn:
        output = Normalize('Generator_e.BN2', [0, 2, 3], output, is_training=is_training)
    output = nonlinearity(output)

    output = lib.ops.deconv2d_e.Deconv2D('Generator_e.3', 4 * model_dim, 2 * model_dim, 5, output)
    if bn:
        output = Normalize('Generator_e.BN3', [0, 2, 3], output, is_training=is_training)
    output = nonlinearity(output)

    output = lib.ops.deconv2d_e.Deconv2D('Generator_e.4', 2 * model_dim, model_dim, 5, output)
    if bn:
        output = Normalize('Generator_e.BN4', [0, 2, 3], output, is_training=is_training)
    output = nonlinearity(output)

    output = lib.ops.deconv2d_e.Deconv2D('Generator_e.5', model_dim, 3, 5, output)
    output = tf.tanh(output)

    lib.ops.conv2d_e.unset_weights_stdev()
    lib.ops.deconv2d_e.unset_weights_stdev()
    lib.ops.linear_e.unset_weights_stdev()

    return tf.reshape(output, [-1, output_dim])


def DCGANDiscriminator(inputs, model_dim, bn=True, nonlinearity=LeakyReLU, n_classes=2):
    output = tf.reshape(inputs, [-1, 3, 64, 64])

    lib.ops.conv2d.set_weights_stdev(0.02)
    lib.ops.deconv2d.set_weights_stdev(0.02)
    lib.ops.linear.set_weights_stdev(0.02)

    output = lib.ops.conv2d.Conv2D('Discriminator.1', 3, model_dim, 5, output, stride=2)
    output = nonlinearity(output)

    output = lib.ops.conv2d.Conv2D('Discriminator.2', model_dim, 2 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator.BN2', [0, 2, 3], output)
    output = nonlinearity(output)

    output = lib.ops.conv2d.Conv2D('Discriminator.3', 2 * model_dim, 4 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator.BN3', [0, 2, 3], output)
    output = nonlinearity(output)

    output = lib.ops.conv2d.Conv2D('Discriminator.4', 4 * model_dim, 8 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator.BN4', [0, 2, 3], output)

    output = nonlinearity(output)

    output1 = tf.reshape(output, [-1, 4 * 4 * 8 * model_dim])
    output2 = lib.ops.linear.Linear('Discriminator.Output', 4 * 4 * 8 * model_dim, 1, output1)
    unescaled_class = lib.ops.linear.Linear('Disc.Class', 4 * 4 * 8 * model_dim, n_classes, output1)

    lib.ops.conv2d.unset_weights_stdev()
    lib.ops.deconv2d.unset_weights_stdev()
    lib.ops.linear.unset_weights_stdev()

    return tf.reshape(output2, [-1]), unescaled_class



def DCGANDiscriminator_e(inputs, model_dim, bn=True, nonlinearity=LeakyReLU):
    output = tf.reshape(inputs, [-1, 3, 64, 64])

    lib.ops.conv2d_e.set_weights_stdev(0.02)
    lib.ops.deconv2d_e.set_weights_stdev(0.02)
    lib.ops.linear_e.set_weights_stdev(0.02)

    output = lib.ops.conv2d_e.Conv2D('Discriminator_e.1', 3, model_dim, 5, output, stride=2)
    output = nonlinearity(output)

    output = lib.ops.conv2d_e.Conv2D('Discriminator_e.2', model_dim, 2 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator_e.BN2', [0, 2, 3], output)
    output = nonlinearity(output)

    output = lib.ops.conv2d_e.Conv2D('Discriminator_e.3', 2 * model_dim, 4 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator_e.BN3', [0, 2, 3], output)
    output = nonlinearity(output)

    output = lib.ops.conv2d_e.Conv2D('Discriminator_e.4', 4 * model_dim, 8 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator_e.BN4', [0, 2, 3], output)

    output = nonlinearity(output)

    output1 = tf.reshape(output, [-1, 4 * 4 * 8 * model_dim])
    output2 = lib.ops.linear_e.Linear('Discriminator_e.Output', 4 * 4 * 8 * model_dim, 1, output1)

    lib.ops.conv2d_e.unset_weights_stdev()
    lib.ops.deconv2d_e.unset_weights_stdev()
    lib.ops.linear_e.unset_weights_stdev()

    return tf.reshape(output2, [-1]), output1


def DCGANDiscriminator_p(inputs, model_dim, bn=True, nonlinearity=LeakyReLU):
    output = tf.reshape(inputs, [-1, 3, 64, 64])

    lib.ops.conv2d_p.set_weights_stdev(0.02)
    lib.ops.deconv2d_p.set_weights_stdev(0.02)
    lib.ops.linear_p.set_weights_stdev(0.02)

    output = lib.ops.conv2d_p.Conv2D('Discriminator_p.1', 3, model_dim, 5, output, stride=2)
    output = nonlinearity(output)

    output = lib.ops.conv2d_p.Conv2D('Discriminator_p.2', model_dim, 2 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator_p.BN2', [0, 2, 3], output)
    output = nonlinearity(output)

    output = lib.ops.conv2d_p.Conv2D('Discriminator_p.3', 2 * model_dim, 4 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator_p.BN3', [0, 2, 3], output)
    output = nonlinearity(output)

    output = lib.ops.conv2d_p.Conv2D('Discriminator_p.4', 4 * model_dim, 8 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator_p.BN4', [0, 2, 3], output)

    output = nonlinearity(output)

    output1 = tf.reshape(output, [-1, 4 * 4 * 8 * model_dim])
    output2 = lib.ops.linear_p.Linear('Discriminator_p.Output', 4 * 4 * 8 * model_dim, 1, output1)

    lib.ops.conv2d_p.unset_weights_stdev()
    lib.ops.deconv2d_p.unset_weights_stdev()
    lib.ops.linear_p.unset_weights_stdev()

    return tf.reshape(output2, [-1]), output1


def DCGANDiscriminator_uda(inputs, model_dim, bn=True, nonlinearity=LeakyReLU, n_classes=2, n_domains = 2):
    output = tf.reshape(inputs, [-1, 3, 64, 64])

    lib.ops.conv2d.set_weights_stdev(0.02)
    lib.ops.deconv2d.set_weights_stdev(0.02)
    lib.ops.linear.set_weights_stdev(0.02)

    output = lib.ops.conv2d.Conv2D('Discriminator.1', 3, model_dim, 5, output, stride=2)
    output = nonlinearity(output)

    output = lib.ops.conv2d.Conv2D('Discriminator.2', model_dim, 2 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator.BN2', [0, 2, 3], output)
    output = nonlinearity(output)

    output = lib.ops.conv2d.Conv2D('Discriminator.3', 2 * model_dim, 4 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator.BN3', [0, 2, 3], output)
    output = nonlinearity(output)

    output = lib.ops.conv2d.Conv2D('Discriminator.4', 4 * model_dim, 8 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator.BN4', [0, 2, 3], output)

    output = nonlinearity(output)

    output1 = tf.reshape(output, [-1, 4 * 4 * 8 * model_dim])
    output2 = lib.ops.linear.Linear('Discriminator.Output', 4 * 4 * 8 * model_dim, 1, output1)
    unescaled_class = lib.ops.linear.Linear('Disc.Class', 4 * 4 * 8 * model_dim, n_classes, output1)
    unescaled_domain = lib.ops.linear.Linear('Discriminator_Uda', 4 * 4 * 8 * model_dim, n_domains, output1)

    lib.ops.conv2d.unset_weights_stdev()
    lib.ops.deconv2d.unset_weights_stdev()
    lib.ops.linear.unset_weights_stdev()

    return tf.reshape(output2, [-1]), unescaled_class, unescaled_domain


def DCGANDiscriminator_comp(inputs, model_dim, bn=True, nonlinearity=LeakyReLU, n_classes=3):
    output = tf.reshape(inputs, [-1, 3, 64, 64])

    lib.ops.conv2d.set_weights_stdev(0.02)
    lib.ops.deconv2d.set_weights_stdev(0.02)
    lib.ops.linear.set_weights_stdev(0.02)

    output = lib.ops.conv2d.Conv2D('Discriminator.1', 3, model_dim, 5, output, stride=2)
    output = nonlinearity(output)

    output = lib.ops.conv2d.Conv2D('Discriminator.2', model_dim, 2 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator.BN2', [0, 2, 3], output)
    output = nonlinearity(output)

    output = lib.ops.conv2d.Conv2D('Discriminator.3', 2 * model_dim, 4 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator.BN3', [0, 2, 3], output)
    output = nonlinearity(output)

    output = lib.ops.conv2d.Conv2D('Discriminator.4', 4 * model_dim, 8 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator.BN4', [0, 2, 3], output)

    output = nonlinearity(output)

    output1 = tf.reshape(output, [-1, 4 * 4 * 8 * model_dim])
    output2 = lib.ops.linear.Linear('Discriminator.Output', 4 * 4 * 8 * model_dim, 1, output1)  # si es real o no
    unescaled_class = lib.ops.linear.Linear('Disc.Class', 4 * 4 * 8 * model_dim, n_classes, output1)  # clasas

    lib.ops.conv2d.unset_weights_stdev()
    lib.ops.deconv2d.unset_weights_stdev()
    lib.ops.linear.unset_weights_stdev()

    #return tf.reshape(output2, [-1]), unescaled_class, output1
    return output1



def DCGANDiscriminator_comp_gan(inputs, model_dim, bn=True, nonlinearity=LeakyReLU, n_classes=2):
    output = tf.reshape(inputs, [-1, 3, 64, 64])

    lib.ops.conv2d.set_weights_stdev(0.02)
    lib.ops.deconv2d.set_weights_stdev(0.02)
    lib.ops.linear.set_weights_stdev(0.02)

    output = lib.ops.conv2d.Conv2D('Discriminator.1', 3, model_dim, 5, output, stride=2)
    output = nonlinearity(output)

    output = lib.ops.conv2d.Conv2D('Discriminator.2', model_dim, 2 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator.BN2', [0, 2, 3], output)
    output = nonlinearity(output)

    output = lib.ops.conv2d.Conv2D('Discriminator.3', 2 * model_dim, 4 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator.BN3', [0, 2, 3], output)
    output = nonlinearity(output)

    output = lib.ops.conv2d.Conv2D('Discriminator.4', 4 * model_dim, 8 * model_dim, 5, output, stride=2)
    if bn:
        output = Normalize('Discriminator.BN4', [0, 2, 3], output)

    output = nonlinearity(output)

    output1 = tf.reshape(output, [-1, 4 * 4 * 8 * model_dim])
    output2 = lib.ops.linear.Linear('Discriminator.Output', 4 * 4 * 8 * model_dim, 1, output1)  # si es real o no
    unescaled_class = lib.ops.linear.Linear('Disc.Class', 4 * 4 * 8 * model_dim, n_classes, output1)  # clasas

    lib.ops.conv2d.unset_weights_stdev()
    lib.ops.deconv2d.unset_weights_stdev()
    lib.ops.linear.unset_weights_stdev()
    #        gan                       class           flat
    return tf.reshape(output2, [-1]), unescaled_class, output1