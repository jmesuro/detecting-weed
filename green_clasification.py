""" Detector de green en plantas.
Basado en:
Greenness identification based on HSV decision tree
https://www.researchgate.net/publication/282526976_Greenness_identification_based_on_HSV_decision_tree
"""

import tflib as lib
import matplotlib.colors as colors
from PIL import Image
import numpy as np

BATCH_SIZE = 4

# --------------------------------
# Clasifica por color verde
# --------------------------------

# ------------------------------
# Get data input
# ------------------------------
#train_gen, dev_gen, test_gen = lib.soja_tesina_dataset.load(BATCH_SIZE)


def inf_train_gen():
    while True:
        for images, targets in train_gen():
            yield images, targets


def inf_test_gen():
    while True:
        for images, targets in test_gen():
            yield images, targets


def inf_valid_gen():
    while True:
        for images, targets in dev_gen():
            yield images, targets


gen_train = inf_train_gen()
gen_test = inf_test_gen()
gen_valid = inf_valid_gen()


# -------------------------
# 1 green, 0 other
# threshold = minimum green pixels
# -------------------------
def tag_batch(x, threshold):
    n = x.shape[0]
    y = np.zeros(shape=(n,1),dtype=np.float)
    for i in range(n):
        nr = np.array(x[i].transpose(1,2,0),dtype=np.uint8)
        rgb = Image.fromarray(nr)

        hsv = colors.rgb_to_hsv(rgb)


        H = hsv[:, :, 0] * 255
        S = hsv[:, :, 1] * 255
        V = hsv[:, :, 2] * 255

        if (H[((H < 40) | (H > 180))]).size > threshold:
            y[i] = 1.
    return y


x, _ = gen_train.next()
tag_batch(x, 2048)
