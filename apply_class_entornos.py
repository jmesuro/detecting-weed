"""Aplica un clasificador entornos.

Aplica la clasificacion a una lista de imagenes, que se suponen que son
frames consecutivos de un video sobre un campo de soja. Sobre cada frame
se define un ROI (region of interest) el cual se divide en celdas (patches)
iguales y se realiza la clasificacion de cada una en (soja, maleza, suelo).

Esta probado sobre frames son 1080 x 1920 y celdas de 64 x 64
"""

import os
import numpy as np
import tensorflow as tf2
tf = tf2.compat.v1
tf.disable_v2_behavior()
import tflib as lib
from gan import GeneratorAndDiscriminator_pe
import time
import tflib.ops.linear_c
from utils import listFiles, mark_frame, make_parches_list_p, make_parches_list_e
from tflib.utils import resize_192_to_64
from PIL import Image




def apply_class_file_in(input='/home/jmesuro/detecting-weed/in_16_test.in'):
    """Aplica clasificaciones definidas por los parametros en el archivo input.
    Esta pensado para aplicar varias clasificaciones sobre varias carpetas de
    frames de video.

    Args:
        input: es un archivo de txt con el siguiente formato:

        ... empty line
        [checkpint_path]
        [input_frame_path]
        [output_class_frame_path]
        [threshold]
        ... empty line
        [checkpint_path]
        [input_frame_path]
        [output_class_frame_path]
        [threshold]
        [model_dim]

        ej:

        /home/jmesuro/checkpoints/best_iter_class-66600
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/New Folder
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_[enrRRR_88]/
        0.88
        16

        /home/jmesuro/checkpoints/best_iter_class-66600
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/New Folder
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_[enrRRR_50]/
        0.50
        16
    """

    f = open(input)
    lines = f.read().split('\n')
    for l in range(0, len(lines) - 1, 6):
        checkpoint_path = lines[l + 1]
        input_frames_path = lines[l + 2]
        output_class_frames_path = lines[l + 3]
        threshold = float(lines[l + 4])
        model_dim = int(lines[l + 5])

        # obtiene la cantidad de frames de la carpeta input_frames_path
        total_frames = len(listFiles(input_frames_path))

        log_folder = output_class_frames_path

        if not os.path.exists(output_class_frames_path):
            os.mkdir(output_class_frames_path)
        if not os.path.exists(log_folder):
            os.mkdir(log_folder)

        main(checkpoint_path, input_frames_path, output_class_frames_path, log_folder, threshold, total_frames,
             model_dim)

def apply_class():
    """Aplica la clasificacion definida por los parametros:

            checkpoint_path
            input_frames_path
            output_class_frames_path
            model_dim
            threshold

        Esta pensado para aplicar una sola clasificaciones sobre una sola carpetas de
        frames de video.
    """
    checkpoint_path = '/home/jmesuro/checkpoints/best_iter_class-103200'  # path del clasificador entrenado, ojo tamnio gan
    input_frames_path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left/'  # path images a clasificar
    output_class_frames_path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_[enrRRR_88]/'  # path imagenes clasificadaas
    log_folder = output_class_frames_path
    total_frames = len(listFiles(input_frames_path))
    model_dim = 16
    threshold = 0.80

    if not os.path.exists(output_class_frames_path):
        os.mkdir(output_class_frames_path)
    if not os.path.exists(log_folder):
        os.mkdir(log_folder)

    main(checkpoint_path, input_frames_path, output_class_frames_path, log_folder, threshold, total_frames,
         model_dim)

def main(checkpoint_path, input_frames_path, output_class_frames_path, log_folder, threshold, total_frames, model_dim):
    """Aplica una clasificacion a una carpeta de frames.

    Args:
      checkpoint_path:
        Un string con el path el checkpoints de la red.
      input_frames_path:
        Un string con el path de los frames a clsificar
      output_class_frames_path:
        Un string con el path donde se van a dejar los frames clasificados
      log_folder:
        Un string con el path donde se va a guardar el log de la clasificacion
      threshold:
        Un float de 0 a 1, donde cero toda celda es clasificada como maleza, y 1 ninguna.
       total_frames:
        Cantidad de frames a clasificar
       model_dim:
        Un int 4,8,16,32 el cual define el tamanio de la red

    Returns:
        Crea una cantidad igual total_frames de imagenes en la carpeta output_class_frames_path.

        Crea un archivo .log que indica el tiempo de clasificacion de cada frame y el total
    """

    threshold = threshold
    model_dim = model_dim
    n_classes = 3  # maleza, suelo, soja
    h = w = 64  # tamanio de la celda
    c = 3
    output_dim = h * w * c
    parches_w = 28  # cantidad de parches por ancho del ROI, 23 original, 30 nuevo
    parches_h = 5  # cantidad de parches por alto del ROI, 9 original, 5 nuevo
    batch_size = parches_w * parches_h  # cantidad de parches que entran en un ROI, es decir en una imagen
    h_e = w_e = h * 3

    # Ejemplo de ROI
    #     W 1920
    # ----------------------------------
    # |                                |H 1080
    # |          W_ROI 1472            |
    # |       -----------------        |
    # |       | ROI           |H_ROI   |
    # |       |               |576     |
    # |       |               |        |
    # |marg_l |               |marg_l  |
    # ----------------------------------

    H = 1080
    W = 1920
    H_ROI = 320
    W_ROI = 1920 - w - w
    H_ROI_e = H_ROI + h
    W_ROI_e = W_ROI + w + w

    if W_ROI >= 1920:
        marg_l = 0
    else:
        marg_l = (W - W_ROI) / 2 - 1
    if W_ROI_e >= 1920:
        marg_l_e = 0
    else:
        marg_l_e = (W - W_ROI_e) / 2 - 1

    marg_l = int(marg_l)
    marg_l_e = int(marg_l_e)
    marg_s = H - H_ROI
    marg_s_e = H - H_ROI_e

    # Create the model
    _, _, Discriminator_p, Discriminator_e = GeneratorAndDiscriminator_pe(model_arch='dcgan')
    batch_in_p = tf.placeholder(tf.uint8, shape=[batch_size, 64, 64, c])
    all_real_data_conv_p = tf.transpose(batch_in_p, perm=[0, 3, 1, 2])
    batch_in_e = tf.placeholder(tf.uint8, shape=[batch_size, c, 64, 64])
    all_real_data_conv_e = batch_in_e
    real_data_p = tf.reshape(2 * ((tf.cast(all_real_data_conv_p, tf.float32) / 255.) - .5),
                             [batch_size, output_dim])
    real_data_e = tf.reshape(2 * ((tf.cast(all_real_data_conv_e, tf.float32) / 255.) - .5),
                             [batch_size, output_dim])
    disc_real_p, flat_p = Discriminator_p(real_data_p, model_dim=model_dim)
    disc_real_e, flat_e = Discriminator_e(real_data_e, model_dim=model_dim)

    # CONCAT and CLASS
    concat = tf.concat([flat_p, flat_e], 1)
    lib.ops.linear_c.set_weights_stdev(0.02)
    generated_labels = lib.ops.linear_c.Linear('Disc_c.Class', 4 * 4 * 8 * model_dim * 2, n_classes, concat)
    lib.ops.linear_c.unset_weights_stdev()
    generated_labels_sof = tf.nn.softmax(generated_labels)

    # Restore trained model
    session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
    session.run(tf.global_variables_initializer())
    disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
    disc_saver.restore(session, save_path=checkpoint_path)

    i_image = 0
    file = open(log_folder + "log.txt", "w")
    tiempos2 = []
    tiempos1 = []
    l_iters = range(0, total_frames)
    for dirpath, _, filenames in os.walk(input_frames_path):
        for f, n in zip(filenames, l_iters):
            path = os.path.abspath(os.path.join(dirpath, f))

            if not os.path.exists(output_class_frames_path + 'm_' + f[:-3] + 'jpg'):

                im_original = Image.open(path)
                # obtiene el ROI de la imagen
                roi_original = np.asarray(im_original)[marg_s - h:-h, marg_l: marg_l + W_ROI]
                # obtiene el ROI para entornos
                roi_entornos = np.asarray(im_original)[marg_s_e - h:, marg_l_e: marg_l_e + W_ROI_e]

                bs_p = make_parches_list_p(roi_original, H_ROI, W_ROI,h,w)
                bs_e_192 = make_parches_list_e(roi_entornos, H_ROI_e, W_ROI_e,h,w)
                bs_e_192 = np.transpose(bs_e_192, (0, 3, 1, 2))
                bs_e = resize_192_to_64(bs_e_192)

                t = time.time()
                _generated_labels, parches = session.run([generated_labels_sof, all_real_data_conv_p],
                                                         feed_dict={batch_in_p: bs_p, batch_in_e: bs_e})

                t1 = time.time() - t
                im = mark_frame(_generated_labels, parches, threshold, c, marg_s, marg_l, im_original,
                                H_ROI, W_ROI, parches_h, parches_w)
                im.save(output_class_frames_path + 'm_' + f[:-3] + 'jpg')

                i_image = i_image + 1
                t2 = time.time() - t
                tiempos2.append(t2)
                tiempos1.append(t1)
                l = 'imagen numero ' + str(i_image) + ' tiempo1 ' + str(t1) + ' tiempo2 ' + str(t2)
                print(l)
                file.write(l + '\n')

    print("class ended!\n")

    if (len(tiempos1) != 0):
        file.write('promedio tiempos1, con escritura en disco: ' + str(sum(tiempos1) / len(tiempos1)) + '\n')
    if (len(tiempos2) != 0):
        file.write('promedio tiempos2, con escritura en disco: ' + str(sum(tiempos2) / len(tiempos2)))

    file.close()

apply_class_file_in()
#apply_class()
