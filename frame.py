""" Clase para el Frame, el ROI y los parches
"""

import numpy as np
from PIL import Image

class Frame(object):

    def __init__(self, parches_h,parches_w,celda_size,image=None):
        """
        Imagen HxW y su ROI hxw

        W 1920
        ----------------------------------
        | |marg_s                        |H 1080
        | |         w 1472               |
        | |     -----------------        |
        |       | ROI           |h       |
        |       |               |576     |
        |       |               |        |
        |marg_l |               |marg_l  |
        ----------------------------------

        El ROI siempre está centrado horizontalmente
        y bottom

        ejemplo:
            parches_w = 23
            parches_h = 9
            h = 576
            w = 1472

        :param parches_h:
            cantidad de parches verticales
        :param parches_w:
            cantidad de parches horizontales
        :param celda_size:
            tamaño de la celda
        :param image:
            imagen del frame de video
        """

        self.celda_size = celda_size
        self.parches_h = parches_h
        self.parches_w = parches_w

        # Tamaño del frame
        self.W = 1920 if not (image) else image.size[0]
        self.H = 1080 if not (image) else image.size[1]

        # Tamaño del roi
        self.w_roi = self.W if (parches_w * celda_size) > self.W else parches_w * celda_size
        self.h_roi = self.H if (parches_h * celda_size) > self.H else parches_h * celda_size

        self.marg_l = 0 if self.w_roi == self.W else (self.W - self.w_roi) / 2 - 1
        self.marg_s = 0 if self.h_roi == self.H else (self.H - self.h_roi)

        self.image_array = np.asarray(image) if image else None

        # acá se podrían hacer los parches
        self.parches = None
        self.labels = []

        return

    def put_parches_labeled(self, parches, labels):
        """Guarda los parches con sus etiquetas
        luego de la clasificación del roi"""
        self.parches = parches
        self.labels = labels
        return

    def get_roi(self):
        """ Retorna la matriz numpy correspondiente al ROI
        en el frame
        :return:
            numpy array que corresponde a la parte
            del roi de la imagen del frame
        """
        return self.image_array[self.marg_s:, self.marg_l: self.marg_l + self.w_roi]


    def __make_roi(self,patch_list):
        """Arma un ROI desde una lista 'ordenada' de parches.
        Además le da un marco al ROI de color azul.

        :param patch_list:
            Lista de parches 'ordenados' que conforman el ROI
        :return:
            Un ndarray representa la imagen del ROI marcado su borde con azul.
        """
        h = len(patch_list[0][1])
        b = np.zeros(shape=(self.h_roi, self.w_roi, 3), dtype=np.uint8)
        for p in range(0,self.parches_h):
            for q in range(0, self.parches_w):
                im = patch_list.pop()
                im = im.transpose(1, 2, 0)  # channel last
                b[h * p:h * (p + 1), h * q:h * (q + 1), :] = im

        borde = 5
        b[0:self.h_roi - 1, 0:borde, 0] = 0
        b[0:self.h_roi - 1, 0:borde, 1] = 0

        b[0:self.h_roi - 1, self.w_roi - borde - 1:self.w_roi - 1, 0] = 0
        b[0:self.h_roi - 1, self.w_roi - borde - 1:self.w_roi - 1, 1] = 0

        b[0:borde, 0:self.w_roi - 1, 0] = 0
        b[0:borde, 0:self.w_roi - 1, 1] = 0

        return b

    @staticmethod
    def __mark_patch_borde(patch):
        """Marca los bordes de rojo de un patch.
        Se supone que es un patch clasificado como maleza.

        Args:
          patch:
            Un ndarray representa la imagen del patch

        Returns:
            Un ndarray representa la imagen del patch con los bordes en rojo

        """
        borde = 3
        h = len(patch[1])
        patch[0, 0:h - 1, 0:borde] = 255
        patch[1, 0:h - 1, 0:borde] = 0
        patch[2, 0:h - 1, 0:borde] = 0

        patch[0, 0:h - 1, h - borde - 1:h - 1] = 255
        patch[1, 0:h - 1, h - borde - 1:h - 1] = 0
        patch[2, 0:h - 1, h - borde - 1:h - 1] = 0

        patch[0, 0:borde, 0:h - 1] = 255
        patch[1, 0:borde, 0:h - 1] = 0
        patch[2, 0:borde, 0:h - 1] = 0

        patch[0, h - borde - 1:h - 1, 0:h - 1] = 255
        patch[1, h - borde - 1:h - 1, 0:h - 1] = 0
        patch[2, h - borde - 1:h - 1, 0:h - 1] = 0
        return patch

    def get_frame_marked(self,threshold):
        """Arma un frame con los parches marcados

        :return:
            Una imagen que es frame con los parches marcados
        """

        marked_parches = []
        i_parche = 0

        for label in self.labels:
            parche = np.array(self.parches[i_parche], dtype=np.uint8)
            if label[0] >= threshold:  # si es maleza supera el tr
                parche = self.__mark_patch_borde(parche)
            marked_parches.append(parche)
            i_parche = i_parche + 1

        marked_parches.reverse()
        marked_roi = self.__make_roi(marked_parches)
        marked_frame = np.array(np.asarray(self.image_array),dtype=np.uint8)
        marked_frame[self.marg_s:,self.marg_l:self.marg_l + self.w_roi] = marked_roi
        return Image.fromarray(marked_frame)

