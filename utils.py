import os
import numpy as np
from PIL import Image, ImageDraw
import tflib as lib
import imageio
import tensorflow as tf2
tf = tf2.compat.v1
tf.disable_v2_behavior()


def listFiles(directory):
    """Retorna la lista de archivos de una carpeta
    """
    files = []
    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            path = os.path.abspath(os.path.join(dirpath, f))
            if os.path.isfile(path) and (".jpg" in f or ".png" in f):
                files.append(path)
    return (files)


def mark_patch_borde(patch):
    """Marca los bordes de rojo de un patch.
    Se supone que es un patch clasificado como maleza.

    Args:
      patch:
        Un ndarray representa la imagen del patch

    Returns:
        Un ndarray representa la imagen del patch con los bordes en rojo

    """
    borde = 3
    h = len(patch[1])
    patch[0, 0:h - 1, 0:borde] = 255
    patch[1, 0:h - 1, 0:borde] = 0
    patch[2, 0:h - 1, 0:borde] = 0

    patch[0, 0:h - 1, h - borde - 1:h - 1] = 255
    patch[1, 0:h - 1, h - borde - 1:h - 1] = 0
    patch[2, 0:h - 1, h - borde - 1:h - 1] = 0

    patch[0, 0:borde, 0:h - 1] = 255
    patch[1, 0:borde, 0:h - 1] = 0
    patch[2, 0:borde, 0:h - 1] = 0

    patch[0, h - borde - 1:h - 1, 0:h - 1] = 255
    patch[1, h - borde - 1:h - 1, 0:h - 1] = 0
    patch[2, h - borde - 1:h - 1, 0:h - 1] = 0
    return patch

def mark_patch_rojo(patch,p):
    """Pinta el canal rojo del path con la probabilidad de que sea maleza.

    Args:
      patch:
        Un ndarray representa la imagen del patch
      p:
        es la probabilidad de que sea maleza

    Returns:
        Un ndarray representa la imagen del patch, donde el canal rojo
        es la probabilidad de que sea maleza.
    """
    h = len(patch[1])
    patch[0, 0:h - 1, 0:h-1] = p * 255
    return patch


def make_roi(patch_list, h_roi, w_roi, c, parches_h, parches_w):
    """Arma un ROI desde una lista 'ordenada' de parches.
    Además le da un marco al ROI de color azul.

    Args:
      patch_list:
        Lista de parches 'ordenados' que conforman el ROI
      h_roi:
        altura del ROi
      w_roi:
        ancho del ROI
      c:
        cantidad de clases
      parches_h:
        cantidad de parches que entran a lo alto del ROI
      parches_w:
        cantidad de parches que entran a lo ancho del ROI

    Returns:
        Un ndarray representa la imagen del ROI marcado su borde con azul.
    """
    h = len(patch_list[0][1])
    b = np.zeros(shape=(h_roi, w_roi, c), dtype=np.uint8)
    for p in range(0, parches_h):
        for q in range(0, parches_w):
            im = patch_list.pop()
            im = im.transpose(1, 2, 0)  # channel last
            b[h * p:h * (p + 1), h * q:h * (q + 1), :] = im

    borde = 5
    b[0:h_roi - 1, 0:borde, 0] = 0
    b[0:h_roi - 1, 0:borde, 1] = 0

    b[0:h_roi - 1, w_roi - borde - 1:w_roi - 1, 0] = 0
    b[0:h_roi - 1, w_roi - borde - 1:w_roi - 1, 1] = 0

    b[0:borde, 0:w_roi - 1, 0] = 0
    b[0:borde, 0:w_roi - 1, 1] = 0

    return b


def make_roi_test(patch_list, h_roi, w_roi, c, parches_h, parches_w):
    """Arma un ROI desde una lista 'ordenada' de parches.
    Además le da un marco al ROI de color azul.

    Args:
      patch_list:
        Lista de parches 'ordenados' que conforman el ROI
      h_roi:
        altura del ROi
      w_roi:
        ancho del ROI
      c:
        cantidad de clases
      parches_h:
        cantidad de parches que entran a lo alto del ROI
      parches_w:
        cantidad de parches que entran a lo ancho del ROI

    Returns:
        Un ndarray representa la imagen del ROI marcado su borde con azul.
    """
    h = len(patch_list[0][1])
    b = np.zeros(shape=(h_roi, w_roi, c), dtype=np.uint8)

    for p in range(0, parches_h):
        for q in range(0, parches_w):
            im = patch_list.pop()
            im = im.transpose(1, 2, 0)  # channel last

            imim = Image.fromarray(im)
            d = ImageDraw.Draw(imim)
            d.text((10, 10), str(h*p) + ' ' + str(h*q), fill=(255, 255, 0))
            im = np.asarray(imim)



            b[h * p:h * (p + 1), h * q:h * (q + 1), :] = im

    borde = 5
    b[0:h_roi - 1, 0:borde, 0] = 0
    b[0:h_roi - 1, 0:borde, 1] = 0

    b[0:h_roi - 1, w_roi - borde - 1:w_roi - 1, 0] = 0
    b[0:h_roi - 1, w_roi - borde - 1:w_roi - 1, 1] = 0

    b[0:borde, 0:w_roi - 1, 0] = 0
    b[0:borde, 0:w_roi - 1, 1] = 0

    return b

def mark_frame(labels, parches_list, tr, c, marg_s, marg_l, im_original, h_roi, w_roi, parches_h, parches_w):
    """Arma un frame con los parches marcados

    Args:
      labels:
        lista de etiquetas correspondientes a los parches
      patch_list:
        Lista de parches 'ordenados' que conforman el ROI
      tr:
        threshold que indica cuando marcar o no el patch
      c:
        cantidad de clases
      marg_s:
        margen superior, distancia entre la altura de la imagen y la del ROI
      marg_l:
        margen lateral, mitad de la distancia entre el ancho de la imagen y del ROI
      im_original:
        imagen del frame original, esta probado en imagenes 1920x1080
      w_roi:
        ancho del roi

    OBS: tiene argumentos reduntantes
    Returns:
        Una imagen que es frame con los parches marcados
    """
    w = im_original.size[0]
    h = im_original.size[1]
    roi_l = []
    i_parche = 0

    for label in labels:
        parche = np.array(parches_list[i_parche], dtype=np.uint8)
        if label[0] >= tr:  # si es maleza supera el tr
            parche = mark_patch_borde(parche)
        roi_l.append(parche)
        i_parche = i_parche + 1

    roi_l.reverse()
    b = make_roi_test(roi_l,h_roi, w_roi, c, parches_h, parches_w)
    a = np.zeros(shape=(h, w, c), dtype=np.uint8)
    a[:, :, :] = np.asarray(im_original)
    a[marg_s: , marg_l:marg_l + w_roi] = b
    im = Image.fromarray(a)
    return im

def mark_roi(parches_list, c, im_original, h_roi, w_roi, parches_h, parches_w):
    """Arma un roi con los parches marcados

    Args:
      labels:
        lista de etiquetas correspondientes a los parches
      patch_list:
        Lista de parches 'ordenados' que conforman el ROI
      tr:
        threshold que indica cuando marcar o no el patch
      c:
        cantidad de clases
      marg_s:
        margen superior, distancia entre la altura de la imagen y la del ROI
      marg_l:
        margen lateral, mitad de la distancia entre el ancho de la imagen y del ROI
      im_original:
        imagen del frame original, esta probado en imagenes 1920x1080
      w_roi:
        ancho del roi

    OBS: tiene argumentos reduntantes
    Returns:
        Una imagen que es frame con los parches marcados
    """
    w = im_original.size[0]
    h = im_original.size[1]
    roi_l = []

    for parche in parches_list:
        parche = np.array(parche, dtype=np.uint8)
        roi_l.append(parche)

    roi_l.reverse()
    b = make_roi_test(roi_l,h_roi, w_roi, c, parches_h, parches_w)
    im = Image.fromarray(b)
    return im

def mark_frame_vecinos(labels, parches_list, tr, c, marg_s, marg_l, im_original, h_roi, w_roi, parches_h, parches_w):
    """Arma un frame con los parches marcados

    Args:
      labels:
        lista de etiquetas correspondientes a los parches
      patch_list:
        Lista de parches 'ordenados' que conforman el ROI
      tr:
        threshold que indica cuando marcar o no el patch
      c:
        cantidad de clases
      marg_s:
        margen superior, distancia entre la altura de la imagen y la del ROI
      marg_l:
        margen lateral, mitad de la distancia entre el ancho de la imagen y del ROI
      im_original:
        imagen del frame original, esta probado en imagenes 1920x1080
      w_roi:
        ancho del roi

    OBS: tiene argumentos reduntantes
    Returns:
        Una imagen que es frame con los parches marcados
    """
    w = im_original.size[0]
    h = im_original.size[1]
    roi_l = []
    i_parche = 0

    k = 3  # vecinos. si es 3, entonces 3x3
    n_bloques = int(parches_w / k)
    blocks = [0] * n_bloques
    atenua = 2/k

    labels_aux = [0.0]*len(labels)

    for fila in range(0,parches_h):
        for b in range(0,n_bloques):
            for i in range(0, k):
                ix = i + b*k + fila*n_bloques*k
                blocks[b] = blocks[b] + labels[ix][0]*atenua


    for fila in range(0,parches_h):
        for b in range(0,n_bloques):
            for i in range(0, k):
                ix = i + b*k + fila*n_bloques*k
                labels_aux[ix] = blocks[b]

    for label in labels_aux:
        parche = np.array(parches_list[i_parche], dtype=np.uint8)
        if label >= tr:  # si es maleza supera el tr
            parche = mark_patch_borde(parche)
        roi_l.append(parche)
        i_parche = i_parche + 1

    roi_l.reverse()
    b = make_roi(roi_l,h_roi, w_roi, c, parches_h, parches_w)
    a = np.zeros(shape=(h, w, c), dtype=np.uint8)
    a[:, :, :] = np.asarray(im_original)
    a[marg_s: , marg_l:marg_l + w_roi] = b
    im = Image.fromarray(a)
    return im



def make_parches_list_p(roi, h_roi,w_roi,h,w):
    """Arma una lista de parches, un batch, a partir de un roi

    Args:
      roi:
        numpy array que corres
      h_roi:
        altura del roi
      w_roi:
        ancho del roi
      h:
        altura de la celda
      w:
        anco de la celda

    Returns:
        Un numpy array que es un batch de parches
    """
    l = []
    for i in range(0, h_roi, h):
        for j in range(0, w_roi, w):
            l.append(roi[i:i + h, j:j + w, :])

    ims = np.array(l, dtype=np.uint8)
    return ims


def make_parches_list_e(roi, h_roi, w_roi,h,w):
    """Arma una lista de parches, un batch, a partir de un roi.
    Pero considerando que los parches son entornos.

    Args:
      roi:
        numpy array que corres
      h_roi:
        altura del roi
      w_roi:
        ancho del roi
      h:
        altura de la celda
      w:
        anco de la celda

    Returns:
        Un numpy array que es un batch de entornos
    """
    l = []
    for i in range(0, h_roi - h, h):
        for j in range(0, w_roi - w * 2, w):
            l.append(roi[i:i + h * 3, j:j + w * 3, :])

    ims = np.array(l, dtype=np.uint8)
    return ims


def get_coord(str):
    """Dada una linea del archivo idx obtiene las corrdenadas.

    Args:
      str:
       linea del archivo idx
       Cada linea tiene la siguiente pinta.

       png_(12935, 224, 224)_joa_no_bordes/imageRGB-006-391_label=|1|soja_coords=663_907.png
                                                     ^   ^
                                                    coordenadas
    Returns:
      x,y:
       int de coordenadas. x crece para la derecha, y para abajo
    """
    str = str[:-1]  # le saco el enter
    y = 0
    x = 0
    i = 0
    # ULTIMO NUMERO
    if not (str[-7].isdigit()):  # dos cifras
        y = int(str[-6:-4])
        i = -1
    else:
        if not (str[-8].isdigit()):  # tres cifras, si es _ entonces es de tres, si no de cuatro
            y = int(str[-7:-4])
        else:
            y = int(str[-8:-4])
            i = 1

    # PENULTIMO NUMERO
    if not (str[-11 - i].isdigit()):  # dos cifras
        x = int(str[-10 - i:-8 - i])
    else:
        if not (str[-12 - i].isdigit()):
            x = int(str[-11 - i:-8 - i])  # tres cifras
        else:
            x = int(str[-12 - i:-8 - i])  # cuatro cifras
    return x, y


def get_coords(l):
    """Dada las lineas del archivo idx obtiene las corrdenadas
    para cada línea.

    Args:
      l:
       líneas del archivo idx
       Cada linea tiene la siguiente pinta.

       png_(12935, 224, 224)_joa_no_bordes/imageRGB-006-391_label=|1|soja_coords=663_907.png
                                                     ^   ^
                                                    coordenadas
    Returns:
        cords: coordenadas extraidas de cada línea
    """
    cords = []
    for i in l:
        x, y = get_coord(i)
        cords.append((x, y))
    return cords


def coord_pos(coords, mi):
    """Calcula si las coordenadas son del centro o los bordes.

    Args:
      coords:
       lista de coordenadas
      mi:
       margen izquierdo

    Returns:
      pos:
       un string 'border' o 'center'
      b:
       cantidad de parches border
      c:
       cantidad de parches center
    """
    c = 0
    b = 0
    md = 1920 - mi
    pos = []
    for x, y in coords:
        if x <= mi or x >= md:
            pos.append('border')
            b = b +1
        else:
            pos.append('center')
            c = c + 1
    return pos, b, c

def generate_image(session, iteration,Generator,bs,model_dim,samples_path):
    """Imprime en disco un batch de parches generados por un generador
        GAN

        Args:
          session:
           tf.Session
          iteration:
           número de iteracion, para el nombre del archivo
          Generator:
            generador de gan.py genera imágenes aleatorias
          samples_path:
            carpeta donde se imprimen

        Returns:
          Imprime un batch de imágenes de tamaño bs en sample_path
        """

    # For generating samples
    fixed_noise = tf.constant(np.random.normal(size=(bs, 128)).astype('float32'))
    all_fixed_noise_samples = []
    n_samples = bs
    all_fixed_noise_samples.append(
        Generator(n_samples, model_dim, noise=fixed_noise[0:n_samples]))

    all_fixed_noise_samples = tf.concat(all_fixed_noise_samples, axis=0)


    samples = session.run(all_fixed_noise_samples)
    samples = ((samples + 1.) * (255.99 / 2)).astype('int32')
    save_images(samples.reshape((bs, 3, 64, 64)),
                                os.path.join(samples_path, 'samples_{}.jpg'.format(iteration)))


def generate_image_entornos(session, iteration,bs,Generator_p,Generator_e,model_dim,samples_path):
    """Imprime en disco un batch de parches generados por dos generadores
       uno de parches y otro de entornos

    Args:
      session:
       tf.Session
      iteration:
       número de iteración, para el nombre del archivo
      Generator_p, Generator_e:
        generador de gan.py genera imágenes aleatorias
      samples_path:
        carpeta donde se imprimen

    Returns:
      Imprime dos batch de imágenes de tamaño bs en sample_path
      uno correspondiente a parches y otro a entornos.
    """
    fixed_noise = tf.constant(np.random.normal(size=(bs, 128)).astype('float32'))
    all_fixed_noise_samples_p = []
    all_fixed_noise_samples_e = []
    n_samples = bs
    all_fixed_noise_samples_p.append(Generator_p(n_samples, model_dim, noise=fixed_noise[0:n_samples]))
    all_fixed_noise_samples_e.append(Generator_e(n_samples, model_dim, noise=fixed_noise[0:n_samples]))


    all_fixed_noise_samples_p = tf.concat(all_fixed_noise_samples_p, axis=0)
    all_fixed_noise_samples_e = tf.concat(all_fixed_noise_samples_e, axis=0)

    samples_p = session.run(all_fixed_noise_samples_p)
    samples_e = session.run(all_fixed_noise_samples_e)
    samples_p = ((samples_p + 1.) * (255.99 / 2)).astype('int32')
    samples_e = ((samples_e + 1.) * (255.99 / 2)).astype('int32')
    save_images(samples_p.reshape((bs, 3, 64, 64)),
                                os.path.join(samples_path, 'samples_p_{}.jpg'.format(iteration)))
    save_images(samples_e.reshape((bs, 3, 64, 64)),
                                os.path.join(samples_path, 'samples_e_{}.jpg'.format(iteration)))



def save_images(X, save_path):
    """
    Image grid saver, based on color_grid_vis from github.com/Newmu
    """
    # [0, 1] -> [0,255]
    if isinstance(X.flatten()[0], np.floating):
        X = (255.99 * X).astype('uint8')

    n_samples = X.shape[0]
    rows = int(np.sqrt(n_samples))
    while n_samples % rows != 0:
        rows -= 1

    nh, nw = rows, int(n_samples / rows)

    if X.ndim == 2:
        X = np.reshape(X, (X.shape[0], int(np.sqrt(X.shape[1])), int(np.sqrt(X.shape[1]))))

    if X.ndim == 4:
        # BCHW -> BHWC
        X = X.transpose(0, 2, 3, 1)
        h, w = X[0].shape[:2]
        a = int(h) * int(nh)
        b = int(w) * int(nw)
        img = np.zeros((a,b, 3))
    elif X.ndim == 3:
        h, w = X[0].shape[:2]
        a = int(h) * int(nh)
        b = int(w) * int(nw)
        img = np.zeros((a, b))

    for n, x in enumerate(X):
        j = int(n / nw)
        i = int(n % nw)

        a = int(j * h)
        b = int(j * h) + int(h)
        c = int(i * w)
        d = int(i * w) + int(w)
        img[a:b, c:d] = x

    imageio.imwrite(save_path, img)


