reset
set style data lines
set xlabel "iter"


set title "Bests.Accs"
set ylabel "accs"
#set logscale y
plot "best_acc.log" u 1:4 t "train_acc", "" u 1:5 t "valid_acc", "" u 1:6 t "test_acc"