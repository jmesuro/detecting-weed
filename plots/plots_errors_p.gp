reset
set style data lines
set xlabel "iter"


set title "Errors_p"
set ylabel "loss"
#set logscale y
plot "errors_p.log" u 1:4 t "gen_loss", "" u 1:5 t "disc_loss", "" u 1:6 t "cl_loss"