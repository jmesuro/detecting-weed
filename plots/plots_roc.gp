reset
set style data lines
set xlabel "iter"


set title "fpr"
set ylabel "tpr"
#set logscale y
plot "roc.log" u 8:7 t "tpr"