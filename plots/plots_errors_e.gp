reset
set style data lines
set xlabel "iter"


set title "Errors_e"
set ylabel "loss"
#set logscale y
plot "errors_e.log" u 1:4 t "gen_loss", "" u 1:5 t "disc_loss", "" u 1:6 t "cl_loss"