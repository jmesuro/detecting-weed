reset
set style data lines



set title "precision recall curve"
set ylabel "precision"
set xlabel "recall"

#set logscale y
plot "pr.log" u 8:7 t "r"