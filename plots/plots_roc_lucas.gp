reset
set style data lines
set xlabel "iter"


set title "p/total"
set ylabel "tpr"
#set logscale y
plot "roc_lucas.log" u 8:7 t "tpr"