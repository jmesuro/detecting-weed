reset
set style data lines
set xlabel "iter"


set title "Accs"
set ylabel "accs"
#set logscale y
plot "accuracies.log" u 1:4 t "train_acc", "" u 1:5 t "valid_Acc", "" u 1:6 t "test_Acc"