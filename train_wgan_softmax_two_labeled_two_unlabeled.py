"""Entrena un clasificador softmax
basado en WGAN con arquitectura DCGAN

Utiliza 6 datasets en disco:
train labels_train,
test  labels_test
unlabeled_fot, unlabeled_robot

Asume que los datasets corresponden a imágenes
    - train, test: NHWC 96x96x3 uint8
    - unlabeled_fot, unlabeled_robot: NCHW 3x224x224 uint8
    - labels_train, labels_test: NC float64

Entrena un clasificador el cual esta pensado para la deteccion de maleza
en videos de 1920x1080, dividiendo cada frame en celdas (parches) de 64x64
y clasificando cada parche en (soja, maleza, suelo).

Returns:
    checkpoints
    best_checkpoints
    logs: acc, bests_acc, errors
    samples
"""

import os
import sys
import time
import tflib.soja_dataset_labeled
import tflib.soja_dataset_unlabeled
from tflib.utils import crop_96_to_64, crop_224_to_64
import numpy as np
import tensorflow as tf2
tf = tf2.compat.v1
tf.disable_v2_behavior()
import tflib as lib
from utils import save_images
from gan import GeneratorAndDiscriminator
import argparse
import datetime
from tflib.augmentation import Augmentation
from utils import generate_image
# ------------------------------------
# WGAN + softmax classifier
# ------------------------------------
sys.path.append(os.getcwd())

parser = argparse.ArgumentParser()
parser.add_argument("--labels_test", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ROB_96_BALANCEADO_IDX/'
                            'labels_(1230,3)_3_clases_float64_BALANCEADO_TEST.npy')

parser.add_argument("--parches_test", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/ROB_96_BALANCEADO_IDX/'
                            'parches_labeled_(1230,96,96,3)_uint8_BALANCEADO_TEST.npy')

parser.add_argument("--parches_train", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/FOT_96_BALANCEADO_IDX/'
                            'parches_labeled_(10986,96,96,3)_uint8_BALANCEADO_TRAIN.npy')
parser.add_argument("--labels_train", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/FOT_96_BALANCEADO_IDX/'
                            'labels_(10986,3)_3_clases_float64_BALANCEADO_TRAIN.npy')

# unlabeled dataset

parser.add_argument("--parches_u_fot", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/soja'
                            '/parches_fotografos_u_1200878_3_224_224_utin8.h5')

# unlabeled dataset
parser.add_argument("--parches_u_robot", type=str,
                    default='/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/datasets/soja'
                            '/parches_robot_u_376200_3_224_244_uint8.h5')

parser.add_argument("--model_arch", type=str, default='dcgan')  # model arch: 'good', 'dcgan'
parser.add_argument("--model_dim", type=int, default=4)  # model size
parser.add_argument("--batch_size", type=int, default=16)
parser.add_argument("--n_iters", type=int, default=1)
parser.add_argument("--n_classes", type=int, default=3)
parser.add_argument("--start_iter", type=int, default=0)  # for continue training from chechpoint
parser.add_argument("--lambda_gp", type=int, default=10)  # wgan param
parser.add_argument("--output_dim", type=int, default=64 * 64 * 3)
parser.add_argument("--checkpoint_start_iter", type=int, default=0)  # iter from where to start to check
parser.add_argument("--checkpoint_iter", type=int, default=1)  # freq to validate
parser.add_argument("--checkpoint_gen_im", type=int, default=1)  # freq to gen im
parser.add_argument("--checkpoint_path", type=str, default=None)  # path to save the check for continue training
parser.add_argument("--rand_seed", type=int, default=2017)
parser.add_argument("--wd", type=float, default=0.01)
parser.add_argument("--back_class", type=str, default='no')  # indicate the deep of backprop signal in class
parser.add_argument("--labeled_da", type=str, default='no')  # data aumentation  labeled dataset
parser.add_argument("--unlabeled_da", type=str, default='no')  # data aumentation  unlabeled dataset
args = parser.parse_args()

# ------------------------
# Prepare labeled datasets
# ------------------------
(train_gen, valid_gen, test_gen), (n_train, n_valid, n_test) = \
    lib.soja_dataset_labeled.load_train_valid_test(args.parches_train,
                                                   args.parches_test,
                                                   args.labels_train,
                                                   args.labels_test,
                                                   args.batch_size,
                                                   cel_size=96)

valid_batches = n_valid / args.batch_size
test_batches = n_test / args.batch_size


def inf_train_gen():
    while True:
        for images, targets in train_gen():
            yield images, targets


def inf_valid_gen():
    while True:
        for images, targets in valid_gen():
            yield images, targets


def inf_test_gen():
    while True:
        for images, targets in test_gen():
            yield images, targets


gen_train = inf_train_gen()
gen_valid = inf_valid_gen()
gen_test = inf_test_gen()

# --------------------------
# Prepare unlabeled datasets
# --------------------------
train_unlab_gen_p = lib.soja_dataset_unlabeled.load_all(args.parches_u_fot, args.parches_u_robot, args.batch_size)


def inf_unlab_train_gen():
    while True:
        for images in train_unlab_gen_p():
            yield images


gen_unlab_train = inf_unlab_train_gen()

# -----------------------
# Build folders and files
# -----------------------
OUT_PATH = os.getcwd() + '/out/'
if not os.path.exists(OUT_PATH):
    os.mkdir(OUT_PATH)

date = (time.strftime("%Y_%m_%d")) + '_' + str(datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M:%S'))
THIS_RUN_PATH = OUT_PATH + date + '_wgan_softmax_' + "model_dim={}_n_clases={}_n_iters={}_bs={}_wd={}". \
    format(
    args.model_dim,
    args.n_classes,
    args.n_iters,
    args.batch_size,
    args.wd)

CHECKPOINTS_FOLDER_PATH = THIS_RUN_PATH + '/checkpoints/'
BEST_CHECKPOINTS_FOLDER_PATH = THIS_RUN_PATH + '/best_checkpoints/'
LOG_FOLDER_PATH = THIS_RUN_PATH + '/logs/'
SAMPLES_PATH = THIS_RUN_PATH + '/samples/'

os.mkdir(THIS_RUN_PATH)
if not os.path.exists(BEST_CHECKPOINTS_FOLDER_PATH):
    os.mkdir(BEST_CHECKPOINTS_FOLDER_PATH)
if not os.path.exists(CHECKPOINTS_FOLDER_PATH):
    os.mkdir(CHECKPOINTS_FOLDER_PATH)
if not os.path.exists(LOG_FOLDER_PATH):
    os.mkdir(LOG_FOLDER_PATH)
if not os.path.exists(SAMPLES_PATH):
    os.mkdir(SAMPLES_PATH)

with open(LOG_FOLDER_PATH + '/errors.log', 'w') as f:
    f.write('# iter data_seen epoch gen_loss disc_loss cl_loss')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/accuracies.log', 'w') as f:
    f.write('# iter data_seen epoch train_acc valid_acc test_acc')
    f.write('\n')
with open(LOG_FOLDER_PATH + '/best_acc.log', 'w') as f:
    f.write('# iter data_seen epoch train_acc valid_acc test_acc')
    f.write('\n')

print("Arguments:")
for arg in vars(args):
    print("\t{}: {}".format(arg, getattr(args, arg)))
print("")


# -------------------------
# Data Aumentation
# -------------------------
rotation_range = 20
width_shift_range = 0.028
height_shift_range = 0.016
shear_range = 0.14
zoom_range_center = 0.97
zoom_range_range = 0.18
zoom_range = (zoom_range_center - zoom_range_range * 0.5, zoom_range_center + zoom_range_range * 0.5)
random_curves_strength = 0.58
seed = 17
augm = Augmentation(rotation_range=rotation_range,  # In degrees
                    width_shift_range=width_shift_range,
                    height_shift_range=height_shift_range,
                    horizontal_flip=True,
                    shear_range=shear_range,  # In radians
                    zoom_range=zoom_range,  # >1 zoom out; <1 zoom in
                    channel_shift_range=0.0,  # 0-255
                    fill_mode='constant',  # 'nearest',
                    random_curves_strength=random_curves_strength,
                    seed=seed)

# ----------------
# Create the model
# ----------------
Generator, Discriminator = GeneratorAndDiscriminator(model_arch='dcgan')
all_real_data_conv = tf.placeholder(tf.int32, shape=[args.batch_size, 3, 64, 64])
real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [args.batch_size, args.output_dim])
real_labels = tf.placeholder(tf.float32, [args.batch_size, args.n_classes])
fake_data = Generator(args.batch_size, model_dim=args.model_dim)
disc_real, generated_labels = Discriminator(real_data, n_classes=args.n_classes, model_dim=args.model_dim)
disc_fake, _ = Discriminator(fake_data, n_classes=args.n_classes, model_dim=args.model_dim)
correct_prediction = tf.equal(tf.argmax(generated_labels, 1), tf.argmax(real_labels, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# LOSS
cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(labels=real_labels, logits=generated_labels)
cross_entropy = tf.reduce_mean(cross_entropy)

w = lib.params_with_name('Disc.Class.W')[0]
reg = tf.nn.l2_loss(w)
cls_loss = cross_entropy + reg * args.wd

gen_cost = -tf.reduce_mean(disc_fake)
disc_cost = tf.reduce_mean(disc_fake) - tf.reduce_mean(disc_real)

alpha = tf.random_uniform(shape=[args.batch_size, 1], minval=0., maxval=1.)
differences = fake_data - real_data
interpolates = real_data + (alpha * differences)
dis_interpolates, _ = Discriminator(interpolates, model_dim=args.model_dim)
gradients = tf.gradients(dis_interpolates, [interpolates])[0]
slopes = tf.sqrt(tf.reduce_sum(tf.square(gradients), reduction_indices=[1]))
gradient_penalty = tf.reduce_mean((slopes - 1.) ** 2)
disc_cost += args.lambda_gp * gradient_penalty

# Trainers
if args.back_class == 'yes':  # Entreno todas las capas o solo la ultima
    class_train_op = tf.train.AdamOptimizer(1e-3).minimize(cls_loss, var_list=lib.params_with_name('Disc'))
else:
    class_train_op = tf.train.AdamOptimizer(1e-3).minimize(cls_loss, var_list=lib.params_with_name('Disc.Class'))

gen_train_op = tf.train.AdamOptimizer(learning_rate=1e-4, beta1=0., beta2=0.9).minimize(gen_cost,
                                                                                        var_list=lib.params_with_name(
                                                                                            'Generator'),
                                                                                        colocate_gradients_with_ops=True)
disc_train_op = tf.train.AdamOptimizer(learning_rate=1e-4, beta1=0., beta2=0.9).minimize(disc_cost,
                                                                                         var_list=lib.params_with_name(
                                                                                             'Discriminator.'),
                                                                                         colocate_gradients_with_ops=True)

# --------------
# Init session
# --------------
session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
session.run(tf.global_variables_initializer())

# --------------
# Savers
# --------------
saver = tf.train.Saver(save_relative_paths=True)  # all session: gen + disc + class
best_d_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Discriminator.'))  # only disc
best_c_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))            # disc + c

# ----------
# Train loop
# ----------
if args.checkpoint_path is None:
    START_ITER = 0
else:
    START_ITER = args.start_iter
    saver.restore(session, save_path=args.checkpoint_path)
    print("\nStarting in iter={}:".format(START_ITER))

best_acc = 0.
it_best = 0
last_it = 0
t = time.time()
for iteration in range(START_ITER, args.n_iters):
    epoch = iteration * args.batch_size / n_train
    start_time = time.time()

    # Train generator
    _gen_cost, _ = session.run([gen_cost, gen_train_op])

    # Train Discriminador
    batch_xs_unlab = next(gen_unlab_train)
    if args.unlabeled_da == 'yes':
        batch_xs_unlab = augm.random_transform(batch_xs_unlab)
    batch_xs_unlab = crop_224_to_64(batch_xs_unlab)
    _disc_cost, _ = session.run([disc_cost, disc_train_op], feed_dict={all_real_data_conv: batch_xs_unlab})

    # Train Classifier
    batch_xs, batch_ys = next(gen_train)
    if args.labeled_da == 'yes':
        batch_xs = augm.random_transform(batch_xs)
    batch_xs = crop_96_to_64(batch_xs)
    _cls_cost, _, _train_acc = session.run([cls_loss, class_train_op, accuracy],
                                           feed_dict={all_real_data_conv: batch_xs, real_labels: batch_ys})

    # ----------------------------------------------------
    # Log, Sample and Check
    # ----------------------------------------------------
    if iteration >= args.checkpoint_start_iter and iteration % args.checkpoint_gen_im == args.checkpoint_gen_im - 1:
        generate_image(session, iteration + 1,Generator,args.batch_size,args.model_dim,SAMPLES_PATH)

    if iteration >= args.checkpoint_start_iter and iteration % args.checkpoint_iter == args.checkpoint_iter - 1:

        # Valid accuracy
        valid_acc = 0.
        for test_it in range(int(valid_batches)):
            batch_xs_valid, batch_ys_valid = next(gen_valid)
            valid_acc += session.run(accuracy, feed_dict={all_real_data_conv: batch_xs_valid,
                                                          real_labels: batch_ys_valid})
        valid_acc = valid_acc / valid_batches

        # Test accuracy
        test_acc = 0.
        for test_it in range(int(test_batches)):
            batch_xs_test, batch_ys_test = next(gen_test)
            test_acc += session.run(accuracy, feed_dict={all_real_data_conv: batch_xs_test,
                                                         real_labels: batch_ys_test})
        test_acc = test_acc / test_batches

        # Log and save best model
        if best_acc < valid_acc:
            best_acc = valid_acc
            it_best = iteration

            # Save best model
            # best_d_saver.save(sess=session, save_path=os.path.join(BEST_CHECKPOINTS_FOLDER_PATH, "best_iter_d"),
            #                     global_step=iteration + 1)
            best_c_saver.save(sess=session, save_path=os.path.join(BEST_CHECKPOINTS_FOLDER_PATH, "best_iter_c"),
                                  global_step=iteration + 1)

            # Log best model
            with open(LOG_FOLDER_PATH + '/best_acc.log', 'a') as f:
                np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size,
                                epoch, _train_acc, valid_acc, test_acc]], fmt='%1.3e')

        with open(LOG_FOLDER_PATH + '/errors.log', 'a') as f:
            np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size, epoch, _gen_cost, _disc_cost, _cls_cost]],
                       fmt='%1.3e')

        with open(LOG_FOLDER_PATH + '/accuracies.log', 'a') as f:
            np.savetxt(f, [[iteration + 1, (iteration + 1) * args.batch_size, epoch, _train_acc, valid_acc,
                            test_acc]], fmt='%1.3e')

        t2 = time.time() - t
        t += t2
        horas = t2 / (1 + iteration - last_it) / 3600. * 10000
        print("iter:%d/%d; epoch:%d; %4.2f horas. para 10000 iteraciones" % (iteration + 1, args.n_iters, epoch, horas))
        last_it = iteration + 1
        # saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter"), global_step=iteration + 1)

# Check last model
# saver.save(sess=session, save_path=os.path.join(CHECKPOINTS_FOLDER_PATH, "iter"), global_step=args.n_iters + 1)

print("Train gan ended!\n")
