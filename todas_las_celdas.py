"""
Toma una imagen y devuelve todas las celdas pixel a pixel

"""


from PIL import Image, ImageDraw
import numpy as np
import os
from scipy import misc
a = Image.open('/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf[0]/image-02569.png')
a_1 = Image.open('/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf[0]/image-02568.png')
aa = np.asarray(a)
aa_1 = np.asarray(a_1)

# width = a.width
# height = a.height

path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf2/'


#(Image.fromarray(aa[i:i + c, j:j + c, :])).save(path + 'celda_' + str(i) + '_' + str(j) + '.png')

def list_img_files(directory):
    """ Lista las imagenes de un directorio

    Args
      directory:
        carpeta con las imagenes

    Returns:
      lista con los path de los archivos
    """
    files = []
    for dirpath, _, filenames in os.walk(directory):
        filenames.sort()
        for f in filenames:
            path = os.path.join(dirpath, f)
            if os.path.isfile(path) and (".jpg" in f or ".png" in f):
                files.append(path)
    return files


def mk_arrays_from_imgs(parches_png_path):
    """ Hace las matrices numpy de parches y labels
    obtiene los labels del nombre de los parches png

    Args:
      parches_png_path:
        carpeta donde estan los png

    Returns:
      tres matrices numpy con los dataset: parches, label, idx
    """
    paths = list_img_files(parches_png_path)
    X = np.array([ np.asarray(Image.open(path)) for path in paths])
    idx = [line[60:] for line in paths]
    return X,idx


pos = (1080-128,896)

#pabajo
w_min = pos[1] - 32
w_max = pos[1] + 64 - 32
h_min = pos[0]
h_max = pos[0] + 64

#parriva
h_max_1 = pos[0]
h_min_1 = pos[0] - 64

# pos_izq = (marg_s, marg_l)
# pos_der = (marg_s, marg_l + W_ROI - 64)
#
# w_min_izq = pos_izq[1] - 32
# w_max_izq = pos_izq[1] + 64 - 32
# h_max_izq = pos_izq[0]
# h_min_izq = pos_izq[0] - 64
#
# w_min_der = pos_der[1] - 32
# w_max_der = pos_der[1] + 64 - 32
# h_max_der = pos_der[0]
# h_min_der = pos_der[0] - 64


Image.fromarray(aa[pos[0]:pos[0]+64,pos[1]:pos[1]+64,:]).save(path + 'celda_0_0.jpg')

# ejes al reves
draw = ImageDraw.Draw(a)
draw.rectangle((  (pos[1],pos[0]), (pos[1]+64, pos[0]+64) ),outline=(255,0,0), width=2)
a.save('/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf[0]/m_image-02569.png')

for i in range(w_min,w_max,5):
    for j in range(h_min_1,h_max_1,5):
        print(j,i)
        (Image.fromarray( aa_1[j:j+64,i:i+64,:])).save(path+'celda_' + str(j)+'_'+ str(i)+'.png')


print(len(range(w_min,w_max,5)) * len(range(h_min_1,h_max_1,5))  +1 )

batch,idx = mk_arrays_from_imgs('/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf2/')
np.save('/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf2/batch.npy', batch)
np.savetxt('/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/nf2/batch.idx', idx, fmt='%s')  # indice
# img = Image.new('RGB', (100, 30), color=(73, 109, 137))
#
# d = ImageDraw.Draw(a)
# d.text((10, 10), "Hello World", fill=(255, 255, 0))
# d.show()

