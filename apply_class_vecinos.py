"""Aplica un clasificador de parches.

La idea es hacer un clasificador con un disperson para el surco y otro
para la tierra. entonces no tiene sentido hacer clasificación a nivel
de parche, es para probar la wea.

Aplica la clasificacion a una lista de imagenes, que se suponen que son
frames consecutivos de un video sobre un campo de soja. Sobre cada frame
se define un ROI (region of interest) el cual se divide en celdas (parches)
iguales y se realiza la clasificacion de cada una en (soja, maleza, suelo).

Esta probado sobre frames son 1080 x 1920 y celdas de 64 x 64
"""

import os
import numpy as np
import tensorflow as tf2
tf = tf2.compat.v1
tf.disable_v2_behavior()
import tflib as lib
from gan import GeneratorAndDiscriminator
import time
from PIL import Image
from utils import listFiles, mark_frame


def apply_class_file_in(input='/home/jmesuro/detecting-weed/test_16.in'):
    """Aplica clasificaciones definidas por los parametros en el archivo input.
    Esta pensado para aplicar varias clasificaciones sobre varias carpetas de
    frames de video.

    Args:
        input: es un archivo de txt con el siguiente formato:

        ... empty line
        [checkpint_path]
        [input_frame_path]
        [output_class_frame_path]
        [threshold]
        ... empty line
        [checkpint_path]
        [input_frame_path]
        [output_class_frame_path]
        [threshold]
        [model_dim]

        ej:

        /home/jmesuro/checkpoints/best_iter_class-66600
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/New Folder
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_[enrRRR_88]/
        0.88
        16

        /home/jmesuro/checkpoints/best_iter_class-66600
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/New Folder
        /media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_[enrRRR_50]/
        0.50
        16
    """

    f = open(input)
    lines = f.read().split('\n')
    for l in range(0, len(lines) - 1, 6):
        checkpoint_path = lines[l + 1]
        input_frames_path = lines[l + 2]
        output_class_frames_path = lines[l + 3]
        threshold = float(lines[l + 4])
        model_dim = int(lines[l + 5])

        # obtiene la cantidad de frames de la carpeta input_frames_path
        total_frames = len(listFiles(input_frames_path))

        log_folder = output_class_frames_path

        if not os.path.exists(output_class_frames_path):
            os.mkdir(output_class_frames_path)
        if not os.path.exists(log_folder):
            os.mkdir(log_folder)

        main(checkpoint_path, input_frames_path, output_class_frames_path, log_folder, threshold, total_frames,
             model_dim)


def apply_class():
    """Aplica la clasificacion definida por los parametros:

            checkpoint_path
            input_frames_path
            output_class_frames_path
            model_dim
            threshold

        Esta pensado para aplicar una sola clasificaciones sobre una sola carpeta de
        frames de video.
    """
    checkpoint_path = '/home/jmesuro/checkpoints/best_iter_c-93900'  # path del clasificador entrenado, ojo tamnio gan
    input_frames_path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left/'  # path images a clasificar
    output_class_frames_path = '/media/jmesuro/b7608595-32da-4f34-b12f-72f355fe3524/videos_soja/erica/hd_left_                      [PALLR_50]/'  # path imagenes clasificadaas
    log_folder = output_class_frames_path
    total_frames = len(listFiles(input_frames_path))
    model_dim = 32
    threshold = 0.50

    if not os.path.exists(output_class_frames_path):
        os.mkdir(output_class_frames_path)
    if not os.path.exists(log_folder):
        os.mkdir(log_folder)

    main(checkpoint_path, input_frames_path, output_class_frames_path, log_folder, threshold, total_frames,
         model_dim)

def main(checkpoint_path, input_frames_path, output_class_frames_path, log_folder, threshold, total_frames, model_dim):


    model_arch = 'dcgan'
    threshold = threshold   # 0.25
    model_dim = model_dim  # 32
    n_classes = 3
    h = w = 64
    c = 3
    output_dim = h * w * c

    parches_w = 30  # cantidad de parches por ancho del ROI, 23 original, 30 nuevo
    parches_h = 3  # cantidad de parches por alto del ROI, 9 original, 5 nuevo
    batch_size = parches_w * parches_h  # cantidad de parches que entran en un ROI, es decir en una imagen

    #     W 1920
    # ----------------------------------
    # |                                |H 1080
    # |          W_ROI 1472            |
    # |       -----------------        |
    # |       | ROI           |H_ROI   |
    # |       |               |576     |
    # |       |               |        |
    # |marg_l |               |marg_l  |
    # ----------------------------------
    H = 1080
    W = 1920
    H_ROI = 192  # 576 original, 320 nuevo
    W_ROI = 1920  # 1472 original, 1920 nuevo
    if W_ROI >= 1920:
        marg_l = 0
    else:
        marg_l = (W - W_ROI) / 2 - 1
    marg_s = H - H_ROI

    # ---------------------------------------------------
    # Armo el clasificador, toma de una imagen a la vez
    # ---------------------------------------------------
    images_crudas_t = tf.placeholder(tf.uint8, shape=[c, H_ROI, W_ROI])  # imagen entera cHN
    a1 = tf.reshape(images_crudas_t, shape=[c, int(H_ROI / h), h, int(W_ROI / w), w])
    a2 = tf.transpose(a1, perm=[1, 3, 0, 2, 4])

    all_real_data_conv = tf.reshape(a2, shape=[int(H_ROI / h * W_ROI / w), c, h, w])
    real_data = tf.reshape(2 * ((tf.cast(all_real_data_conv, tf.float32) / 255.) - .5), [batch_size, output_dim])
    _, Discriminator = GeneratorAndDiscriminator(model_arch)
    disc_real, generated_labels = Discriminator(real_data, model_dim, n_classes=n_classes)

    generated_labels_sof = tf.nn.softmax(generated_labels)

    # ----------------------
    # Restore trained model
    # ----------------------
    session = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
    session.run(tf.global_variables_initializer())
    disc_saver = tf.train.Saver(save_relative_paths=True, var_list=lib.params_with_name('Disc'))
    disc_saver.restore(session, save_path=checkpoint_path)

    i_image = 0
    file = open(log_folder + "log.txt","w")
    tiempos2 = []
    tiempos1 = []


    l_iters = range(0,total_frames)
    for dirpath, _, filenames in os.walk(input_frames_path):
        for f,n in zip(filenames,l_iters):
            path = os.path.abspath(os.path.join(dirpath, f))

            if not os.path.exists(output_class_frames_path + 'm_' + f[:-3] + 'jpg'):

                im_original = Image.open(path)
                # obtengo el ROI de la imagen
                roi_original = np.asarray(im_original)[H - H_ROI:, marg_l: marg_l + W_ROI]
                batch_xs = roi_original.transpose(2, 0, 1)  # channel first

                t = time.time()
                _generated_labels, parches = session.run([generated_labels_sof, all_real_data_conv],
                                                         feed_dict={images_crudas_t: batch_xs})
                t1 = time.time() - t

                im = mark_frame(_generated_labels, parches, threshold, c, marg_s, marg_l, im_original,
                                H_ROI, W_ROI, parches_h, parches_w)  # marco la imagen
                im.save(output_class_frames_path + 'm_' + f[:-3] + 'jpg')

                i_image = i_image + 1
                t2 = time.time() - t
                tiempos2.append(t2)
                tiempos1.append(t1)
                l = 'imagen numero ' + str(i_image) + ' tiempo1 ' + str(t1) + ' tiempo2 ' + str(t2)

                print(l)
                file.write(l + '\n')


    print("class ended!\n")
    if (len(tiempos1) != 0):
        file.write('promedio tiempos1, con escritura en disco: ' + str(sum(tiempos1) / len(tiempos1)) + '\n')
    if (len(tiempos2) != 0):
        file.write('promedio tiempos2, con escritura en disco: ' + str(sum(tiempos2) / len(tiempos2)))
    file.close()

apply_class()