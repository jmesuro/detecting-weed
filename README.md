Detección de Malezas
====================

Joaquín O. Mesuro, Lucas C. Uzal, Guillermo L. Grinblat, Pablo M. Granitto

CIFASIS, Centro Internacional Franco-Argentino de Ciencias de la Información y de Sistemas,
UNR-CONICET, Argentina

En este proyecto presentamos un sistema para la detección de malezas 
en campos de soja en diferentes estadíos de crecimientos. 
El sistema permite que un robot desmalezador equipado con una cámara de video RGB, 
encuentre la ubicación en el espacio de la maleza para la fumigación en tiempo real. 
Construimos un clasificador de 3 clases (suelo, maleza y soja), usamos una red CNN, 
un modelo [WGAN](https://arxiv.org/abs/1704.00028) basado en la arquitectura 
[DCGAN](https://arxiv.org/abs/1511.06434). 
El entrenamiento es semi-supervizado, usamos aproximadamente 1.5 millones de imágenes
 no etiquetadas para representation-learning y alrededor de 10 mil imágenes etiquetadas
 para el detector.

---
[TOC]

# Requerimientos
```bash

Ubuntu 20.04.2 LTS
cuda_11.0.1_450.36.06_linux.run
gcc version 9.3.0 

sudo apt-get install g++ freeglut3-dev build-essential libx11-dev libxmu-dev libxi-dev libglu1-mesa libglu1-mesa-dev

miniconda3 4.8.3
conda create --name <name> --file ./assets/requirements.txt 
```

## 1 El robot
![](assets/robot.png "icon")



El objetivo es que el robot navegue de manera autónoma por cualquier 
campo de soja, con o sin surcos
de cultivos, de día, y en diversas condiciones climáticas. El sistema
de navegación utiliza una cámara estéreo RGB montada en uno de sus ejes, 
y con la misma cámara queremos hacer la detección de malezas. De esta manera, 
el area de visión de la cámara tiene que servir para ambos propósitos. 


## 2 Ejemplo de detección
![](assets/class.gif)

La detección de las malezas se hace definiendo un ROI (Region of interest),
el ROI en celdas que llamamos _parches_ (64x64 pixel) y clasificando cada una de 
ellas en maleza, soja o suelo. 

## 3 Arquitectura
![](assets/diagramagan2.png)

Nuestro Clasificador de malezas se ubica a la salida del Discriminador en 
paralelo con el clasificador del GAN.
En cada iteración del entrenamiento se arma un mini-batch con mitad de imágenes reales y 
mitad de imágenes generadas, con la etiqueta correspondiente según de donde provenga.



## 4 Entornos
![](assets/m_image-002-02306.jpg)
Decidimos hacer experimentos con lo que llamamos _entornos_. 
Un entorno se refiere al las celdas que rodean al _parche_, 
es decir, es un _parche_ más grande y que contiene a otro.

## 5 Conjunto de Datos

Cada elemento del batch al momento del entrenamiento o aplicación, tanto para 
los _parhces_ y _entornos_ es una imagen de 64 x 64. 

Los dataset tienen imágenes de 224 x 224 pix para los dataset. Tienen esos tamaño porque 
usamos Data Aumentation (DA) y puede producir bordes negros.

![](assets/da3.png)
 En a) un minibatch de 16 imágenes de 224 pix. 
En b) se aplica DA. De allí se extraen los _parches_ 
(en verde) y los _entornos_ (en rojo), estos últimos son reescalados a 64 pix.

Las imágenes se consiguieron desde dos set de filmaciones, 
una realizada por fotógrafos profesionales _videos_fot_ en una resolución 3840 x 2160 y 
otra por filmaciones realizadas con una cámara montada en el robot _videos_robot_ 2560 x 1440.
Ambos fueron reescaleadas a 1920 x 1080 y todos los frames fueron extraídos en formato png. 

Se generaron 4 conjuntos.
1. _unlabeled\_(1200878,224,224)\_photographer.h5_
2. _unlabeled\_(376200,224,244)\_robot.h5_
3. _labeled\_(9585,224,224)\_photographer.npy_
4. _labeled\_(999,224,224)\_robot.npy_

## 6 Ejecución

```bash_apply_class.py_ , _apply_class_entornos.py_ ```

Aplican la clasificacion definida por los parametros:

_checkpoint_path, input_frames_path, output_class_frames_path, model_dim, threshold_

Esta pensado para aplicar clasificaciones sobre carpetas de frames de video. 
Se le pasa como parámetro un archivo de texto. Ej [_test_16.in_](in_16_test.in) donde se definen esos 
parámetros y se puede hacer más de una clasificación.

La salida es una carpeta con los parches etiquetados.

---
```bash _apply_class.py_ _apply_class_roc_ , _apply_class_entornos_roc_```

Aplica un clasificador entornos para obtener la curva ROC y PR

Aplica la clasificacion a un dataset etiquetado, la curva
se calcula moviendo el valor del threshold de a pasos definidos por
tr_step. Ademas hace lo mismo pero discriminando parches que estan
en el centro de la imagen y parches que estan en los bordes laterales.
Esto ultimo es para comprar si es verdad que el clasificador es mejor
en el centro. Para eso se usa el archivo idx, con la info de cada parche.

La salida son archivos 

* _roc.log_ tiene columnas: (thres,acc,tp,fp,tn,fn,tpr,fpr)
* _pr.log_ tiene columnas: (thres,acc,tp,fp,tn,fn,p,r)
* _solo_roc.log_: (pr,fpr)
* _solo_pr.log_: (p,r)
* _best_line_roc.log_: La línea con mejor acc de roc.log
* _print.log_ una línea con: (best_acc, aur, aur_pr)


## 7 Resultados

* _acc_ es la cantidad de celdas bien clasificadas sobre el total del ROI 
* _AUR_ es el área bajo la curva ROC
* _AUPR_ es el área bajo la curva de precision-recall 

Estudiamos como se comporta el clasificador en relación al origen de las filmaciones,
de dos datasets, llamamos **P** al dataset que probiene de _videos\_fot_ y **R** a los
provenientes de _videos\_robot_. Ambas filmaciones fueron hechas en días, en campos y con cámaras
distintas. Y a su vez cada set de filmación tiene diversos estadíos de crecimiento. 


**P** es de mejor calidad que **R**
![](assets/for_vs_rob_2.png)
 
Tiene mejor definición, menor distorcion en el lente de la cámara y mejores condiciones de luz.
Además **P** tiene 10 veces más imágenes etiquetadas y 4 veces no etiquetadas. 

Con estos dos conjuntos podemos simular diferentes escenarios, por ejemlo, el caso donde tenemos
un entrenamiento con buenas imágenes y queremos fumigar un campo con malas imágenes por diferentes
motivos, una cámara inferior, por condiciones climáticas, etc. O el caso contrario, 
o casos intermedios donde podemos mezclar imágenes de distintas fuentes.


![](assets/tabla_resultados.png)

# Otras Pruebas
## FFT 
```bash fft_dim1.py ``` 
Calcula la frecuencia de las imágenes aplicando fast fourier transform. La idea
era hacer parches de tamaño adaptativos según su frecuencia. Es decir más grandes mientras menor 
frecuencia. Con la hipótesis de que los más lejanos tienen mayor frecuencia. Los resultados no fueron 
los esperados.

![](assets/image-2996_fft.png)
---
![](assets/image-11174_fft.png)

## Green
```bash gree_claification.py ```
Permite detectar las plantas mediente su tonalidad de verde.
La idea era preprocesar las imágenes con este detector antes de meterla en la red.
![](assets/2494_thumb0200_mask.png)

## UDA
```bash train_wgan_softmax_uda.py ``` 
Intenta hacer Unsupervised Domain Adaptation by Backpropagation 
https://arxiv.org/abs/1409.7495 entre los dataset de fotografos y robot. Los resultados no son buenos.

## COMP
```bash train_wmax_softmax_comp.py ``` 
Permite entrenar una red que compara dos parches
y te dice si es el mismo o no. Es para armar un tracker de maleza.
Está mal encarado. Una buena idea es el enfoque de reconocimiento facial
donde autoencodeas las caras, y las cercanas en el nuevo espacio son la misma.